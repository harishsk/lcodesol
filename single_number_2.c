int singleNumber(int* nums, int numsSize){
	int res = 0;
	for(int bit_pos = 0; bit_pos < sizeof(int) * 8; bit_pos++) {
		unsigned int mask = 1 << bit_pos, sum;
		for(int idx = 0; idx < numsSize; idx++) {
			if(nums[idx] & mask) {
				sum++;
			}
		}
		sum = sum % 3;
		res = res | sum;
	}
	return res;
}
