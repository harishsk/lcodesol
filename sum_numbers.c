/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

void sumNumbers_r(struct TreeNode* root, int csum, int *sum){
   if(root == NULL)
      return;
   csum = csum * 10 + root->val;
   if(root->left == NULL && root->right == NULL) {
      *sum = *sum + csum;
      return;
   }
   sumNumbers_r(root->left, csum, sum);
   sumNumbers_r(root->right, csum, sum);   
}

int sumNumbers(struct TreeNode* root){
   int sum = 0;
   sumNumbers_r(root, 0, &sum);
   return sum;
}
