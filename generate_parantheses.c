#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void add_back(char ***ret, int *rS, char *s, int len) {
    *ret = realloc(*ret, sizeof(char*) * (*rS + 1));
    (*ret)[*rS] = malloc(sizeof(char) * (len + 1));
    memcpy((*ret)[*rS], s, len);
    (*ret)[*rS][len] = '\0';
    //printf("AD %s\n", (*ret)[*rS]);
    *rS = *rS + 1;
}
void generateParenthesis_r(char ***ret, int *rS,int nump, int numo, 
                           int numc, char *s, int len) {
   if(2*nump == len) {
      //printf("AB %s \n", s);
      add_back(ret, rS, s, len);
      return;
   }
      
   if(numo < nump) {
      char temp[len +2];
      memcpy(temp, s, len);
      temp[len] = '(';
      temp[len + 1] = '\0';
      //printf("%s \n", temp);
      generateParenthesis_r(ret, rS, nump, numo + 1, numc, temp, len + 1);
   }
   if(numc < numo) {
      char temp[len +2];
      memcpy(temp, s, len);
      temp[len] = ')';
      temp[len + 1] = '\0';
      //printf("%s \n", temp);
      generateParenthesis_r(ret, rS, nump, numo, numc + 1, temp, len + 1);
   }
   return;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
char ** generateParenthesis(int n, int* returnSize){
    char **ret = NULL;
    
    *returnSize = 0;
    generateParenthesis_r(&ret, returnSize, n, 0, 0, "", 0);
    //printf("ReturnSize is %d\n", *returnSize);
    
    return ret;

}
int main() {
   int n = 5;
   int rs;
   char **p;
   p = generateParenthesis(n, &rs);

   printf("rs is %d\n", rs);
   for(int i = 0; i < rs; i++) {
      printf("%s\n", p[i]);
      free(p[i]);
   }
   free(p);
   return 0;
}
   
