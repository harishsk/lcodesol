#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d -> ", p->val);
      p = p->next;
   }
   printf("NULL\n");

}

int list_length(struct ListNode* head) {
   int n = 0;
   while(head) {
      n++;
      head = head->next;
   }
   return n;
}

struct ListNode *reverse(struct ListNode** head, int k){
   struct ListNode* cur = *head, *next, *prev = NULL;
   int count = 0;

   while(cur != NULL && count < k) {
      next = cur->next;
      cur->next = prev;
      prev = cur;
      cur = next;
      count++;
   }
   cur = *head;
   *head = prev;
   cur->next = next;
   return cur;
}

struct ListNode* reverseKGroup(struct ListNode* head, int k){
   if(head == NULL || k < 2)
      return head;
   int n =list_length(head);

   struct ListNode dummy_head;
   struct ListNode *cur = &dummy_head;

   cur->next = head;
   
   for(int i = 0; i < n/k; i++) {
      cur = reverse(&cur->next, k);
   }
   return dummy_head.next;
}

int main()
{
   struct ListNode a, b, c;
   struct ListNode x, y, z;
   
   a.val = 1;
   a.next = &b;

   b.val = 2;
   b.next = &c;

   c.val = 3;
   c.next = &x;

   x.val = 4;
   x.next = &y;

   y.val = 5;
   y.next = &z;

   z.val = 6;
   z.next = NULL;   

   
   struct ListNode *m = reverseKGroup(&a, 4);
   
   print_list(m);
   
}
