/*
https://leetcode.com/problems/count-and-say/
The count-and-say sequence is the sequence of integers with the first five terms as following:

1.     1
2.     11
3.     21
4.     1211
5.     111221
1 is read off as "one 1" or 11.
11 is read off as "two 1s" or 21.
21 is read off as "one 2, then one 1" or 1211.

Given an integer n where 1 <= n <= 30, generate the nth term of the count-and-say sequence. You can do so recursively, in other words from the previous member read off the digits, counting the number of digits in groups of the same digit.

Note: Each term of the sequence of integers will be represented as a string.

 

Example 1:

Input: 1
Output: "1"
Explanation: This is the base case.
Example 2:

Input: 4
Output: "1211"
Explanation: For n = 3 the term was "21" in which we have two groups "2" and "1", "2" can be read as "12" which means frequency = 1 and value = 2, the same way "1" is read as "11", so the answer is the concatenation of "12" and "11" which is "1211".
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STR_MAX 5000

char str1[STR_MAX] = {0};
char str2[STR_MAX] = {0};

char * countAndSay(int n)
{
   int i = 1, j = 1, k = 0, p_cnt = 0;
   char *s = NULL, *d = NULL;
   char p_char = 0;
   int cnt = 0;
   
   str1[0] = '1';
   str1[1] = 0;
   str2[0] = 0;   
   
   for(i = 2; i <= n; i++) {
      if(i % 2 == 0) {
         s = str1;
         d = str2;
      }else{
         s = str2;
         d = str1;
      }
      cnt = 0;
      p_char = 0;
      k = 0;
      
      for(j = 0; s[j] != 0; j++) {
         if(p_char == 0) {
            cnt++;
         }else if(p_char == s[j]){
            cnt++;
         }else {
            d[k++] = (char)cnt + '0';
            d[k++] = p_char;
            cnt = 1;
         }
         p_char = s[j];
      }
      d[k++] = (char)cnt + '0';
      d[k++] = p_char;
      d[k++] = 0;      
   }
   if(n == 1) {
      return str1;
   }else {
      return d;
   }
}

int main()
{
   int n = 30;

   printf("%d say and tell is %s\n", n, countAndSay(n));

   return 0;
}
