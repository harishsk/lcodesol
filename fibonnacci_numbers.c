/* Link:
https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
*/

/* Problem:
The Fibonacci numbers are the numbers in the following integer sequence.
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...

In mathematical terms, the sequence Fn of Fibonacci numbers is defined by the recurrence relation
    Fn = Fn-1 + Fn-2
with seed values
    F0 = 0 and F1 = 1.

Given a number n, print n-th Fibonacci Number.
Examples:

Input  : n = 2
Output : 1

Input  : n = 9
Output : 34

Write a function int fib(int n) that returns Fn. For example, if n = 0, then fib() should return 0. If n = 1, then it should return 1. For n > 1, it should return Fn-1 + Fn-2

For n = 9
Output:34

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

/* Solution: */

int fib(int n) {
   int f[n];
   f[0] = 1;
   f[1] = 1;
   for(int i = 2; i < n; i++) {
      f[i] = f[i-1] + f[i-2];
   }
   return f[n-1];
}

/* Test Driver: */
int main (){ 
  int n = 9; 
  printf("%d th fibonachi number is %d\n", fib(n));
  return 0; 
} 
