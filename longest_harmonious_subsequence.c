/* Link:
   https://leetcode.com/problems/longest-harmonious-subsequence/
*/

/* Problem:
   We define a harmonious array as an array where the difference between its maximum value and its minimum value is exactly 1.

Given an integer array nums, return the length of its longest harmonious subsequence among all its possible subsequences.

A subsequence of array is a sequence that can be derived from the array by deleting some or no elements without changing the order of the remaining elements.

 

Example 1:

Input: nums = [1,3,2,2,5,2,3,7]
Output: 5
Explanation: The longest harmonious subsequence is [3,2,2,2,3].

Example 2:

Input: nums = [1,2,3,4]
Output: 2

Example 3:

Input: nums = [1,1,1,1]
Output: 0

 

Constraints:

    1 <= nums.length <= 2 * 104
    -109 <= nums[i] <= 109


*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <uthash.h>
#endif

/* Solution: */

# if defined(BRUTE_FORCE)
#define MAX(x, y) ((x) > (y) ? (x) : (y))

int find_hs(int* nums, int numsSize, int x, int y){
   int len_x = 0, len_y = 0;
   for(int i = 0; i < numsSize; i++){
      if(nums[i] == x)
         len_x++;
      if(nums[i] == y)
         len_y++;      
   }
   if(len_x > 0 && len_y > 0)
      return (len_x + len_y);
   else 
      return 0;
}

int findLHS(int* nums, int numsSize){
   int max = 0;
   for(int i = 0; i < numsSize; i++) {
      int len_hs = find_hs(nums, numsSize, nums[i], nums[i] - 1);
      max = MAX(max, len_hs);
      len_hs = find_hs(nums, numsSize, nums[i], nums[i] + 1);
      max = MAX(max, len_hs);      
   }
   return max;
}
#elif defined(SORT)
int cmp(const void *a, const void *b) {
   long la = *(int*)a;
   long lb = *(int*)b;   

   return (la - lb);
}
int find_hs(int* nums, int numsSize, int x, int y){
   int len_x = 0, len_y = 0;
   for(int i = 0; i < numsSize; i++){
      if(nums[i] == x)
         len_x++;
      if(nums[i] == y)
         len_y++;      
   }
   if(len_x > 0 && len_y > 0)
      return (len_x + len_y);
   else 
      return 0;
}

#define MAX(x, y) ((x) > (y) ? (x) : (y))
int findLHS(int* nums, int numsSize){

   /* Sort the given array */
   qsort(nums, numsSize, sizeof(int), cmp);
   int max = 0, prev = nums[0];
   max = MAX(max, find_hs(nums, numsSize, nums[0], nums[0] + 1));
   for(int i = 1; i < numsSize; i++) {
      if(prev != nums[i])
         max = MAX(max, find_hs(nums, numsSize, nums[i], nums[i] + 1));
      prev = nums[i];
   }
   return max;
}
#elif defined(HASHMAP)
struct inthash{
   int num;
   int cnt;
   UT_hash_handle hh;
};
void hash_add(struct inthash **ihash, int num){
   struct inthash *el = NULL;
   HASH_FIND_INT(*ihash, &num, el);
   if(el == NULL){
      el = malloc(sizeof(struct inthash));
      el->num = num;
      el->cnt = 1;
      HASH_ADD_INT(*ihash, num, el);
   }else {
      el->cnt++;
   }
}
#define MAX(x, y) ((x) > (y) ? (x) : (y))
int findLHS(int* nums, int numsSize){

   struct inthash *ihash = NULL;
   for(int i = 0; i < numsSize; i++)
      hash_add(&ihash, nums[i]);
   

   struct inthash *el, *tmp, *tmp2;
   int max = 0;
   HASH_ITER(hh, ihash, el, tmp) {
      int fnum = el->num + 1;
      HASH_FIND_INT(ihash, &fnum, tmp2);
      if(tmp2 != NULL)
         max = MAX(max, el->cnt + tmp2->cnt);

   }
   HASH_ITER(hh, ihash, el, tmp) {
      HASH_DEL(ihash, el);
      free(el);
   }
   return max;
}

#elif defined(HASHMAP_SINGLE_LOOP)

struct inthash{
   int num;
   int cnt;
   UT_hash_handle hh;
};
int hash_add(struct inthash **ihash, int num){
   struct inthash *el = NULL;
   int cnt = 0;
   HASH_FIND_INT(*ihash, &num, el);
   if(el == NULL){
      el = malloc(sizeof(struct inthash));
      el->num = num;
      el->cnt = 1;
      HASH_ADD_INT(*ihash, num, el);
   }else {
      el->cnt++;
   }
   cnt = el->cnt;
   return cnt;
}
#define MAX(x, y) ((x) > (y) ? (x) : (y))
int findLHS(int* nums, int numsSize){

   struct inthash *ihash = NULL, *tmp = NULL;
   int max = 0, fnum, cnt;
   for(int i = 0; i < numsSize; i++) {
      cnt = hash_add(&ihash, nums[i]);
      fnum = nums[i] + 1;
      HASH_FIND_INT(ihash, &fnum, tmp);
      if(tmp != NULL)
         max = MAX(max, cnt + tmp->cnt);
      fnum = nums[i] - 1;
      HASH_FIND_INT(ihash, &fnum, tmp);
      if(tmp != NULL)
         max = MAX(max, cnt + tmp->cnt);      
   }
   struct inthash *el = NULL;
   HASH_ITER(hh, ihash, el, tmp) {
      HASH_DEL(ihash, el);
      free(el);
   }
   return max;
}
#else /* SORT_HASHMAP_SINGLE */

int cmp(const void *a, const void *b) {
   long la = *(int*)a;
   long lb = *(int*)b;   

   return (la - lb);
}
struct inthash{
   int num;
   int cnt;
   UT_hash_handle hh;
};
int hash_add(struct inthash **ihash, int num){
   struct inthash *el = NULL;
   int cnt = 0;
   HASH_FIND_INT(*ihash, &num, el);
   if(el == NULL){
      el = malloc(sizeof(struct inthash));
      el->num = num;
      el->cnt = 1;
      HASH_ADD_INT(*ihash, num, el);
   }else {
      el->cnt++;
   }
   cnt = el->cnt;
   return cnt;
}
#define MAX(x, y) ((x) > (y) ? (x) : (y))
int findLHS(int* nums, int numsSize){

   /* Sort the given array */
   qsort(nums, numsSize, sizeof(int), cmp);
   int max = 0;
   struct inthash *ihash = NULL, *tmp = NULL;
   int fnum, cnt;

   int i = 0;
   
   for(i = 0; i < numsSize - 1; i++) {
      cnt = hash_add(&ihash, nums[i]);
      if(nums[i] != nums[i + 1]){
         fnum = nums[i] + 1;
         HASH_FIND_INT(ihash, &fnum, tmp);
         if(tmp != NULL)
            max = MAX(max, cnt + tmp->cnt);
         fnum = nums[i] - 1;
         HASH_FIND_INT(ihash, &fnum, tmp);
         if(tmp != NULL)
            max = MAX(max, cnt + tmp->cnt);         
      }
   }
   cnt = hash_add(&ihash, nums[i]);
   fnum = nums[i] + 1;
   HASH_FIND_INT(ihash, &fnum, tmp);
   if(tmp != NULL)
      max = MAX(max, cnt + tmp->cnt);
   fnum = nums[i] - 1;
   HASH_FIND_INT(ihash, &fnum, tmp);
   if(tmp != NULL)
      max = MAX(max, cnt + tmp->cnt);            
   
   return max;
}

#endif

/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = {-3,-1,-1,-1,-3,-2};
   //int a[] = {1,3,2,2,5,2,3,7};
   printf("LHS is %d\n", findLHS(a, sizeof(a)/sizeof(a[0])));
   
   return 0;
}
#endif
