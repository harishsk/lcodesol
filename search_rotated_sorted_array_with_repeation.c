bool search(int* nums, int numsSize, int target){
	int low, mid, high;
	bool found = false;
		
    
    if(numsSize == 0 || nums == NULL)
        return false;
    
	low = 0;
	high = numsSize -1;

	while(low <=high) {
		mid = low + (high - low) / 2;
	
		if(nums[low] == target || nums[mid] == target || nums[high] == target) {
			found = true;
			break;
		}	
		if(nums[low] > target) {
			if(nums[low] >= nums[mid]) 
				high = mid - 1;
			else
				low = mid + 1;
		}else if(nums[low] < target) {
			if(nums[low] > nums[mid]) 
				high = mid - 1;
			else
				low = mid + 1;
		}else {
			found = true;
			break;
		}
	}
	return found;
}

