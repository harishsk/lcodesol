#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

static inline bool isValidRow(char **board, int boardSize, int ridx) {
	int cidx = 0;
	int cnt[10] = {0};
   int num;
	for(cidx = 0 ; cidx < boardSize; cidx++) {      
      if(board[cidx][ridx] == '.')
         continue;
      num = board[cidx][ridx] - '0'; 
        
	  if(cnt[num] > 0) 
		  return false;
	  else 
			cnt[num] = 1;
	}
	return true;
}

static inline bool isValidCol(char **board, int boardSize, int cidx) {
	int ridx = 0, num;
	int cnt[10] = {0};
	for(ridx = 0 ; ridx <boardSize; ridx++) {
      if(board[cidx][ridx] == '.')
         continue;
      num = board[cidx][ridx] - '0';      
		if(cnt[num] > 0) 
			return false;
		else 
			cnt[num] = 1;
	}
	return true;
}

static inline bool isValidSquare(char **board, int boardSize, int rstart, int cstart) {
	int cidx = 0, ridx = 0, num;
	int cnt[10] = {0};
    
	for(cidx = cstart; cidx < cstart + 3; cidx++) {
        for(ridx = rstart; ridx < rstart + 3; ridx++) {

         if(board[cidx][ridx] == '.')
            continue;
         num = board[cidx][ridx] - '0';      
         if(cnt[num] > 0) 
            return false;
         else 
            cnt[num] = 1;
		}
	}
	return true;
}

bool isValidSudoku(char **board, int boardSize, int* boardColSize){
	int cidx = 0, ridx = 0;

    
   for(ridx = 0; ridx < boardSize; ridx++) {
		if(!isValidRow(board, boardSize, ridx)) 
			return false;
	}
	for(cidx = 0; cidx < boardSize; cidx++) {
		if(!isValidCol(board, boardSize, cidx)) 
			return false;
	}

	for(cidx = 0; cidx < boardSize; cidx+=3) {
        for(ridx = 0; ridx < boardSize; ridx+=3) {
			if(!isValidSquare(board, boardSize, ridx, cidx)) 
				return false;
		}
	}
	return true;
}

int main()
{
   char **board = {"53..7....", }

   //printf("board %p \n", board);
   //printf("board %p \n", &board);   
   
   printf("Is valid Sudoku %d\n", isValidSudoku(board, 9, NULL));

   return 0;
   
}
