#if defined(BINARY_SEARCH)
bool binary_search(int *a, int *b, int target) {
	int *mid, *low, *high;
	bool found = false;
	
	low = a;
	high = b;
	while(low <= high) {
		mid = low + (high - low) / 2;
        if(*mid == target) {
            found = true;
            break;
        }
		if(*mid > target) {
			high = mid - 1; 
		}else if(*mid < target){
			low = mid + 1;
		}
	}
	return found;
}
bool searchMatrix(int** matrix, int matrixRowSize, int matrixColSize, int target) {
    for(int row = 0; row < matrixRowSize; row++) {
		if(true == binary_search(matrix[row], matrix[row] + matrixColSize - 1, 
							target)) {
			return true;
		}
	}
	return false;
}
#else
bool searchMatrix(int** matrix, int matrixRowSize, int matrixColSize, int target) {
    int row = 0, col = matrixColSize - 1;
    
    if (matrix == NULL || matrixRowSize <= 0 || matrixColSize <= 0)
       return false;

    while (row < matrixRowSize && col >= 0) {
       if (target == matrix[row][col]) {
          return true;
       }else if (target > matrix[row][col]) {
          row++;
       }else {
          col--;
       }
    }
    return false;
}
#endif

