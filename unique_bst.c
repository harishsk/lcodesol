#include <stdio.h>
#include <stdlib.h>

/**
 * Definition for a binary tree node. */
struct TreeNode {
   int val;
   struct TreeNode *left;
   struct TreeNode *right;
};

void pushNode(struct TreeNode*** arr, struct TreeNode* n, int* returnSize)
{
    *returnSize += 1;
    *arr = (struct TreeNode**)realloc(*arr, sizeof(struct TreeNode*) * (*returnSize));
    (*arr)[*returnSize-1] = n;
}
struct TreeNode* createNode(int a) {
    struct TreeNode *t = NULL;
    t = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    t->left = t->right = NULL;
    t->val = a;
    return t;
}

struct TreeNode** generateTrees_r(int start, int end, int* returnSize){

   struct TreeNode** arr= (struct TreeNode**)malloc(sizeof(struct TreeNode*));
   struct TreeNode* t = NULL;
   
   if(start > end) {
      pushNode(&arr, NULL, returnSize); // NULL node
      return arr;
   }else if(start == end) {
      t = createNode(start);
      pushNode(&arr, t, returnSize);
      return arr;
   }
   for(int cnt = start; cnt <= end; cnt++) {
      int lcnt_max = 0, rcnt_max = 0, lcnt, rcnt;
      struct TreeNode **larr = generateTrees_r(begin, cnt - 1, &lcnt_max);
      struct TreeNode **rarr = generateTrees_r(cnt + 1, end, &rcnt_max);
      for(int lcnt = 0; lcnt < lcnt_max; lcnt++) {
         for(int rcnt = 0; rcnt < rcnt_max; rcnt++) {
            t = createNode(i);
            pushNode(&arr, t, returnSize);
            t->left = larr[lcnt];
            t->right = rarr[rct];
         }
      }
      
   }
   return arr;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
struct TreeNode** generateTrees(int n, int* returnSize){

   if(n == 0) {
      *returnSize = 0;
      return NULL;
   }
   return generateTrees_r(1, n, returnSize);

}
