#ifdef BRUTE_FORCE
struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
	struct ListNode *pa = headA, *pb = headB;

	while(pa != NULL) {
		pb = headB;
		while(pb != NULL) {
			if(pa == pb)
				return pa;
			pb = pb->next;
		}
		pa = pa->next;
	}
	return NULL;
}
#elif CREATING_LOOP_METHOD

struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {

   /* If either of head is empty there cannot be Intersection */ 
   if(headA == NULL || headB == NULL)
      return NULL;   
   
   /* Check if first non NULL node is same */
   if(headA != NULL && headB != NULL && headA == headB)
      return headA;
   
   struct ListNode *pa = headA;

   /* Traverse to end of Link List A */
	while(pa->next != NULL)
 		pa = pa->next;
   
	/* Create a loop */
   pa->next = headA;
   
   if(headB->next == NULL) {
      pa->next = NULL;
      /* Remove loop before returning */
      return NULL;
   }

   /* Detect loop in Link List B, this should detect the 
      loop created in Link List A if there is a intersection */
   struct ListNode *slow = headB->next, *fast = headB->next->next;
   while(fast != NULL && fast->next != NULL){
      if(slow == fast)
         break;
      
      slow = slow->next;
      fast = fast->next->next;
   }
   /* Find where the loop starts, that is intersection point */
   if(slow == fast) {
      struct ListNode *cur = headB;
      
      while(cur != slow) {
         cur = cur->next;
         slow = slow->next;
      }
      /* Remove loop before returning */
      pa->next = NULL;
      return cur;
   }
   /* Remove loop before returning */
   pa->next = NULL;
   /* No intersection found */
	return NULL;
}
#else
struct ListNode *getIntersectionNode(struct ListNode *headA, struct ListNode *headB) {
   /* If either of head is empty there cannot be Intersection */ 
   if(headA == NULL || headB == NULL)
      return NULL;
   
   /* Find length of both Linked List */
   int lenA = findLLLen(headA);
   int lenB = findLLLen(headB);

   /* Skip first |lenA - lenB| of the longest List */   
   struct ListNode *p = (lenA > lenB) ? headA : headB;
   struct ListNode *q = (lenA > lenB) ? headB : headA;
   int skip = (lenA > lenB) ? (lenA - lenB) : (lenB - lenA);

   for(int i = 0; i < skip; i++) 
      p = p->next;
   
   while(p && q) {
      if(p == q)
         return p;
      p = p->next;
      q = q->next;
   }
   return NULL;
}

int findLLLen(struct ListNode *head) {
   struct ListNode* curr;
   int ret = 0;
   
   curr = head;
   while(curr) {
      curr = curr->next;
      ret++;
   }
   return ret;
}
#endif
