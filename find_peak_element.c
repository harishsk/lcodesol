
int findPeakElement(int* nums, int numsSize){
	int low, mid, high;

	low = 0;
	high = numsSize - 1;

	while(low < high) {
		mid = low + (high - low)/2;
		if(nums[low] < nums[high] )
			low = mid + 1;
		else if(nums[low] > nums[high])
			high = mid;
		else 
			high--;
	}
	return high;
}
