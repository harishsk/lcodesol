#include <stdio.h>
#include <stdlib.h>

typedef int csum_t;

typedef struct {
    csum_t *csum;
    int n;
} NumArray;


NumArray* numArrayCreate(int* nums, int numsSize) {
    if(nums == NULL || numsSize < 1)
        return NULL;
    csum_t *csum = malloc(sizeof(csum_t)*numsSize);
    csum[0] = nums[0];
    for (int i = 1; i < numsSize; i++) {
        csum[i] = nums[i] + csum[i-1];
    }
    NumArray *na = malloc(sizeof(NumArray));
    na->csum = csum;
    na->n = numsSize;
    return na;
}

int numArraySumRange(NumArray* obj, int i, int j) {

   return (obj->csum[j+1] - obj->csum[i]);
  
}

void numArrayFree(NumArray* obj) {
    free(obj->csum);
    free(obj);    
}
int main()
{
   int nums[] = {-2, 0, 3, -5, 2, -1};
   NumArray* obj = numArrayCreate(nums, sizeof(nums)/sizeof(nums[0]));
   int param_1 = numArraySumRange(obj, 2, 3);
   printf("%d\n", param_1);
 
   numArrayFree(obj);
   return 0;
}
