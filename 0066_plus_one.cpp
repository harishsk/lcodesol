#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
   vector<int> plusOne(vector<int>& digits) {
      int r = 1;
      for(int i = digits.size() - 1; i >= 0; i--) {
         digits[i] += r;
         r = digits[i] / 10;
         digits[i] %= 10;
      }
      if(r == 1)
         digits.insert(digits.begin(), 1);
      return digits;
   }
};

int main()
{
   Solution s;
   vector <int>d{9,9,9,9};
   d = s.plusOne(d);

   for(int i = 0; i < d.size(); i++){
      cout << d[i];
   }
   cout << endl;
   
   return 0;
}
