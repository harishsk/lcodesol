/* Link:
   https://leetcode.com/problems/sort-array-by-parity/   
*/

/* Problem:
Given an array A of non-negative integers, return an array consisting of all the even elements of A, followed by all the odd elements of A.

You may return any answer array that satisfies this condition.

 

Example 1:

Input: [3,1,2,4]
Output: [2,4,3,1]
The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
#define IS_EVEN(x) ((x) %2 == 0)
#define IS_ODD(x) (!IS_EVEN(x))

int* sortArrayByParity(int* A, int ASize, int* returnSize){
   if(ASize < 1) {
      *returnSize = 0;
      return NULL;
   }
   int *RA = malloc(sizeof(int) * ASize);
   *returnSize = ASize;

   int rai = 0;
   /* Copy all even first */
   for(int i = 0; i < ASize; i++) {
      if(IS_EVEN(A[i]))
         RA[rai++] = A[i];
   }
   /* Copy odd now first */
   for(int i = 0; i < ASize; i++) {
      if(IS_ODD(A[i]))
         RA[rai++] = A[i];
   }
   return RA;
      
}

/* Test Driver: */
#if defined(TEST)
int main() {
   int a[]= {3,1,2,4};
   int *r, s;
   r = sortArrayByParity(a, sizeof(a)/sizeof(a[0]), &s);
   for(int i = 0; i < s; i++) {
      printf("%d ", r[i]);
   }
   printf("\n");
   free(r);
   
   return 0;
}
#endif
