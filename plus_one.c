/*
https://leetcode.com/problems/plus-one/
Given a non-empty array of digits representing a non-negative integer, increment one to the integer.

The digits are stored such that the most significant digit is at the head of the list, and each element in the array contains a single digit.

You may assume the integer does not contain any leading zero, except the number 0 itself.

Example 1:

Input: [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Example 2:

Input: [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* plusOne(int* digits, int digitsSize, int* returnSize)
{
   int i = 0, r = 0;;
   int *p = malloc(sizeof(int) * (digitsSize + 1));
   int *q = NULL;
   if(p == NULL){
      return NULL;
   }
   p[digitsSize] = digits[digitsSize-1] + 1;
   r = p[digitsSize] / 10;   
   p[digitsSize] = p[digitsSize] % 10;

   for(i = digitsSize-2; i >= 0; i--) {
      p[i + 1] = digits[i] + r;
      r = p[i + 1] / 10;   
      p[i + 1] = p[i + 1] % 10;      
   }
   if(r != 0) {
      p[0] = r;
      *returnSize = digitsSize + 1;
      return p;
   }else {
      *returnSize = digitsSize;
      q = malloc(sizeof(int) * digitsSize);
      memcpy(q, p+1, sizeof(int) * digitsSize);
      free(p);
      return q;
   }
}

int main()
{
   int num[] = {1, 2, 3};
   int *pnum = NULL;
   int size = 0, i;

   pnum = plusOne(num, sizeof(num)/sizeof(num[0]), &size);

   printf("Plus one array is : \n");

   for(i = 0; i< size; i++) {
      printf("%d ", pnum[i]);
   }
   printf("\n");

   free(pnum);

   return 0;
}
