/*
104. Maximum Depth of Binary Tree
https://leetcode.com/problems/maximum-depth-of-binary-tree/

Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its depth = 3.


 */

#include <stdio.h>

/**
 * Definition for a binary tree node.
 */
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};


int find_level(struct TreeNode* a, struct TreeNode* b);

int maxDepth(struct TreeNode* root)
{
   if(root == NULL) {
      return 0;
   }
   return find_level(root->left, root->right);
}

int find_level(struct TreeNode* a, struct TreeNode* b)
{
   int level_a, level_b;
   
   if(a == NULL && b == NULL) {
      return 1;
   }
   if(a == NULL) {
      return find_level(b->left, b->right) + 1;
   }else if (b == NULL){
      return find_level(a->left, a->right) + 1;
   }else {
      level_a = find_level(a->left, a->right) + 1;
      level_b = find_level(b->left, b->right) + 1;
   }
   return level_a > level_b ? level_a : level_b;
}


int main()
{
   struct TreeNode r1, a1, b1;
   
   r1.left = &a1;
   r1.right = NULL;
   a1.left = &b1;
   b1.left = NULL;
   a1.right = NULL;
   b1.right = NULL;   

   r1.val = 2;
   a1.val = 1;
   b1.val = 1;   

   printf("Level of the tree is %d\n", maxDepth(&r1));
   return 0;
}

