
bool searchMatrix(int** matrix, int matrixSize, int* matrixColSize, int target){

#define ROW(x) ((x) % (*matrixColSize))
#define COL(x) ((x) / (*matrixColSize))
   
   int low, high, mid, numsSize = matrixSize * (*matrixColSize);
   bool found = false;
   
   
   if(matrixSize == 0 || *matrixColSize == 0) {
      return false;
   }
   
   low = 0;
	high = numsSize -1;
	
	while(low <= high) {
		mid = low + (high - low)/2;
		if(matrix[COL(mid)][ROW(mid)] == target) {
			found = 1;
			break;
		}else if(matrix[COL(mid)][ROW(mid)] > target) {
			high = mid - 1;
		}else {
			low = mid + 1;
		}
	}
   return found;
   
#undef ROW
#undef COL
}
