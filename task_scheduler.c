#include <stdio.h>
#include <limits.h>
#include <stdbool.h>
#include <string.h>

int getFreqTask(int *fq_arr, int *ftask) {
	bool done = false;
	int ft_idx = 0;
	while(!done) {
		int max = INT_MIN, max_idx;
		for(int i = 0; i < 27; i++) {
			if(fq_arr[i] > max) {
				max = fq_arr[i];
				max_idx = i;
			}
		}
		if(max != 0) {
			ftask[ft_idx] = max; 
			fq_arr[max_idx] = 0;
         ft_idx++;
		}else {
			done = true;
		}
	}
	return ft_idx;
}

int leastInterval(char* tasks, int tasksSize, int n){
	int cycles = 0, freq_task[27] = {0}, max_task = 0;
	int ftask[27];
   
   if(tasks == NULL || tasksSize <=0)
        return 0;

	//Freq map
	for(int i = 0; i < tasksSize && tasks[i] != '0'; i++) 
		freq_task[tasks[i]-'A']++;

	// Sorted Freq map
	max_task = getFreqTask(freq_task, ftask);
	
	while(max_task > 0) {
		int cur_max_task = max_task;
		for(int i = 0 ; i < cur_max_task; i++) {
			ftask[i]--;
			if(ftask[i] == 0)
				max_task--;
		}
		if((cur_max_task - (n + 1) >= 0) || max_task == 0) {
			cycles += cur_max_task;
		}else {
			cycles += (n + 1);
		}
	}
	return cycles;
}

int main() {
   char *tasks = "AAAAAABCDEFG";
   int n = 2;

   printf("Number of minimum CPU cycles - %d\n", leastInterval(tasks, strlen(tasks), n));

   return 0;
}
