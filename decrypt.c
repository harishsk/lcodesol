/* https://www.pramp.com/challenge/8noLWxLP6JUZJ2bA2rnx */

#include <stdio.h>

void decrypt(char *word, char *out) {
  int i, csum, out_idx, new_char;

  i = 0;
  csum = 1;
  out_idx = 0;
  
  while(word[i] != '\0') {
     printf("csum %d\n", csum);
     new_char = word[i];
     new_char -= csum;
     
     while (new_char < (int)'a') {
        new_char += 26;
     }
     out[out_idx++] = new_char;
     csum += new_char;
     i++;
  }
}
int main()
{
   char in[] = "dnotq";
   char ou[sizeof(in)];

   decrypt(in, ou);

   printf("%s\n", ou);
}

