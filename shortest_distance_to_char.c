/* Link:
   https://leetcode.com/problems/shortest-distance-to-a-character/
*/

/* Problem:
   Given a string s and a character c that occurs in s, return an array of integers answer where answer.length == s.length and answer[i] is the distance from index i to the closest occurrence of character c in s.

The distance between two indices i and j is abs(i - j), where abs is the absolute value function.

 

Example 1:

Input: s = "loveleetcode", c = "e"
Output: [3,2,1,0,1,0,0,1,2,2,1,0]
Explanation: The character 'e' appears at indices 3, 5, 6, and 11 (0-indexed).
The closest occurrence of 'e' for index 0 is at index 3, so the distance is abs(0 - 3) = 3.
The closest occurrence of 'e' for index 1 is at index 3, so the distance is abs(1 - 3) = 3.
For index 4, there is a tie between the 'e' at index 3 and the 'e' at index 5, but the distance is still the same: abs(4 - 3) == abs(4 - 5) = 1.
The closest occurrence of 'e' for index 8 is at index 6, so the distance is abs(8 - 6) = 2.

Example 2:

Input: s = "aaab", c = "b"
Output: [3,2,1,0]

 

Constraints:

    1 <= s.length <= 104
    s[i] and c are lowercase English letters.
    It is guaranteed that c occurs at least once in s.

    
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */


#define MIN(x, y) ((x) < (y) ? (x) : (y))
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* shortestToChar(char * s, char c, int* returnSize){
   int len = strlen(s);
   int *ret = malloc(len * sizeof(int));
   
   int count = 0;
   int found = 0;
   for(int i = 0; i < len; i++) {
      if(s[i] == c) {
         found = 1;
         ret[i] = 0;
         count = 0;
      }else if(found == 0) {
         ret[i] = INT_MAX;
         continue;
      }else {
         count++;
         ret[i] = count;
      }
   }
   count = 0;
   found = 0;   
   for(int i = len - 1; i >= 0; i--) {
      if(s[i] == c) {
         found = 1;
         ret[i] = 0;
         count = 0;
      }else if(found == 0) {
         continue;
      }else {
         count++;
         ret[i] = MIN(ret[i], count);
      }
   }
   *returnSize = len;
   return ret;  
}

/* Test Driver: */
#if defined(TEST)
int main() {
   char *p = "loveleetcode";
   int *r, s;
   r = shortestToChar(p, 'e', &s);

   for(int i = 0; i < s; i++) {
      printf("%d ", r[i]);
   }
   printf("\n");
   free(r);
   return 0;
}
#endif
