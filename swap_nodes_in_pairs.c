#include <stdio.h>
#include <stdlib.h>

struct ListNode {
   int val;
   struct ListNode *next;
};

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}
struct ListNode* swapPairs(struct ListNode* head){
	struct ListNode *odd, *even, *leven;
	int first_swap = 1;

	odd = head;
	if(head == NULL || head->next == NULL) 
      return head;
	even = head->next;
	while(even != NULL && odd != NULL) {
		odd->next = even->next;
		even->next = odd;
		if(first_swap) {
			first_swap = 0;
			head = even;
         leven = odd;
      }else {
			leven->next = even;
         leven = odd;
		}
		odd = odd->next;
		if(odd == NULL) 
			break;
		even = odd->next;
   }
   return head;

}

int main()
{
   struct ListNode a, b, c, d;
   a.val = 1;
   b.val = 10;
   c.val = 12;
   d.val = 15;

   a.next = &b;
   b.next = &c;
   c.next = &d;
   d.next = NULL;

   print_list(swapPairs(&a));

   return 0;
}
