#include <stdio.h>
#include <stdlib.h>

static inline int searchLowRange(int* nums, int numsSize, int target) {
	int low, mid, high;
	int ret_index = -1;
	
	low = 0;
	high = numsSize -1;
	
	while(low <= high) {
      mid = low + (high - low)/2;
		if(nums[mid] >= target)
			high = mid - 1;
		else
			low = mid + 1;

		if(nums[mid] == target) ret_index = mid;
	}
	return ret_index;
}
static inline int searchHighRange(int* nums, int numsSize, int target) {
	int low, mid, high;
	int ret_index = -1;
	
	low = 0;
	high = numsSize -1;
	
	while(low <= high) {
      mid = low + (high - low)/2;
		if(nums[mid] <= target)
         low = mid + 1;
		else
         high = mid - 1;

		if(nums[mid] == target) ret_index = mid;
	}
	return ret_index;
}


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* searchRange(int* nums, int numsSize, int target, int* returnSize){
    
	int *ret_index_range = malloc(sizeof(int) * 2);
   
   if(nums == NULL || numsSize <= 0) {
      ret_index_range[0] = -1;
      ret_index_range[1] = -1;
      *returnSize = 2;
      return ret_index_range;
   }
	
	ret_index_range[0] = searchLowRange(nums, numsSize, target);
	ret_index_range[1] = searchHighRange(nums, numsSize, target);
    
	*returnSize = 2;
	return ret_index_range;
}

int main()
{
   int p[] = {2,2};
   int target = 2;
   int *r = NULL;
   int size = 0;

   r = searchRange(p, sizeof(p)/sizeof(p[0]), target, &size);
   
   printf("Index are %d %d\n", r[0], r[1]);

   free(r);

   return 0;
   
}
