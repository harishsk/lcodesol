struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

#include <stdio.h>
#include <stdlib.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))

int height(struct TreeNode* root) {
	int lh, rh;
	if(root == NULL)
		return 0;
	lh = height(root->left) + 1;
	rh = height(root->right) + 1;
	
	return MAX(lh, rh);
}
void addLevelOrder(struct TreeNode* root, int*** arr, int** returnColumnSizes,
                   int* returnSize, int level, int i){
   
	if(root == NULL) 
		return;
    
	if(level == 0) {
      (*arr)[i] = realloc((*arr)[i], sizeof(int) * ((*returnColumnSizes)[i] + 1));
      (*arr)[i][(*returnColumnSizes)[i]] = root->val;
      (*returnColumnSizes)[i] = (*returnColumnSizes)[i] + 1;
      return;
	}
	addLevelOrder(root->left, arr, returnColumnSizes, returnSize, level - 1, i);
	addLevelOrder(root->right, arr, returnColumnSizes, returnSize, level - 1, i);
}
int** levelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes){
	int h = height(root);
   int **ret = NULL;
   int *pcol = NULL;
   int *pret = NULL;
   
   if(height == 0) {
      *returnSize = 0;
      *returnColumnSizes = NULL;
      return NULL;
   }
   
	ret = (int**)calloc(sizeof(int*), h);
   
	*returnColumnSizes = calloc(sizeof(int), h);
	*returnSize = 0;
	
	for(int i = 0; i < h; i++) {
		addLevelOrder(root, &ret, returnColumnSizes, returnSize, i, i);
      *returnSize = *returnSize +1;
	}

	return ret;
}
int main()
{
   struct TreeNode t1, t2, t3, t4, t5, t6, t7;
   t1.val = 1;
   t2.val = 2;
   t3.val = 3;
   t4.val = 4;
   t5.val = 5;
   t6.val = 6;
   t7.val = 7;

   t1.left = &t2;
   t1.right = &t3;   
   
   t2.left = &t4;
   t2.right = &t5;

   t3.left = &t6;
   t3.right = &t7;


   t4.left = t4.right = NULL;
   t5.left = t5.right = NULL;
   t6.left = t6.right = NULL;
   t7.left = t7.right = NULL;

   int retSize, *reCol, **p;
   p = levelOrder(&t1, &retSize, &reCol);

   //printf("DONE\n");

   for(int i = 0; i < retSize; i++) {
      //printf("ColumnSize %d\n", reCol[i]);
      int cz = reCol[i], *pret;
      pret = p[i];
      for(int j = 0; j < cz; j++) {
         printf("%d ", pret[j]);
      }
      free(p[i]);
      printf("\n");
   }
   free(reCol);
   

   return 0;

      
}




