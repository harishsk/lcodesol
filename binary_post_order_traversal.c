#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

#include "utstack.h"
#include "utarray.h"

typedef struct el_tag {
	struct TreeNode* tnode;
	struct el_tag *next;
}el_t;

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* postorderTraversal(struct TreeNode* root, int* returnSize){
	struct TreeNode *cur = NULL;
	el_t *st = NULL, *sel = NULL;
	UT_array *nums = NULL;
	int *ret = NULL, *p, i;

	utarray_new(nums, &ut_int_icd);
   
   cur = root;
   
	while(cur != NULL || !STACK_EMPTY(st)) {
		/* Push all the left node to stack */
		while(cur != NULL) {
         if(cur->right != NULL) {
            sel = malloc(sizeof(el_t));
            sel->tnode = cur->right;
            STACK_PUSH(st, sel);
         }
			sel = malloc(sizeof(el_t));
			sel->tnode = cur;
			STACK_PUSH(st, sel);

         cur = cur->left;
		}
      STACK_POP(st, sel);
      cur = sel->tnode;
      free(sel);

      if(cur->right &&
         STACK_TOP(st) != NULL &&
         cur->right == STACK_TOP(st)->tnode) {
         
         STACK_POP(st, sel);
         sel->tnode = cur;
         STACK_PUSH(st, sel);
         cur = cur->right;
      }else {
         utarray_push_back(nums, &cur->val);
         cur = NULL;

      }
	}
	*returnSize = utarray_len(nums);
	ret = malloc(sizeof(int)*(*returnSize));
	
	i = 0;
	for(p=(int*)utarray_front(nums); p != NULL; p=(int*)utarray_next(nums,p), i++) {
      ret[i] = *p;
	}
	utarray_free(nums);
	return ret;
}


int main()
{
   int *p, retSize;
   struct TreeNode t1, t2, t3, t4, t5, t6, t7;
   t1.val = 1;
   t2.val = 2;
   t3.val = 3;
   t4.val = 4;
   t5.val = 5;
   t6.val = 6;
   t7.val = 7;

   t1.left = &t2;
   t1.right = &t3;

   t2.left = &t4;
   t2.right = &t5;

   t3.left = &t6;
   t3.right = &t7;

   t4.left = t4.right = NULL;
   t5.left = t5.right = NULL;
   t6.left = t6.right = NULL;
   t7.left = t7.right = NULL;   

   p = postorderTraversal(&t1, &retSize);

   for(int i = 0; i < retSize; i++) {
      printf("%d ", p[i]);
   }
   printf("\n");
   free(p);
   return 0;
}
