void add_back(char ***ret, int *rS, int a, int b) {
	*ret = realloc(*ret, sizeof(char*) * (*rS + 1));
	(*ret)[*rS] = malloc(100);
	int fwd = 0;
	if(a == b) {
		fwd = sprintf((*ret)[*rS] ,"%d", a);
	}else {
		fwd = sprintf((*ret)[*rS] ,"%d->%d", a, b);
	}
	(*ret)[*rS][fwd] = '\0';
	*rS += 1;
}
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
char ** summaryRanges(int* nums, int numsSize, int* returnSize){
	*returnSize = 0;
	char **ret = NULL;
	if(nums == NULL || numsSize == 0)
		return ret;

	int key = nums[0];
	int prev = key, i;
	for(i = 1; i < numsSize; i++) {
		if(nums[i] == prev + 1) {
			prev = nums[i];
			continue;
		}
		add_back(&ret, returnSize, key, nums[i - 1]);
		key = nums[i];
		prev = key;
	}
    add_back(&ret, returnSize, key, nums[i - 1]);
	return ret;
}



