/* reveres n node takes care of rejoining head->next */
struct ListNode* reverse(struct ListNode* head, int n){
    struct ListNode* cur = head, *prev = NULL, *next = NULL;    
    while(n--){
        next = cur->next;
        cur->next = prev;
        prev = cur;
        cur = next;
    }    
    head->next = next;
    return prev;
}

struct ListNode* reverseBetween(struct ListNode* head, int m, int n){
    /* NULL link list, cannot be revered */
    if(head == NULL)
        return head;
    /* Reversing at one node, reverse link list is same */
    if(m == n)
        return head;
    
    int idx = 1;
    struct ListNode* cur = head, *prev = NULL;
    
    /* Move forward to m node */
    while(idx < m) {
        prev = cur;
        cur = cur->next;
        idx++;
    }
    /* Reverse m- m + 1 nodes from cur */
    if(prev == NULL) {
        prev = reverse(cur, n - m + 1);
        return prev;
    }else {
        prev->next = reverse(cur, n - m + 1);
        return head;
    }
}
