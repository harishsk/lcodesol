void minDepth_r(struct TreeNode*root, int height, int *min_depth) {
	if(root == NULL) {
		if(*min_depth < height)
			*min_depth = height;
		return;
	}
	minDepth_r(root->left, height + 1, min_depth);
	minDepth_r(root->right, height + 1, min_depth);
	return;
}
int minDepth(struct TreeNode* root){
	int min_depth = INT_MAX;
	minDepth_r(root, 0, &min_depth);
	return min_depth;
}

