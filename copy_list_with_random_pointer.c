#include <stdio.h>
#include <stdlib.h>
struct Node {
	 int val;
	 struct Node *next;
	 struct Node *random;
};

# if defined(FIRST_TRY)
struct Node *createNode(struct Node* node) {
	struct Node* new_node = malloc(sizeof(struct Node));
	new_node->val = node->val;
	new_node->random = NULL;
	return new_node;
}
struct Node* copyRandomList(struct Node* head) {
	if(head == NULL) 
		  return NULL;
	struct Node *cur = head, *new_cur = NULL;
	
	/* Value is copied to next node */
	while(cur != NULL) {
		new_cur = createNode(cur);
		struct Node* n = cur->next;
		cur->next = new_cur;
		new_cur->next = n;
		cur = n;
	}
	/* Random pointer copied */
	cur = head;
	while(cur != NULL) {
		if(cur->random != NULL) 
			cur->next->random = cur->random->next;
		else
			cur->next->random = NULL;
		cur = cur->next->next;
	}
	/* Deinterlace the link list */
	cur = head;
	struct Node* new_head = cur->next;
	new_cur = cur->next;
	while(cur != NULL) {
		cur->next = cur->next->next;
		if(new_cur->next != NULL)
			new_cur->next = new_cur->next->next;
		cur = cur->next;
		new_cur = new_cur->next;
	}
	return new_head;
}
#else

/* Utility function that will create new node
   with same val as parameter passed to it */

struct Node* createNode(struct Node*n) {
   struct Node* new_node = malloc(sizeof(struct Node));
   new_node->val = n->val;
   new_node->next = NULL;
   new_node->random = NULL;
   return new_node;
}

struct Node* copyRandomList(struct Node* head) {
   /* Nothing to copy if empty list */
	if(head == NULL)
		return NULL;
   
   
   /* NH is dummy head node with NH.next pointing 
      to head of merged list */
	struct Node NH;
   /* cur will pointer used to traverse the merged list */
   struct Node *cur = NULL;
   /* p will traverse the orginal list pointed by head */
   struct Node *p;
   
	/* Duplicate each node and create merged list 
      which is twice as long as original list*/   
	NH.next = head;
	cur = &NH;    
    p = head;
	while(p) {
		cur->next = p;
		cur = cur->next;
      
		struct Node *nxt = p->next;
		cur->next = createNode(p);
		cur = cur->next;

		p = nxt;
	}
	
	/* Link Random pointers, w.r.to adjacent node */
	p = head;
	while(p) {
		if(p->random != NULL) 
			p->next->random = p->random->next;
		p = p->next->next;
	}

   /* RH is dummy head with RH.next pointing to head of 
      resultant de interlaced node */
   struct Node RH;
   
	/* De-interlace alternate node */
   p = &RH;
   RH.next = NULL;
	while(head) {
		p->next = head->next;
        p = p->next;
		head = head->next->next;
	}
	return RH.next;
}

#endif
int main(){
	struct Node a, b, c, d;
	a.val = 100;
	b.val = 200;
	c.val = 300;
	d.val = 400;
	a.next = &b;
	b.next = &c;
	c.next = &d;
	d.next = NULL;
	a.random = &c;
	b.random = NULL;
	c.random = &d;
	d.random = &a;

	struct Node *p = copyRandomList(&a);

	return 0;
}
	
