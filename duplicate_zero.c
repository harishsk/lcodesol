#include <stdio.h>
#define TEST
#if defined(TEST)
void print_array(int *p, int n) {
   for(int i = 0; i < n; i++) 
      printf("%d ", p[i]);
   printf("\n");
   return;
}
#endif

void duplicateZeros(int* arr, int arrSize){
#if defined(TEST)   
   print_array(arr, arrSize);
#endif
	for(int i = 0; i < arrSize - 1; i++) {
		if(arr[i] == 0) {
         int j;
			for(j = arrSize - 2; j > i; j--) {
				arr[j + 1] = arr[j];
#if defined(TEST)            
            print_array(arr, arrSize);
#endif
			}
         
			arr[i + 1] = 0;
			i++;
		}
	}
	return;
}

#if defined(TEST)
// Driver function
int main(){
   int a[] = {1,0,2,3,0,4,5,0};
   duplicateZeros(a, sizeof(a)/sizeof(a[0]));
   print_array(a, sizeof(a)/sizeof(a[0]));
   return 0;
}
#endif
