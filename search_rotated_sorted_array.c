int find_rotate_offset(int *nums, int numSize) {
   int low = 0, high = numSize -1, mid;
	
	while(low < high) {
		mid = (low + high)/2;
		if(nums[mid] > nums[high]) {
			low = mid + 1;
		}else {
			high = mid ;
		}
	}
	return low;
}
int search(int* nums, int numsSize, int target){
#define RI(x) (((x) + rot_offset) % numsSize)
	int low, mid, high;
	int rot_offset = 0;
	rot_offset = find_rotate_offset(nums, numsSize);
	bool found = false;
	
	low = 0;
	high = numsSize -1;
	
	while(low <= high) {
		mid = low + (high - low)/2;
		if(nums[RI(mid)] == target) {
			found = 1;
			break;
		}else if(nums[RI(mid)] > target) {
			high = mid - 1;
		}else {
			low = mid + 1;
		}
	}
	if(found) {
		return RI(mid);
	}else {
		return -1;
	}
#undef RI
}

