/* Link: https://leetcode.com/problems/maximal-rectangle/
*/

/* Problem:
Given a rows x cols binary matrix filled with 0's and 1's, find the largest rectangle containing only 1's and return its area.

Example 1:
Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
Output: 6
Explanation: The maximal rectangle is shown in the above picture.

Example 2:

Input: matrix = []
Output: 0

Example 3:

Input: matrix = [["0"]]
Output: 0

Example 4:

Input: matrix = [["1"]]
Output: 1

Example 5:

Input: matrix = [["0","0"]]
Output: 
*/
// NOT_SOLVED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

/* Solution: */
#define DYNAMIC_PROGRAMMING

/* DYNAMIC_PROGRAMMING */
#define MIN(a,b) ((a) < (b) ? (a) : (b))
//#define MIN(a, b, c) (MIN_TWO(a,MIN_TWO(b,c)))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

int maximalRectangle(char** matrix, int matrixSize, int* matrixColSize){
   if(matrix == NULL || matrixSize < 1 || *matrixColSize < 1)
      return 0;
   int rowMax = matrixSize, colMax = *matrixColSize;

   int **dp, max = 0;
   dp = malloc(sizeof(int*) * rowMax);
   for(int row = 0; row < rowMax; row++) {
      dp[row] = malloc(sizeof(int) * colMax);
   }   
   
   for(int row = 0; row < rowMax; row++)
      dp[row][0] = matrix[row][0] -'0';

   for(int col = 0; col < colMax; col++)   
      dp[0][col] = matrix[0][col] -'0';
   
    for(int row = 1; row < rowMax; row++){
        for(int col = 1; col < colMax; col++){
            if(matrix[row][col]=='1')
                dp[row][col] = MAX(dp[row-1][col], dp[row][col-1])+1;
            else
                dp[row][col] = 0;
        }
    }
    for(int row = 1; row < rowMax; row++){
       for(int col = 1; col < colMax; col++){
          max = MAX(max, dp[row][col]);
       }
    }
    return max * max;
}


/* Test Driver: */
int main() {
   char a0[] = {'1', '0', '1', '0', '0'};
   char a1[] = {'1', '0', '1', '1', '1'};
   char a2[] = {'1', '1', '1', '1', '1'};
   char a3[] = {'1', '0', '0', '1', '0'};

   char *a[] = {a0, a1, a2, a3};
   int colS = sizeof(a0) / sizeof(a0[0]);

   printf("Maximum Rectangle is %d \n", maximalRectangle(a, sizeof(a)/sizeof(a[0]), &colS));
   
   return  0;
}
