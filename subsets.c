#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

int create_subsets(int* nums, int numsSize, bool *bin_map, int***arr, int i) {
	int count = 0;
	for(int itr = 0; itr < numsSize; itr++) {
		if(bin_map[itr] == true) {
			(*arr)[i] = (int*)realloc((*arr)[i], sizeof(int) * (count + 1));
			(*arr)[i][count] = nums[itr];
			count = count + 1;
		}
	}
	return count;
}
void increment_bin_map(bool *bin_map, int numsSize) {
	int itr = 0, csum = 1;
	for(itr = 0; itr < numsSize; itr++) {
		csum = csum + bin_map[itr];
		bin_map[itr] = csum % 2;
		csum = csum / 2;
	}
}
int** subsets(int* nums, int numsSize, int* returnSize, int** returnColumnSizes){
	int **ret = NULL;
	if(nums == NULL || numsSize == 0) {
		*returnSize = 1;
      *returnColumnSizes = malloc(sizeof(int));
      (*returnColumnSizes)[0] = 0;
      ret = malloc(sizeof(int*));
      ret[0] = NULL;
		return ret;
	}

   bool bin_map[numsSize];
	int max_comb = pow(2, numsSize), itr = 0, sub_num;
    
	memset(bin_map, 0, sizeof(bin_map));

	ret = malloc(sizeof(int*)*max_comb);
	*returnColumnSizes = malloc(sizeof(int)*max_comb);
	*returnSize = max_comb;
	for(itr = 0; itr < max_comb; itr++) {
		ret[itr] = NULL;
		sub_num = create_subsets(nums, numsSize, bin_map, &ret, itr);
		increment_bin_map(bin_map, numsSize);
		(*returnColumnSizes)[itr] = sub_num;
	}
	return ret;
}

int main()
{
   int a[] = {1,2,3};
   int **sub_arr, *col, rs;

   sub_arr = subsets(a, sizeof(a)/sizeof(a[0]), &rs, &col);

   return 0;
}

