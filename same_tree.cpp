#include <bits/stdc++.h>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
class Solution {
   /* Recursive function of solution function */
   void isSameTree_r(TreeNode* p, TreeNode *q, bool& same) {
      /* If tree is same no point in continuing */
      if(same == false) return;
      /* If p and q are nullptr we have reached left node
         so dont continue */
      if(p == nullptr and q == nullptr) return;

      /* If any of p/q is nullptr other not then tree is not same 
         If the val in p and q are not equal then tree is not same */
      if((p == nullptr and q != nullptr)
         or (p != nullptr and q == nullptr)
         or (p->val != q->val)){
         same = false;
         return;
      }
      /* Continue down the tree */
      isSameTree_r(p->left, q->left, same);
      isSameTree_r(p->right, q->right, same);
      return;
   }
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
       bool isSame= true;
       this->isSameTree_r(p, q, isSame);
       return isSame;
    }
};
int main() {
   TreeNode t4(4), t5(5), t6(6), t7(9);
   TreeNode t2(2, &t4, &t5); 
   TreeNode t3(3, &t6, &t7);  
   TreeNode t1(1, &t2, &t3);

   TreeNode x4(4), x5(5), x6(6), x7(7);
   TreeNode x2(2, &x4, &x5); 
   TreeNode x3(3, &x6, &x7);  
   TreeNode x1(1, &x2, &x3);   

   Solution s;


   cout << s.isSameTree(&x1, &t1) << "\n";
   
}

