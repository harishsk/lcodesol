/* QUEUE IMPLEMENTATION	 START*/
typedef struct queue_node_tag {
	int p;
	struct queue_node_tag *next;
}queue_node_t;

typedef struct queue_tag {
	queue_node_t *head;
	queue_node_t *tail;
	int count;
}queue_t;

static inline queue_t * init_queue(){
	queue_t *q = NULL;
	q = malloc(sizeof(queue_t));
	q->head = NULL;
	q->tail = NULL;
	q->count = 0;
	return q;
}
static inline void cleanup_queue(queue_t *q){
	free(q);
}

static inline void en_queue(queue_t *q, int p){
	queue_node_t *t = malloc(sizeof(queue_node_t));
	
	t->p = p;
	t->next = NULL;
	
	if(q->head == NULL) 
		q->head = t;
	
	if(q->tail == NULL){
		q->tail = t;
	}else {
		q->tail->next = t;
		q->tail = t;		
	}
	q->count++;
}

static inline int de_queue(queue_t *q){
	int ret;
	queue_node_t *t = q->head;

	if(q->head == NULL)
		return 0;
	
	ret = q->head->p;
	q->head = q->head->next;
	if(q->head == NULL)
		q->tail = NULL;
	
	free(t);
	q->count--;
	
	return ret;
}

static inline int count_queue(queue_t *q) {
	return q->count;
}
static inline bool is_empty_queue(queue_t *q) {
	return q->count == 0;
}

typedef struct {
    queue_t *st;
    queue_t *stby;
    int cnt;
} MyStack;

/** Initialize your data structure here. */

MyStack* myStackCreate() {
    MyStack* s = malloc(sizeof(MyStack));
    s->st = init_queue();
    s->stby = init_queue();    
    s->cnt = 0;
    return s;
}

/** Push element x onto stack. */
void myStackPush(MyStack* obj, int x) {
    en_queue(obj->st, x);
    obj->cnt++;
}

/** Removes the element on top of the stack and returns that element. */
int myStackPop(MyStack* obj) {
    if(obj->cnt <= 0)
        return 0;
    for(int i = 0; i < (obj->cnt - 1); i++){
        en_queue(obj->stby, de_queue(obj->st));
    }
    int ret = de_queue(obj->st);
    queue_t *temp = obj->st;
    obj->st = obj->stby;
    obj->stby = temp;
    obj->cnt--;
    return ret;
}

/** Get the top element. */
int myStackTop(MyStack* obj) {
    if(obj->cnt <= 0)
        return 0;
    for(int i = 0; i < (obj->cnt - 1); i++){
        en_queue(obj->stby, de_queue(obj->st));
    }
    int ret = de_queue(obj->st);
    en_queue(obj->stby, ret);
    queue_t *temp = obj->st;
    obj->st = obj->stby;
    obj->stby = temp;    
    return ret;
}

/** Returns whether the stack is empty. */
bool myStackEmpty(MyStack* obj) {
   return is_empty_queue(obj->st);
}

void myStackFree(MyStack* obj) {
    while(!is_empty_queue(obj->st)){
        de_queue(obj->st);
    }
    cleanup_queue(obj->st);
    cleanup_queue(obj->stby);
    free(obj);
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);
 
 * int param_2 = myStackPop(obj);
 
 * int param_3 = myStackTop(obj);
 
 * bool param_4 = myStackEmpty(obj);
 
 * myStackFree(obj);
*/
