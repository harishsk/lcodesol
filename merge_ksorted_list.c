#if defined(ITERATIVE)
struct ListNode* mergeKLists(struct ListNode** lists, int listsSize){
	struct ListNode dummy_head;
	struct ListNode *cur = &dummy_head;
	bool done = false;

    cur->next = NULL;
    
	while(!done) {
		struct ListNode *s = NULL;
		int s_val = INT_MAX;
		int s_idx = 0;
		done = true;
		for(int i = 0; i < listsSize; i++) {
			if(lists[i] != NULL) {
				if(lists[i]->val < s_val) {
					s_val = lists[i]->val;
					s = lists[i];
					s_idx = i;
				}
				done = false;
			}
		}
        if(!done) {
            cur->next = s;
		    lists[s_idx] = lists[s_idx]->next;
		    cur = cur->next;
        }
	}
	return dummy_head.next;
}

#else /* MERGE TWO */
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list1){
   struct ListNode dummy_head;
   struct ListNode*cur = &dummy_head;
   cur->next = NULL;

   while(list1 != NULL && list2 != NULL) {
      if(list1->val < list2->val) {
         cur->next = list1;
         list1 = list1->next;
      }else {
         cur->next = list2;
         list2 = list2->next;         
      }
      cur = cur->next;
   }
   if(list1 != NULL) {
      cur->next = list1;
   }else if(list2 != NULL) {
      cur->next = list2;
   }
   return dummy_head.next;
}
struct ListNode* mergeKLists(struct ListNode** lists, int listsSize){
   if(lists == NULL || listsSize < 1)
      return NULL;
   
	struct ListNode *cur = NULL;
	cur = lists[0];

   for(int i = 1; i < listsSize; i++) {
      cur = mergeTwoLists(cur, lists[i]);
   }
   return cur;
}
#endif
