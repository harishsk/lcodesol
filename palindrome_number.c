/*
https://leetcode.com/problems/palindrome-number/

Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:

Input: 121
Output: true
Example 2:

Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
Example 3:

Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#if str_implementation

bool strpal(char *s)
{
   int i = 0, n;
   n = strlen(s);
   for(i = 0; i < n/2; i++) {
      if(s[i] != s[n-i-1]) {
         return false;
      }
   }
   return true;
}


bool isPalindrome(int x){
  char str_x[128] = {0};
  long r = 0;
  
  snprintf(str_x, 128, "%d", x);
  
  if(str_x[0] == '-') {
    return false;
  }
  
  if(strpal(str_x))
    return true;
  else 
    return false;
}
#else
bool isPalindrome(int x){

   /* All negetive numbers are not palindrome*/
   if(x < 0) {
      return false;
   }
   long rx = 0, tx = x;
   while(tx) {
      rx = rx * 10;
      rx = rx + tx%10;
      tx = tx/10;
   }
   return x == (int)rx;
}
#endif

int main()
{
   int x = 121123;

   printf("%d is palindrome? - %s\n", x, isPalindrome(x)? "true":"false");

   return 0;
}
