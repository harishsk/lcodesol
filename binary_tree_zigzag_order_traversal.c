#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
};
#if defined(OLD_METHOD)
/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
int height_tree(struct TreeNode* root) {
	int rh = 0, lh = 0;
	if(root == NULL)
		return 0;

	lh = height_tree(root->left) + 1;
	rh = height_tree(root->right) + 1;

	return (lh > rh ? lh : rh);
}
void print_tree_level(struct TreeNode* root, int level, bool order) {
	if(level == 1)	 {
		printf("%d ", root->val);
		return;
	}

	if(order) {
		if(root->left) print_tree_level(root->left, level-1, order);
		if(root->right) print_tree_level(root->right, level-1, order);
	} else {
		if(root->right) print_tree_level(root->right, level-1, order);
		if(root->left) print_tree_level(root->left, level-1, order);
	}	
}

int** zigzagLevelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes){
	int h = 0;
	
	h = height_tree(root);	 

	for(int i = 1; i <= h; i++) {
		print_tree_level(root, i, i%2);
		printf("\n");
	}
}
#else

/* QUEUE IMPLEMENTATION	 START*/
typedef struct queue_node_tag {
	void *p;
	struct queue_node_tag *next;
}queue_node_t;

typedef struct queue_tag {
	queue_node_t *head;
	queue_node_t *tail;
	int count;
}queue_t;

static inline queue_t * init_queue()
{
	queue_t *q = NULL;
	q = malloc(sizeof(queue_t));
	q->head = NULL;
	q->tail = NULL;
	q->count = 0;
	return q;
}
static inline void cleanup_queue(queue_t *q)
{
	free(q);
}

static inline void en_queue(queue_t *q, void *p)
{
	queue_node_t *t = malloc(sizeof(queue_node_t));
	
	t->p = p;
	t->next = NULL;
	
	if(q->head == NULL) 
		q->head = t;
	
	if(q->tail == NULL){
		q->tail = t;
	}else {
		q->tail->next = t;
		q->tail = t;		
	}
	q->count++;
}
static inline void insert(queue_t *q, void *p)
{
	queue_node_t *t = malloc(sizeof(queue_node_t));
	
	t->p = p;
	t->next = NULL;	
	
	if(q->head == NULL) {
		q->head = t;
	}else {
		t->next = q->head;
		q->head = t;
	}
	
	if(q->tail == NULL)
		q->tail = t;
	
	q->count++;
}


static inline void* de_queue(queue_t *q)
{
	void *ret;
	queue_node_t *t = q->head;

	if(q->head == NULL)
		return NULL;
	
	ret = q->head->p;
	q->head = q->head->next;
	if(q->head == NULL)
		q->tail = NULL;
	
	free(t);
	q->count--;
	
	return ret;
}

static inline int count_queue(queue_t *q) {
	return q->count;
}
static inline bool is_empty_queue(queue_t *q) {
	return q->count == 0;
}
void add_back(int ***ret, int *rS, int **rCS, int level, int val) {
	if(level > *rS) {
		*ret = realloc(*ret, sizeof(int*) * (*rS + 1));
		(*ret)[*rS] = NULL;
		*rCS = realloc(*rCS, sizeof(int) * (*rS + 1));
		(*rCS)[*rS] = 0;
		(*ret)[*rS] = realloc((*ret)[*rS], sizeof(int) * ((*rCS)[*rS] + 1));
		(*ret)[*rS][(*rCS)[*rS]] = val;
		(*rCS)[*rS] += 1;
		*rS += 1;
	}else {
		int row = *rS - 1;
		(*ret)[row] = realloc((*ret)[row], sizeof(int) * ((*rCS)[row] + 1));
		(*ret)[row][(*rCS)[row]] = val;
		(*rCS)[row] += 1;
	}
	return;
}
void zigzagLevelOrder_r(struct TreeNode* root, int ***ret, 
								int *rS, int **rCS) {
	
	if(root == NULL)
		return;
	
	queue_t *q = init_queue();
	
	en_queue(q, root);
	
	int level = 1;
	while(!is_empty_queue(q)) {
		int cnt = count_queue(q);
		queue_t *qn = init_queue();
		for(int i = 0; i < cnt; i++) {
			struct TreeNode* node = de_queue(q);
			if((level % 2) == 0) {
				if(node->right)
					insert(qn, node->right);
				if(node->left)
					insert(qn, node->left);				  
			}else {
				if(node->left)
					insert(qn, node->left);
				if(node->right)
					insert(qn, node->right);
			}
			add_back(ret, rS, rCS, level, node->val);
		}
		cleanup_queue(q);		  
		q = qn;
		level++;
	}
	cleanup_queue(q);
}
int** zigzagLevelOrder(struct TreeNode* root, int* returnSize, 
							  int** returnColumnSizes){
	int **ret = NULL;
	*returnSize = 0;
	*returnColumnSizes = NULL;
	zigzagLevelOrder_r(root, &ret, returnSize, returnColumnSizes);
	return ret;
}

#endif

int main()
{
	struct TreeNode t1, t2, t3, t4, t5, t6, t7;
	t1.val = 1;
	t2.val = 2;
	t3.val = 3;
	t4.val = 4;
	t5.val = 5;
	t6.val = 6;
	t7.val = 7;

	t1.left = &t2;
	t1.right = &t3;

	t2.left = &t4;
	t2.right = &t5;

	t3.left = &t6;
	t3.right = &t7;

	t4.left = t4.right = NULL;
	t5.left = t5.right = NULL;
	t6.left = t6.right = NULL;
	t7.left = t7.right = NULL;

	int S, *CS;
	int **p = zigzagLevelOrder(&t1, &S, &CS);

	printf("Size is %d\n", S);
	
	for(int i = 0; i < S; i++) {
		for(int c = 0; c < CS[i]; c++) {
			printf("%d ", p[i][c]);
		}
		printf("\n");
		free(p[i]);
	}
	free(p);
	free(CS);

	return 0;

		
}
