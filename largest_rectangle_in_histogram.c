#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "utstack.h"

typedef struct intstack_tag {
   int val;
   int idx;
   struct intstack_tag *next;
}intstack_t;

#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) < (y) ? (x) : (y))

int largestRectangleArea(int* heights, int heightsSize){
   if(heightsSize < 1 || heights == NULL)
      return 0;
   
   intstack_t *istack = NULL, *el = NULL;
   int max = INT_MIN, hi;

   el = malloc(sizeof(intstack_t));
   el->val = heights[0];
   el->idx = 0;
   STACK_PUSH(istack, el);
   
   for(hi = 1; hi < heightsSize; hi++) {
      max = MAX(max, heights[hi]);
      while(!STACK_EMPTY(istack) && heights[hi] < STACK_TOP(istack)->val) {
         STACK_POP(istack, el);
         max = MAX(max, el->val * (hi - el->idx));
         free(el);
      }
      if(!STACK_EMPTY(istack) && heights[hi] == STACK_TOP(istack)->val) {
         continue;
      }

      el = malloc(sizeof(intstack_t));
      el->val = heights[hi];
      el->idx = hi;
      STACK_PUSH(istack, el);
   }
   while(!STACK_EMPTY(istack)) {
      STACK_POP(istack, el);
      max = MAX(max, MIN(heights[hi], el->val) * (hi - el->idx));
      free(el);
   }
   return max;
   
}

int main(){
   int h[] = {2,1,5,6,2,3};
   printf("Size of rectangle %d\n", largestRectangleArea(h, sizeof(h)/sizeof(h[0])));
   return 0;
}

