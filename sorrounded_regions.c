bool is_covered(char** board, int row, int col, int rowMax, int colMax){
    //Go UP
    int found = false, r, c;
    
    for(r = row - 1; r >= 0; r--){
        if(board[r][col] == 'X') {
            found = true;
            break;
        }
    }
    if(!found)
        return false;
    
    //Go RIGHT
    found = false;
    for(c = col + 1; c < colMax; c++){
        if(board[row][c] == 'X') {
            found = true;
            break;
        }
    }
    if(!found)
        return false;
    //Go DOWN
    found = false;
    for(r = row + 1; r < rowMax; r++){
        if(board[r][col] == 'X') {
            found = true;
            break;
        }
    }
    if(!found)
        return false;
    //Go LEFT
    found = false;
    for(c = col - 1; c >= 0; c--){
        if(board[row][c] == 'X') {
            found = true;
            break;
        }
    }
    if(!found)
        return false;
    
    return true;
}


void solve(char** board, int boardSize, int* boardColSize){
    if(board == NULL || boardSize < 3 || *boardColSize < 3)
        return;
    printf(" %d %d \n", boardSize, *boardColSize);
    for(int row = 0; row < boardSize; row++) {
        for(int col = 0; col < *boardColSize; col++) {
            if(board[row][col] == 'O' && is_covered(board, row, col, boardSize, *boardColSize)) {
               board[row][col] = 'P';
            }
        }
    }
    for(int row = 0; row < boardSize; row++) {
        for(int col = 0; col < *boardColSize; col++) {
            if(board[row][col] == 'P') {
               board[row][col] = 'X';
            }
        }
    }
    return;
}
