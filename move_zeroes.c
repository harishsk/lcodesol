/* Link:
https://leetcode.com/problems/move-zeroes/   
*/

/* Problem:
Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Example:

Input: [0,1,0,3,12]
Output: [1,3,12,0,0]
Note:

You must do this in-place without making a copy of the array.
Minimize the total number of operations.
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
void moveZeroes(int* nums, int numsSize){
   int i, k;
   
   if(numsSize <= 1) {
      return;
   }
   for(i = 0, k = 0; i < numsSize; i++) {
      if(nums[i] != 0) {
         nums[k] = nums[i];
         k++;
      }
   }
   for(; k < numsSize; k++) {
      nums[k] = 0;
   }
   return;
}
#if defined(TEST)
void print_array(int *p, int n) {
   for(int i = 0; i < n; i++) 
      printf("%d ", p[i]);
   printf("\n");
   return;
}
#endif

/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = {0,0,0,0,1};
   moveZeroes(a, sizeof(a)/sizeof(a[0]));
   print_array(a, sizeof(a)/sizeof(a[0]));
   return 0;
}
#endif
