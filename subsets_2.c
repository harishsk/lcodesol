#include <uthash.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

struct my_struct {
    int num;
    int time;
    UT_hash_handle hh;
};

struct my_struct *nhash = NULL;

void duplicate_subsets(int***arr, int *returnSize, int** returnColumnSizes, int add) {
   int itr ;
   *arr = realloc(*arr, sizeof(int*) * (*returnSize + 1));
   *returnColumnSizes = realloc(*returnColumnSizes, sizeof(int) * (*returnSize + 1));
   (*arr)[*returnSize] = (int*)malloc(sizeof(int) * ((*returnColumnSizes)[*returnSize - 1] + 1));
   memcpy((*arr)[*returnSize], (*arr)[*returnSize - 1], (*returnColumnSizes)[*returnSize]);
   (*arr)[*returnSize][(*returnColumnSizes)[*returnSize]] = add;
   (*returnColumnSizes)[*returnSize] = count;
   *returnSize = *returnSize + 1;
}

void create_subsets(struct my_struct *nhash, int numsSize, bool *bin_map, int***arr,
                    int *returnSize, int** returnColumnSizes) {
	int count = 0, itr, time;
   struct my_struct *hel, *tmp;
   *arr = realloc(*arr, sizeof(int*) * (*returnSize + 1));
   *returnColumnSizes = realloc(*returnColumnSizes, sizeof(int) * (*returnSize + 1));
   itr = 0;
   HASH_ITER(hh, nhash, hel, tmp) {
		if(bin_map[itr] == true) {
         for(time = 0; time < hel->time; time++) {
            duplicate_subsets(arr, returnSize, returnColumnSizes, hel->num);
         }
			(*arr)[*returnSize] = (int*)realloc((*arr)[*returnSize], sizeof(int) * (count + 1));
			(*arr)[*returnSize][count] = hel->num;
			count = count + 1;
		}
      itr++;
	}
   (*returnColumnSizes)[*returnSize] = count;
   *returnSize = *returnSize + 1;
   return;
}
void increment_bin_map(bool *bin_map, int numsSize) {
	int itr = 0, csum = 1;
	for(itr = 0; itr < numsSize; itr++) {
		csum = csum + bin_map[itr];
		bin_map[itr] = csum % 2;
		csum = csum / 2;
	}
}
int** subsets(int* nums, int numsSize, int* returnSize, int** returnColumnSizes){
	int **ret = NULL;
	if(nums == NULL || numsSize == 0) {
		*returnSize = 1;
      *returnColumnSizes = malloc(sizeof(int));
      (*returnColumnSizes)[0] = 0;
      ret = malloc(sizeof(int*));
      ret[0] = NULL;
		return ret;
	}

   for(int itr = 0; itr < numsSize; itr++) {
      struct my_struct *hel;
      HASH_FIND_INT(nhash, &nums[itr], hel);
      if(hel != NULL) {
         hel->time++;
      }else {
         hel = (struct my_struct *)malloc(sizeof *hel);
         hel->num = nums[itr];
         hel->time = 1;
         HASH_ADD_INT(nhash, num, hel);
      }
   }

   int num_uniq = HASH_COUNT(nhash);
   bool bin_map[num_uniq];
	int max_comb = pow(2, num_uniq), itr = 0;
    
	memset(bin_map, 0, sizeof(bin_map));

   ret = NULL;
   *returnSize = 0;
	for(itr = 0; itr < max_comb; itr++) {
		create_subsets(nhash, num_uniq, bin_map, &ret,
                     returnSize, returnColumnSizes);
		increment_bin_map(bin_map, num_uniq);
	}
	return ret;
}

int main()
{
   int a[] = {1,2,2,2,3};
   int **sub_arr, *col = NULL, rs;

   sub_arr = subsets(a, sizeof(a)/sizeof(a[0]), &rs, &col);

   for(int i = 0; i < rs; i++) {
      for(int j = 0; j < col[i]; j++) {
         printf(" %d", sub_arr[i][j]);
      }
      printf("\n");
   }
   
   return 0;
}

