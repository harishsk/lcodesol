#include <stdio.h>
#include <stdlib.h>

struct ListNode {
   int val;
   struct ListNode *next;
};

struct ListNode * create_node(int val)
{
   struct ListNode *p = malloc(sizeof(struct ListNode));
   p->next = NULL;
   p->val = val;
   return p;
}

void append_node(struct ListNode *p, int a)
{
   while(p->next) {
      p = p->next;
   }
   p->next = create_node(a);

}

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}
struct ListNode* deleteDuplicates(struct ListNode* head){
	struct ListNode dummy;
	struct ListNode *first, *prev, *cur, *t;
	int free_prev = 0;
	dummy.next = head;
	
	if(head == NULL || head->next == NULL) 
		return head;
	
	first = &dummy;
	prev = head;
	cur = head->next;
   
	while(cur != NULL) {
		if(prev->val == cur->val) {
			prev->next = cur->next;
			t = cur;
			cur = cur->next;
			free(t);
			free_prev = 1;
		}else {
			if(free_prev == 1) {
				free_prev = 0;
				first->next = cur;
				free(prev);
            prev = first;            
			}else {
				first = prev;
				prev = cur;
				cur = cur->next;
         }
      }
   }
   if(free_prev) {
      first->next = cur;
      free(prev);
   }
	return dummy.next;
}

int main()
{
   struct ListNode *a;
   a = create_node(1);
   append_node(a, 2);
   append_node(a, 3);
   append_node(a, 3);
   append_node(a, 4);
   append_node(a, 4);   
   append_node(a, 5);   

   print_list(a);
   
   print_list(deleteDuplicates(a));

   return 0;
}

