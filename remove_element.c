/* 
https://leetcode.com/problems/remove-element/
Given an array nums and a value val, remove all instances of that value in-place and return the new length.

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

The order of elements can be changed. It doesn't matter what you leave beyond the new length.

Example 1:

Given nums = [3,2,2,3], val = 3,

Your function should return length = 2, with the first two elements of nums being 2.

It doesn't matter what you leave beyond the returned length.
Example 2:

Given nums = [0,1,2,2,3,0,4,2], val = 2,

Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.
*/

#include <stdio.h>

#ifdef first_trial
int removeElement(int* nums, int numsSize, int val){
   int i, j = 0, k;
   for(i = 0, k= numsSize - 1; i < numsSize - j; i++) {
      if(nums[i] == val){
         if(i < k) {
            nums[i] = nums[k];
            i--;
            j++;
         }
         k--;
      }
   }
   return k + 1;
}
#else
int removeElement(int* nums, int numsSize, int val){
   int low, high = numsSize - 1;
   for(low = 0, high = numsSize - 1; low <= high; low++) {
      if(nums[low] == val){
         nums[low] = nums[high];
         low--;
         high--;
      }
   }
   return high + 1;
}
#endif
int main()
{
   int a[] = {3,2,2,3};
   int n, i;

   n = removeElement(a, sizeof(a)/sizeof(int), 3);

   printf("Len of a is %d\n", n);
   for(i = 0; i < n; i++) {
      printf(" %d,", a[i]);
   }
   printf("\n");
   
}
