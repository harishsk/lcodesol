#define MIN(x, y) ((x) < (y) ? (x) : (y))
int get_spiral(int** matrix, int rsize, int csize, int start,
			int *arr, int arr_idx) {
	int ri ,ci, rel, cel;
	
	ri = start; 
    ci = start;
	cel = start + csize - 1;
	for(; ci <= cel; ci++)
		arr[arr_idx++] = matrix[ri][ci];
	
   if(rsize >= 2) {
      ci = start + csize - 1; 
      ri = start + 1;
      rel = start + rsize - 1;
      for(; ri <= rel; ri++) 
         arr[arr_idx++] = matrix[ri][ci];
    }

   if(csize >= 2 && rsize >= 2) {
      ri = start + rsize - 1;
      ci = start + csize - 2;
      cel = start;
      for(; ci >= cel; ci--) 
         arr[arr_idx++] = matrix[ri][ci];
    }
    
   if(rsize >= 3 && csize >=2) {
      ri = start + rsize - 2;
      ci = start;
      rel = start + 1;
      for(; ri >= rel; ri--) 
         arr[arr_idx++] = matrix[ri][ci];
   }
   return arr_idx;
}
int* spiralOrder(int** matrix, int matrixSize, int* matrixColSize, int* returnSize){
	int bound, bound_limit;
	int rsize, csize, *ret_arr = NULL, ret_arr_idx = 0;

	/* Trivial case - no rotation */
	if(matrixSize == 0 || matrix == NULL || *matrixColSize == 0) {
		*returnSize = 0;
		return NULL;
	}
	ret_arr = malloc(sizeof(int) * matrixSize * (*matrixColSize));
	if(matrixSize == 1 && *matrixColSize == 1) {
		ret_arr[0] = matrix[0][0];
		*returnSize = 1;
		return ret_arr;
	}
	bound_limit = MIN(matrixSize, *matrixColSize);
   ret_arr_idx = 0;
	for(bound = 0; bound < (bound_limit + 1)/2; bound++) {
		rsize = matrixSize - bound * 2;
		csize = (*matrixColSize) - bound * 2;
		ret_arr_idx = get_spiral(matrix, rsize, csize, bound, 
                               ret_arr, ret_arr_idx);
	}
   *returnSize = ret_arr_idx;
   return ret_arr;
}
