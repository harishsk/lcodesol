/**
 * Definition for a Node.
 * struct Node {
 *     int val;
 *     struct Node *left;
 *     struct Node *right;
 *     struct Node *next;
 * };
 */
int height(struct Node* root) {
   if(root == NULL)
      return 1;
   int lh = height(root->left) + 1;
   int rh = height(root->right) + 1;

   return (lh > rh ? lh : rh);
}
void connect_r(struct Node* root, int level, struct Node ***tailq) {
   if(root == NULL)
      return;
   
   if((*tailq)[level] == NULL) {
      (*tailq)[level] = root;
   }else {
      (*tailq)[level]->next = root;
      (*tailq)[level] = (*tailq)[level]->next;
   }
   connect_r(root->left, level + 1, tailq);
   connect_r(root->right, level + 1, tailq);   
}
struct Node* connect(struct Node* root) {
   if(root == NULL || root->next == NULL)
      return root;
   
   int h = height(root);
   struct Node **tailq = calloc(sizeof(struct Node*), h);
	connect_r(root, 0, &tailq);
   free(tailq);
   
   return root;   
}
