#include <stdio.h>

void print_array(int *p, int num)
{
   int i = 0;
   for(i = 0; i < num; i++)
      printf("%d ", p[i]);
   printf("\n");
}

void sortColors(int* nums, int numsSize){
    int start = 0;
    int end = numsSize -1;
    int index = 0;
    
    
    while(index <= end && start < end){
        if(nums[index] == 0){
            nums[index++] = nums[start];
            nums[start++] = 0;
        }
        else if(nums[index] == 2){
            nums[index] = nums[end];
            nums[end--] = 2; 
        }
        else{
            index++; 
        }
    }
}

int main()
{
   int a[] = {2,0,1};
   print_array(a, sizeof(a)/ sizeof(a[0]));
   sortColors(a, sizeof(a)/ sizeof(a[0]));
   print_array(a, sizeof(a)/ sizeof(a[0]));
      
}

