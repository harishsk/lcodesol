#include <iostream>
#include <stack>

using namespace std;

#if OLD_IMPLEMENTATION
class MyQueue {
   stack<int> stPush;
   stack<int> stPop;   
public:
    MyQueue() {
       
    }
    
    void push(int x) {
       stPush.push(x);
    }
    
    int pop() {
       if(!stPop.empty()){
          int t = stPop.top();
          stPop.pop();
          return t;
       }
       while(!stPush.empty()){
          int t = stPush.top();
          stPush.pop();
          stPop.push(t);
       }
       if(!stPop.empty()){
          int t = stPop.top();
          stPop.pop();
          return t;
       }
       return -1;       
    }
    
    int peek() {
       if(!stPop.empty()){
          return stPop.top();
       }
       while(!stPush.empty()){
          int t = stPush.top();
          stPush.pop();
          stPop.push(t);
       }
       if(!stPop.empty()){
          return stPop.top();
       }
       return -1;
    }
    
    bool empty() {
       return stPop.empty() && stPush.empty();
    }
};
#else /* NEW_IMPLEMENTATION */
class MyQueue {
       stack <int> s1,s2;
public: 
    MyQueue() {
    }
    
    void push(int x) {
        s1.push(x);
    }
    
    int pop() {
        while(!s1.empty()){
            s2.push(s1.top());
            s1.pop();
        }
        int pop = s2.top();
        s2.pop();
        while(!s2.empty()){
            s1.push(s2.top());
            s2.pop();
        }
        return pop;
    }
    
    int peek() {
        while(!s1.empty()){
            s2.push(s1.top());
            s1.pop();
        }
        int ans = s2.top();
        while(!s2.empty()){
            s1.push(s2.top());
            s2.pop();
        }
        return ans;
    }
    
    bool empty() {
        return s1.empty();
    }
};
#endif

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */
int main(){
   MyQueue *myQueue = new MyQueue();

   myQueue->push(1); // queue is: [1]
   myQueue->push(2); // queue is: [1, 2] (leftmost is front of the queue)
   cout << myQueue->peek() << endl; // return 1
   cout << myQueue->pop() << endl; // return 1, queue is [2]
   cout << myQueue->empty() << endl; // return false

   return 0;
}
