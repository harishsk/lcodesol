/* Link:
   
*/

/* Problem:
  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
bool hasCycle(struct ListNode *head) {
	if(head == NULL || head->next == NULL) 
		return false;
	struct ListNode *slow = head, *fast = head->next;
	bool found_cycle = false;
	
	while(fast != NULL && fast->next != NULL) {
		if(slow == fast) {
			found_cycle = true;
			break;
		}
		slow = slow->next;
		fast = fast->next->next;
	}
	return found_cycle;
}

/* Test Driver: */
#if defined(TEST)
int main() {

   
   return 0;
}
#endif
