

int findKthLargest(int* nums, int numsSize, int k){

    for(int i = numsSize - 1; i >= numsSize - k; i--) {
        int max = INT_MIN, max_idx;
        for(int j = 0; j <= i; j++) {
            if(nums[j] > max) {
                max = nums[j];
                max_idx = j;
            }            
        }
        int temp = nums[max_idx];
        nums[max_idx] = nums[i];
        nums[i] = temp;
    }
    return nums[numsSize - k];
}
