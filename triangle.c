/* 120. Triangle
https://leetcode.com/problems/triangle/

Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

For example, given the following triangle

[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]

The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

Note:

Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.

*/
#define MIN(x, y) ((x) < (y) ? (x) : (y))                                     
int minimumTotal(int** triangle, int triangleSize, int* triangleColSize){
   if(triangleSize == 0)
      return 0;
   for(int h = 1; h < triangleSize; h++) {
      triangle[h][0] = triangle[h][0] + triangle[h-1][0];
      int c_col_sz = triangleColSize[h] - 1;
      int p_col_sz = triangleColSize[h-1] - 1;
      triangle[h][c_col_sz] = triangle[h][c_col_sz] 
         + triangle[h -1][p_col_sz];
   }
   for(int h = 2; h < triangleSize; h++) {
      for(int l = 1; l < triangleColSize[h] - 1; l++) {
         triangle[h][l] = triangle[h][l] + MIN(triangle[h-1][l], triangle[h-1][l-1]);
      }
   }
   int min = INT_MAX;
   for(int l = 0; l < triangleColSize[triangleSize -1]; l++) {
      if(triangle[triangleSize -1][l] < min) {
         min = triangle[triangleSize -1][l];
      }
   }
   return min;
}
