#include <iostream>
//#include <queue>
#include <deque>
#include <vector>

using namespace std;

/*Definition for a binary tree node.*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> res;
        deque<TreeNode*> dq;
        if (!root) return res;
        dq.push_back(root);
        while(dq.size()) {
            int size = dq.size();
            vector<int> row;
            for(int i = 0; i < size; i++) {
                TreeNode* f = dq.front();
                dq.pop_front();
                row.push_back(f->val);
                if (f->left) dq.push_back(f->left);
                if (f->right) dq.push_back(f->right);
            }
            res.push_back(row);
        }
        return res;
    }
};

int main() {
   TreeNode t4(4), t5(5), t6(6), t7(7);
   TreeNode t2(2, &t4, &t5); 
   TreeNode t3(3, &t6, &t7);  
   TreeNode t1(1, &t2, &t3);

   Solution s1;

   vector<vector<int>> res;

   res = s1.levelOrder(&t1);

   for (int i = 0; i < res.size(); i++) {
        for (int j = 0; j < res[i].size(); j++)
            cout << res[i][j] << " ";
        cout << endl;
    }
}
