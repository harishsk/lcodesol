char * frequencySort(char * s){
	
	if(s == NULL)
		return NULL;

	int char_freq[255] = {0};
	int idx, max = INT_MIN, k = 0, ritr = 0;
	bool done = false;

	idx = 0;
	while(s[idx] != '\0') {
		char_freq[s[idx]]++;
		idx++;
	}

	while(!done) {
		max = INT_MIN;
		for(idx = 0; idx < 255; idx++) {
			if(char_freq[idx] > max) {
				max = char_freq[idx];
				max_char = idx;
				char_freq[idx] = 0;
			}
		}
		if(max == 0) {
			done = true;
		}else {
			for(idx = 0 ; idx < max; i++) {
				s[k++] = max_char;
			}
		}
	}
	return s;
}

