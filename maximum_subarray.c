/*
 https://leetcode.com/problems/maximum-subarray/

Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
 */

#include <stdio.h>
#include <limits.h>

// BRUTE FORCE
#ifdef brute_force
int maxSubArray(int* nums, int numsSize)
{
   int max_sum = 0, sum = 0, i, j;
   max_sum = INT_MIN;
   sum = 0;
   
   for(i = 0; i < numsSize; i++) {
      for(j = i; j < numsSize; j++) {
         sum += nums[j];
         if(sum > max_sum) {
            max_sum = sum;
         }
      }
      sum = 0;
   }
   return max_sum;

}
#else
// KADANE's Algorithm modification
int maxSubArray(int* nums, int numsSize)
{
   int max_sum = 0, sum = 0, i;
   max_sum = nums[0];
   sum = nums[0];
   
   for(i = 1; i < numsSize; i++) {
      if(sum + nums[i] > nums[i]) {
         sum = sum + nums[i];
      }else {
         sum = nums[i];
      }
      if(sum > max_sum) {
         max_sum = sum;
      }
   }
   return max_sum;

}
#endif

int main()
{
   //int arr[] = {-2,1};
   int arr[] = {-2,1,-3,4,-1,2,1,-5,4};
   int sum = 0;

   sum = maxSubArray(arr, sizeof(arr)/sizeof(arr[0]));

   printf("Max sum of subarray is %d\n", sum);

   return 0;
   
}
