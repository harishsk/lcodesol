/*
https://leetcode.com/problems/remove-duplicates-from-sorted-list/
Given a sorted linked list, delete all duplicates such that each element appear only once.

Example 1:

Input: 1->1->2
Output: 1->2
Example 2:

Input: 1->1->2->3->3
Output: 1->2->3
 */

#include <stdio.h>


/* Definition for singly-linked list. */
struct ListNode {
    int val;
    struct ListNode *next;
};

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}



struct ListNode* deleteDuplicates(struct ListNode* head)
{
   struct ListNode *t = head, *n;
   
   if(head == NULL)
      return NULL;

   n = head->next;
   
   if(n == NULL)
      return head;

   while(n != NULL) {
      if(t->val == n->val) {
         t->next = n->next;
         //free(n);
         n = t->next;
      }else {
         t = t->next;
         n = n->next;
      }
   }
   return head;
}

int main()
{
   struct ListNode a, b, c, d;

   struct ListNode* m = NULL;
   
   
   a.val = 1;
   a.next = &b;

   b.val = 1;
   b.next = &c;

   c.val = 2;
   c.next = NULL;

   d.val = 4;
   d.next = NULL;

   printf("List is : \n   ");
   print_list(&a);

   m = deleteDuplicates(&a);

   printf("List after deleting duplicate : \n   ");
   print_list(&a);

   return 0;
   
}
