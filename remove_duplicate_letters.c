#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <utstack.h>

typedef struct intstack_t {
	 char c;
	 struct intstack_t *next;
}intstack_t;

char * removeDuplicateLetters(char * s){
	 int cnt[26] = {0};
	 int idx = 0, nf = 0;
	 bool visited[26] = {0};
	 intstack_t *istack = NULL, *el = NULL;	 
	 
	 while(s[idx] != '\0'){
		  int c = s[idx] - 'a';
		  cnt[c]++;			 
		  idx++;
	 }
	 idx = 0;
	 while(s[idx] != '\0'){
		  int c = s[idx] - 'a';
		  cnt[c]--;
		  if(visited[c]) {
			  idx++;
			  continue;
		  }
		  while(!STACK_EMPTY(istack) && 
				  c < STACK_TOP(istack)->c && 
				  cnt[STACK_TOP(istack)->c]!=0){ 
			  STACK_POP(istack, el);
			  visited[el->c]=false;
			  free(el);
		  }
		  el = malloc(sizeof(intstack_t));
		  el->c = c;
		  STACK_PUSH(istack, el);			
		  visited[c] = true;			
		  idx++;
	 }
	 int count;
	 STACK_COUNT(istack, el, count);
	 s[count--] = '\0';
		  
	 while(!STACK_EMPTY(istack)){
		  STACK_POP(istack, el);
		  s[count--] = el->c + 'a';
		  free(el);
	 }
	 return s;
}
int main(){
	char c[100];
	strcpy(c, "cbacdcbc");

	char * p = removeDuplicateLetters(c);

	printf("%s\n", p);
	return 0;
}
