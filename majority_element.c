#if defined(USING_UTHASH)
struct inthash {
    int num;
    int count;
    UT_hash_handle hh;
};

void hash_add(struct inthash **ihash, int n) {
    struct inthash *el = NULL;
    HASH_FIND_INT(*ihash, &n, el);
    if(el == NULL) {
        el = malloc(sizeof(struct inthash));
        el->num = n;
        el->count = 0;
        HASH_ADD_INT(*ihash, num, el);
    }
    el->count++;
}

void add_back(int **ret, int *rS, int num){
    *ret = realloc(*ret, sizeof(int) * (*rS + 1));
    (*ret)[*rS] = num;
    *rS += 1;
    return;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* majorityElement(int* nums, int numsSize, int* returnSize){
    *returnSize = 0;
    if(nums == NULL || numsSize < 1)
        return NULL;
    
    struct inthash *ihash = NULL;
    for(int i = 0; i < numsSize; i++) {
        hash_add(&ihash, nums[i]);
    }
    
    int *ret = NULL;
    struct inthash *el = NULL, *temp = NULL;
    HASH_ITER(hh, ihash, el, temp){
        if(el->count > numsSize/3) {
            add_back(&ret, returnSize, el->num);
        }
    }
    HASH_ITER(hh, ihash, el, temp){
        HASH_DEL(ihash, el);
        free(el);
    }
    return ret;
}
#else
void add_back(int **ret, int *rS, int num){
    *ret = realloc(*ret, sizeof(int) * (*rS + 1));
    (*ret)[*rS] = num;
    *rS += 1;
    return;
}

int* majorityElement(int* nums, int numsSize, int* returnSize){
    *returnSize = 0;
    if(nums == NULL || numsSize < 1)
        return NULL;
    
   int cand1, cand2, count1, count2;
   cand1 = cand2 = count1 = count2 = 0;
   for (int i = 0; i < numsSize; i++) {
      int num = nums[i];
       if (cand1 == num) {
         count1++;   
       }else if (cand2 == num) {
         count2++;   
       }else if (count1 == 0) {
         cand1 = num;
         count1++;  
       }else if (count2 == 0) {
         cand2 = num;
         count2++;  
       }else {
         count1--;
         count2--;
      }
   }
   
   count1 = count2 = 0;
   for (int i = 0; i < numsSize; i++) {
      int num = nums[i];
      if (cand1 == num)
         count1++;
      else if (cand2 == num)
         count2++;
   }
   int *ret = NULL;

   if(count1 > numsSize/3)
      add_back(&ret, returnSize, cand1);
   
   if(count2 > numsSize/3)
      add_back(&ret, returnSize, cand2); 

   return ret;
}

#endif
