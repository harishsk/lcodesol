/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

/* QUEUE IMPLEMENTATION START */
typedef struct queue_node_tag{
   int val;
   struct queue_node_tag *next;
}queue_node_t;

typedef struct queue_tag {
   int c;
   int max;
   queue_node_t *head;
   queue_node_t *tail;
}queue_t;

void queue_init(queue_t *q, int max) {
   q->c = 0;
   q->max = max;
   q->head = NULL;
   q->tail = NULL;
}
bool queue_full(queue_t *q) {
   if(q->c == q->max) 
      return true;
   else
      return false;
   
}
void queue_add(queue_t *q, int val) {
   if(queue_full(q))
       return;
   queue_node_t *n = malloc(sizeof(queue_t));
   n->val = val;
   n->next = NULL;
   if(q->head == NULL)
      q->head = n;
   if(q->tail == NULL) {
      q->tail = n;
   }else {
      q->tail->next = n;
      q->tail = n;
   }
   q->c++;
   return;
}

void queue_free(queue_t *q) {
    while(q->head != NULL) {
        queue_node_t *n = q->head;
        q->head = q->head->next;
        free(n);
    }
}
/* QUEUE IMPLEMENTATION END */

void kthSmallest_r(struct TreeNode* root, queue_t *q){
   if(root == NULL || queue_full(q))
      return;
   kthSmallest_r(root->left, q);
   queue_add(q, root->val);
   kthSmallest_r(root->right, q);
}

int kthSmallest(struct TreeNode* root, int k){
   if(root == NULL)
      return 0;
   queue_t q;
   queue_init(&q, k);
   kthSmallest_r(root, &q);
    
   int res =  q.tail->val;
   queue_free(&q); 
   return res;
}
