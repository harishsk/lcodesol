/* Link:
https://leetcode.com/problems/fraction-to-recurring-decimal/
*/

/* Problem:
Given two integers representing the numerator and denominator of a fraction, return the fraction in string format.

If the fractional part is repeating, enclose the repeating part in parentheses.

If multiple answers are possible, return any of them.

It is guaranteed that the length of the answer string is less than 104 for all the given inputs.

 

Example 1:

Input: numerator = 1, denominator = 2
Output: "0.5"

Example 2:

Input: numerator = 2, denominator = 1
Output: "2"

Example 3:

Input: numerator = 2, denominator = 3
Output: "0.(6)"

Example 4:

Input: numerator = 4, denominator = 333
Output: "0.(012)"

Example 5:

Input: numerator = 1, denominator = 5
Output: "0.2"

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <uthash.h>

/* Solution: */

#define MAX_STR 10000

struct inthash{
   long long val;
   long long q;   
   int idx;
   UT_hash_handle hh;
};

void hash_add(struct inthash **ihash, int num, int q, int idx){
   struct inthash *el = malloc(sizeof(struct inthash));
   el->val = num;
   el->idx = idx;
   el->q = q;
   HASH_ADD_INT(*ihash, val, el);
}
bool hash_find(struct inthash **ihash, int num, int *rec_idx){
    struct inthash *el = NULL;
    HASH_FIND_INT(*ihash, &num, el);
    if(el != NULL)
       *rec_idx = el->idx;
    return el != NULL;
}
int sort_idx(struct inthash *a, struct inthash *b) {
    return (a->idx - b->idx);
}

#define ABS(num) ((num) < 0 ? (-((long long)num)) : (num))
char * fractionToDecimal(int numerator, int denominator){
   char *res = calloc(sizeof(char), MAX_STR);
   int res_idx = 0;
   
   /* Result sign */
   bool is_neg = (numerator < 0) ^ (denominator < 0);
   if(is_neg)
      res_idx += snprintf(res, MAX_STR, "-");

   /* Remove the sign from numerator and denominator */
   long long nume = ABS(numerator);
   long long deno = ABS(denominator);

   /* resultant full number */
   long long res_num = nume / deno;
   res_idx += snprintf(res + res_idx, MAX_STR, "%lld", res_num);
   
   /* No remainder left, completely divides no decimal point */
   if((nume % deno) == 0) {
      if(res_num == 0) {
         memset(res, 0, MAX_STR);
         res[0] = '0';
      }
      return res;
   }
   /* Find the decimal */
   struct inthash *ihash = NULL;
   long long remainder = nume % deno;
   long long quos = (remainder * 10) / deno;
   bool is_recurring = false;
   int res_fract_idx = 0;
   int rec_idx = 0;
   
   while(remainder != 0 && !is_recurring) {
      if(hash_find(&ihash, remainder, &rec_idx) == false) {
         hash_add(&ihash, remainder, quos, res_fract_idx);
      }else {
         is_recurring = true;
         break;
      }
      remainder = (remainder * 10) % deno;
      quos = (remainder * 10) / deno;
      res_fract_idx++;
   }
   HASH_SORT(ihash, sort_idx);

   char *res_fract = calloc(sizeof(char), MAX_STR);
   char *res_recurring = calloc(sizeof(char), MAX_STR);
   int rf_idx = 0;
   int rr_idx = 0;

   struct inthash *el, *tmp;
   HASH_ITER(hh, ihash, el, tmp) {
      if(is_recurring && el->idx >= rec_idx)
         res_recurring[rr_idx++] = el->q + '0';
      else
         res_fract[rf_idx++] = el->q + '0';

      HASH_DEL(ihash, el);
      free(el);
   }

   res_idx += snprintf(res + res_idx, MAX_STR, ".%s", res_fract);
   
   if(is_recurring == true) 
      res_idx += snprintf(res + res_idx, MAX_STR, "(%s)", res_recurring);
   
   free(res_fract);
   free(res_recurring);
   return res;
}

/* Test Driver: */
int main() {
   char *p = NULL;
   printf("%s\n", p = fractionToDecimal(-2147483648, 1));
   free(p);
   return 0;
}
