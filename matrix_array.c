void sq_rotate(int** matrix, int sq_size, int start) {
	int ri ,ci, rel, cel, pres, prev = 0;
	
	ri = start; ci = start;
	
	cel = start + sq_size -1;
	for(; ci <= cel; ci++) {
		pres = matrix[ri][ci];
		matrix[ri][ci] = prev;
		prev = pres;
	}
    ci--; ri++;
	rel = start + sq_size -1;
	for(; ri <= rel; ri++) {
		pres = matrix[ri][ci];
		matrix[ri][ci] = prev;
		prev = pres;
	}
    ri--; ci--;
	cel = start;
	for(; ci >= cel; ci--) {
		pres = matrix[ri][ci];
		matrix[ri][ci] = prev;
		prev = pres;
	}
    ci++; ri--;
	rel = start;
	for(; ri >= rel; ri--) {
		pres = matrix[ri][ci];
		matrix[ri][ci] = prev;
		prev = pres;
	}
}
void rotate(int** matrix, int matrixSize, int* matrixColSize){
	int sq_bound, sq_num_rot, sq_size;
	/* Trivial case - no rotation */
	if(matrixSize == 0 || matrix == NULL || *matrixColSize == 0 ||
		matrixSize == 1 || *matrixColSize == 1)
		return matrix;

	for(sq_bound = 0; sq_bound < matrixSize/2; sq_bound++) {
		sq_size = matrixSize - sq_bound * 2;
		for(sq_num_rot = 0; sq_num_rot < sq_size - 1; sq_num_rot++) {
			sq_rotate(matrix, sq_size, sq_bound);
		}
	}
    return;
}


#if defined(STUDY_LATER)
void rotate (int **matrix, int n, int *thisvariableisuseless) {
    for (int y = 0; y < n / 2; y++) {
        for (int x = 0; x < (n + 1) / 2; x++) {
            int q = matrix[y][x];
            matrix[y][x] = matrix[n-x-1][y];
            matrix[n-x-1][y] = matrix[n-y-1][n-x-1];
            matrix[n-y-1][n-x-1] = matrix[x][n-y-1];
            matrix[x][n-y-1] = q;
        }
    }
}
// ()[]()()()
// ()()()()[]
// ()()()()()
// []()()()()
// ()()()[]()
#endif
