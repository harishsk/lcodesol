void flatten(struct TreeNode* root){
	if(root == NULL)
		return;
	
	struct TreeNode* temp_right = root->right;
	flatten(root->left);
	flatten(root->right);
	
	root->right = root->left;
	root->left = NULL;
	
	while(root->right) {
		root = root->right;
	}
	root->right = temp_right;
}
