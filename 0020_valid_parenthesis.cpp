#include <iostream>
#include <stack>
using namespace std;

class Solution {
   bool is_open(char c) {
      return c == '[' || c == '(' || c == '{';
   }
   bool is_match(char c1, char c2) {
      return (c1 == ']' && c2 == '[' ||
              c1 == ')' && c2 == '(' ||
              c1 == '}' && c2 == '{' );
   }   
public:
   bool isValid(string s) {
      /* Odd number of chareter cannot have balanced parenthesis */
      if(s.length() % 2 == 1)
         return false;
      stack <char>st;
      for(char c: s) {
         if(is_open(c)) {
            st.push(c);
         }else {
            if(st.empty()) 
               return false;
            if(!is_match(c, st.top()))
               return false;
            st.pop();
         }
      }
      if(!st.empty())
         return false;
      return true;
   }
};

int main()
{
   Solution s;
   string str = "([{}]){)";
   cout << "is parenthesis valid " << s.isValid(str) << endl;
   return 0;
}
