void add_back(int ***ret, int *rS, int **rC, int *com, int clen, int k) {
    *ret = realloc(*ret, sizeof(int*) * (*rS + 1));
    *rC = realloc(*rC, sizeof(int*) * (*rS + 1));
    (*rC)[*rS] = k;
    (*ret)[*rS] = malloc(sizeof(int) * clen);
    memcpy((*ret)[*rS], com, sizeof(int) * clen);
    *rS += 1;
}
void combine_r(int n, int k, int ***ret, int* rS, int** rC, int *com, int clen, int *rem, int rlen) {
    if(clen == k) {
        add_back(ret, rS, rC, com, clen, k);
        return;
    }
    for(int i = 0; i < rlen; i++) {
        com[clen] = rem[i];
        int new_rem[n];
        memcpy(new_rem, rem + i + 1, sizeof(int) * (rlen - i - 1));
        combine_r(n, k, ret, rS, rC, com, clen + 1, new_rem, rlen - i - 1);
    }
    return;
}
int** combine(int n, int k, int* returnSize, int** returnColumnSizes){
    
    int **ret = NULL;
    int com[k], rem[n];
    for(int i = 1; i <= n ; i++)
        rem[i-1] = i;
    *returnSize = 0;
    *returnColumnSizes = NULL;
    combine_r(n, k, &ret, returnSize, returnColumnSizes, com, 0, rem, n);    
    return ret;
}
