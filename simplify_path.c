#include <stdio.h>

#if defined(OLD_SOLUTION)
char simple_path[1000] = {};
int path_index = 0;
int dir[1000];
int dir_index = 0;

void add(char x) {
	simple_path[path_index] = x;
	if(x == '/') 
		dir[dir_index++] = path_index;
	path_index++;
}
void remove_dir() {
	if(dir_index - 1 >= 0) {
		dir_index--;
		path_index = dir[dir_index - 1] + 1;
	}
}
char *getSimplePath() {
	if(path_index == 1) {
	simple_path[path_index] = '\0';
   }else {
      if(simple_path[path_index - 1] = '/') 
         simple_path[path_index - 1] = '\0';
   }
	return simple_path;
}
char * simplifyPath(char * path){
	int i = 0;
	//char prev[4] = "";
   char prev = '\0';

   path_index = 0;
   dir_index = 0;
   
	while(path[i] != '\0') {
		if(path[i] == '/' && prev == '/') {
			//Ignore
		}else if(path[i] == '.' && prev == '.') {
			remove_dir();
		}else if(path[i] == '.' && prev == '/') {
			//Ignore
		}else if(path[i] == '/' && prev == '.') {
			//Ignore
		}else {
			add(path[i]);
		}
		prev = path[i];
		i++;
   }
   return getSimplePath();
}
#else
#include "utstack.h"
#include <stdlib.h>
#include <string.h>

#define BUFLEN 1000

typedef struct el {
    char fname[BUFLEN];
    struct el *next;
} el;

char simple_path[BUFLEN];
int simple_path_idx = 0;

char * simplifyPath(char * path){
   char tsep[BUFLEN], tfname[BUFLEN];   
   int i = 0, tsep_idx = 0, tfname_idx = 0;
   el *st = NULL, *st2 = NULL;

   tfname_idx = 0;
   memset(tfname, '\0', BUFLEN);

   tsep_idx = 0;
   memset(tfname, '\0', BUFLEN);   

   simple_path_idx = 0;
   memset(simple_path, '\0', BUFLEN);
   
   while(path[i] != '\0') {      
      if(path[i] == '/' || path[i] == '.') {
         tsep[tsep_idx++] = path[i];
         tsep[tsep_idx] = '\0';
      }else if(path[i] != '/' && path[i] != '.') {
         if(strlen(tsep) != 0) {
            strcpy(tfname + tfname_idx, tsep);
            tfname_idx += tsep_idx;
            tsep[tsep_idx = 0] = '\0';
         }
         tfname[tfname_idx++] = path[i];
         tfname[tfname_idx] = '\0';
      }
      
      if(strlen(tfname) == 0 &&
         strncmp(tsep, "./", 2) == 0) {
         tsep[tsep_idx = 0] = '\0';
      }      
      if(strlen(tfname) != 0 &&
         strncmp(tsep, "/", 1) == 0) {
         
         el *sel = malloc(sizeof(el));
         strcpy(sel->fname, tfname);
         STACK_PUSH(st, sel);

         tfname[tfname_idx = 0] = '\0';
         tsep[tsep_idx = 0] = '\0';         
      }
      if(strlen(tfname) == 0 &&
         strncmp(tsep, "/", 1) == 0) {
         
         tfname[tfname_idx = 0] = '\0';
         tsep[tsep_idx = 0] = '\0';         
      }      
      if(strlen(tfname) == 0 &&
         strncmp(tsep, "../", 3) == 0) {
         if(!STACK_EMPTY(st)) {
            el *sel = NULL;
            STACK_POP(st, sel);
            free(sel);
         }
         tsep[tsep_idx = 0] = '\0';         
      }
      if(strlen(tfname) == 0 &&
         strstr(tsep, ".../") != NULL) {
         
         el *sel = malloc(sizeof(el));
         strncpy(sel->fname, tsep, tsep_idx - 1);
         sel->fname[tsep_idx -1]='\0';
         STACK_PUSH(st, sel);
 
         tsep[tsep_idx = 0] = '\0';         
      }
      i++;
   }

   if(strlen(tfname) != 0) {
      
      el *sel = malloc(sizeof(el));
      strcpy(sel->fname, tfname);
      STACK_PUSH(st, sel);
      
      tfname[tfname_idx = 0] = '\0';
   }   
   if(strlen(tfname) == 0 &&
      strstr(tsep, "...") != 0) {
      
      el *sel = malloc(sizeof(el));
      strncpy(sel->fname, tsep, tsep_idx);
      sel->fname[tsep_idx]='\0';      
      STACK_PUSH(st, sel);

      tsep[tsep_idx = 0] = '\0';
   }
   
   if(strncmp(tsep, "..", 2) == 0) {
      if(!STACK_EMPTY(st)) {
         el *sel = NULL;
         STACK_POP(st, sel);
         free(sel);
      }
      tsep[tsep_idx = 0] = '\0';
   }

   simple_path[0] = '\0';

   if(STACK_EMPTY(st)) {
      simple_path[simple_path_idx++] = '/';
      simple_path[simple_path_idx++] = '\0';      
   }
   
   while(!STACK_EMPTY(st)){
      el *sel = NULL;
      STACK_POP(st, sel);
      STACK_PUSH(st2, sel);
   }
   while(!STACK_EMPTY(st2)){
      el *sel = NULL;
      STACK_POP(st2, sel);
      simple_path[simple_path_idx++] = '/';
      strcpy(simple_path + simple_path_idx,
             sel->fname);
      simple_path_idx += strlen(sel->fname);
      free(sel);
   }
   simple_path[simple_path_idx++] = '\0';
   return simple_path;

}
#endif

int main() {
   char *p = "/..hidden";

   printf("Simplified path is %s\n", simplifyPath(p));


   return 0;
   
}

