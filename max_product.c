#define BRUTE_FORCE
int maxProduct(int* nums, int numsSize){
	int i, j, cprod = 1, max_prod = INT_MIN;
	
	if(nums == NULL || numsSize == 0)
		return 0;
	
	if(numsSize == 1) 
		return nums[0];

	for(i = 0; i < numsSize; i++) {
		cprod = 1;
		for(j = i; j < numsSize; j++) {
			cprod = cprod * nums[j];
			if(cprod > max_prod) {
				max_prod = cprod;
			}
		}
	}
	return max_prod;
}
#else


#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

int maxProduct(int* nums, int numsSize){
    if (!nums || numsSize == 0) return 0;
    if (numsSize == 1) return nums[0];
    
    int max = nums[0];
    int min = nums[0];
    int res = nums[0];
    
    for (int i = 1; i < numsSize; i++) {
        int cur = nums[i];
        int temp_max = MAX(cur, MAX(max * cur, min * cur));
        min = MIN(cur, MIN(max * cur, min * cur));
        max = temp_max;
        
        res = MAX(res, max);
    }
    
    return res;
}

#endif
