void add_back(char ***ret, int *rS, char *ip, int iplen) {
	*ret = realloc(*ret, sizeof(char*) * (*rS + 1));
	(*ret)[*rS] = malloc(sizeof(char) * (iplen + 1));
	memcpy((*ret)[*rS], ip, iplen);
    (*ret)[*rS][iplen] = '\0';
	*rS += 1;
	return;
}
void restoreIpAddresses_r(char * s, int len, char ***ret, 
			int* rS, int idx, int num_oct, char *ip, int iplen){    
	if(num_oct > 4)
		return; // Go back invalid IP
	if(idx == len && num_oct == 4) {
		add_back(ret, rS, ip, iplen);
		return;  // Go back, done finding
	}
	if(idx >= len)
		return; // Go back No string left 
    bool is_last = (num_oct == 3);
	ip[iplen] = s[idx];
    if(!is_last) {
	    ip[iplen+1] = '.';
	    restoreIpAddresses_r(s, len, ret, rS, idx + 1, 
		    			num_oct + 1, ip, iplen + 2);
    }else {
        restoreIpAddresses_r(s, len, ret, rS, idx + 1, 
		    			num_oct + 1, ip, iplen + 1);
    }
	
	if(s[idx] != '0' && idx + 1 < len) {
		ip[iplen] = s[idx];
		ip[iplen+1] = s[idx+1];
        if(!is_last) {
		    ip[iplen+2] = '.';
		    restoreIpAddresses_r(s, len, ret, rS, idx + 2, 
			    			    num_oct + 1, ip, iplen + 3);
        }else {
            restoreIpAddresses_r(s, len, ret, rS, idx + 2, 
			    			    num_oct + 1, ip, iplen + 2);
        }
	}
	if(idx + 2 < len && s[idx] > '0' && s[idx] <= '2') {
        int ipval = (s[idx] - '0') * 100 + (s[idx + 1] - '0')* 10 + 
                    (s[idx + 2] - '0');
        if(ipval > 255)
            return;
		ip[iplen] = s[idx];
		ip[iplen+1] = s[idx+1];
		ip[iplen+2] = s[idx+2];
        if(!is_last) {
		    ip[iplen+3] = '.';
		    restoreIpAddresses_r(s, len, ret, rS, idx + 3, 
			    			    num_oct + 1, ip, iplen + 4);
        }else {
            restoreIpAddresses_r(s, len, ret, rS, idx + 3, 
			    			    num_oct + 1, ip, iplen + 3);
        }
	}
	return;
}
char ** restoreIpAddresses(char * s, int* returnSize){
	*returnSize = 0;
	if(s == NULL)
		return NULL;
	int len = strlen(s);
	if(len < 4)
		return NULL;
	char **ret = NULL;
	char ip[len+4];
	restoreIpAddresses_r(s, len, &ret, returnSize, 0, 0, ip, 0);
	return ret;
}

