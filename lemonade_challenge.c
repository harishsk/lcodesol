#include <stdio.h>
#include <stdbool.h>

void consume_change(int *deno, int *change, int num) {
   if(num > *change)
      return;
   int deno_n = 0;   
   while(deno_n <= *deno) {
      if(deno_n*num - *change > 0)
         break;
      deno_n++;
   }
   if(deno_n > 0)
      deno_n--;
   *change -= deno_n*num;
   *deno -=deno_n;
   return;
}

typedef enum {FIVE, TEN, TWENTY} deno_t;

bool lemonadeChange(int* bills, int billsSize){
   if(bills == NULL || billsSize < 1)
      return false;
   int deno[3] = {0};
   
   for(int i = 0; i < billsSize; i++) {
      int return_change = bills[i] - 5;

      if(return_change > 0) {
         consume_change(&deno[TWENTY], &return_change, 20);        
         consume_change(&deno[TEN], &return_change, 10);
         consume_change(&deno[FIVE], &return_change, 5);
      }
         
      if(return_change != 0)
           return false;
      
      switch(bills[i]) {
      case 5:
         deno[FIVE]++;
         break;
      case 10:
         deno[TEN]++;
         break;
      case 20:
         deno[TWENTY]++;
         break;                
      }
   }
   return true;
}
int main() {
   //int tras[] = {5,5,5,10,20};
   //int tras[] = {5,5,10};
   //int tras[] = {10,10};
   int tras[] = {5,5,10,10,20};
   printf("Is change possible ? %s \n", lemonadeChange(tras, sizeof(tras)/sizeof(tras[0])) == true ? "true" : "false");
   return 0;
}
