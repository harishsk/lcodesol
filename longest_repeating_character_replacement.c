#include <stdio.h>
#include <limits.h>

#if defined(OLD_NOT_WORKING)
typedef struct char_stat_tag {
	int last_idx;
	int freq;
	int left;
}char_stat_t;

void init_char_stat(char_stat_t (*char_stat)[26], int c, int k) {
	(*char_stat)[c].last_idx = -1;
	(*char_stat)[c].freq = 0;
	(*char_stat)[c].left = k;
}
int characterReplacement(char * s, int k){
	if(s == NULL)
		return 0;
	int idx = 0, max = INT_MIN, char_idx;
	char_stat_t char_stat[26];

	for(idx = 0; idx < 26; idx++)
		init_char_stat(&char_stat, idx, k);
	for(idx = 0; s[idx] != '\0'; idx++) {
		if(char_stat[s[idx]- 'A'].last_idx == -1) {
			char_stat[s[idx]- 'A'].last_idx = idx;
		}
		int idx_diff = idx - char_stat[s[idx]- 'A'].last_idx - 1;
      if(idx_diff <= 0){
         char_stat[s[idx]- 'A'].freq += 1;
			char_stat[s[idx]- 'A'].last_idx = idx;
         if(char_stat[s[idx] - 'A'].freq > max)
            max = char_stat[s[idx]- 'A'].freq;         
      }else if(idx_diff <= char_stat[s[idx]- 'A'].left) {
			char_stat[s[idx]- 'A'].freq += (idx_diff + 1);
			char_stat[s[idx]- 'A'].last_idx = idx;
         char_stat[s[idx]- 'A'].left -= idx_diff;
         if(char_stat[s[idx] - 'A'].freq > max)
            max = char_stat[s[idx]- 'A'].freq;
		}else {
         char_stat[s[idx]- 'A'].freq = 1;
         char_stat[s[idx]- 'A'].last_idx = idx;
         char_stat[s[idx]- 'A'].left = k;
		}
	}
	for(char_idx = 0; char_idx < 26; char_idx++) {
		int idx_diff = idx - char_stat[char_idx].last_idx - 1;
		if(idx_diff >= 1 && idx_diff <= char_stat[char_idx].left) {
			char_stat[char_idx].freq += idx_diff;
			if(char_stat[char_idx].freq > max) {
				max = char_stat[char_idx].freq;
			}
		}
	}
	return max;
}
#else


int characterReplacement(char * s, int k){
   int m[128];
   memset(m, 0 , 128);
   int count = 0, begin = 0, end = 0, d = 0;
   while(s[end] != '\0') {
      m[s[end]]++;
      if (m[s[end]] > count)
         count = m[s[end]];
      end++;
      if (end - begin - count > k) {
         m[s[begin]]--;
         begin++;
      }
   }
   d = end - begin;
   return d;
}
#endif

int main()
{
   char *s = "ABAA";
   int k = 0;

   printf("Longest repeating char %d\n", characterReplacement(s, k));

   return 0;
}

