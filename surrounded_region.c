void check(char** board, int row, int col, int rowMax, int colMax){
   if(row < 0 || row >= rowMax || col < 0 || col >= colMax)
      return;
   if(board[row][col] == 'O'){
      board[row][col] = '1';			
      check(board, row - 1, col , rowMax, colMax);
      check(board, row , col - 1, rowMax, colMax);
      check(board, row + 1, col , rowMax, colMax);
      check(board, row, col + 1 , rowMax, colMax);
   }
}

void solve(char** board, int boardSize, int* boardColSize) {
   if(board == NULL || boardSize < 3 || *boardColSize < 3)
      return;

   int row, col;
   int rowMax = boardSize;		  
   int colMax = *boardColSize;			

   /* Four loops below will traverse the permiter */
   /* They will convert all the 0 and connected 0 to 1 */
   
   /* First row */
   row = 0;
   for(col = 0; col < colMax; col++)
      check(board, row, col, rowMax, colMax);

   /* Last row */
   row = rowMax - 1;
   for(col = 0; col < colMax; col++)
      check(board, row, col, rowMax, colMax);				
   
   /* First column */
   col = 0;
   for(row = 1; row < rowMax - 1; row++)
      check(board, row, col, rowMax, colMax);
   
   /* Last column */
   col = colMax - 1;
   for(row = 1; row < rowMax - 1; row++)
      check(board, row, col, rowMax, colMax);
   
   /* Replace remaining O with X */
   for(row = 0; row < rowMax; row++){
      for(col = 0; col < colMax; col++){
         if(board[row][col]=='O')
            board[row][col]='X';
      }
   }
   /* Replace Edge 1 or connected 1 to O */
   for(row = 0; row < rowMax; row++){
      for(col = 0; col < colMax; col++){
         if(board[row][col]=='1')
            board[row][col]='O';
      }
   }		  
}


