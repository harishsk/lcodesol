struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};
#include <stdio.h>
#include <stdlib.h>

#include "utstack.h"
#include "utarray.h"

typedef struct el_tag {
	struct TreeNode* tnode;
	struct el_tag *next;
}el_t;

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* inorderTraversal(struct TreeNode* root, int* returnSize){
	struct TreeNode *cur = NULL;
	el_t *st = NULL, *sel = NULL;
	UT_array *nums = NULL;
	int *ret = NULL, *p, i;
   
	utarray_new(nums, &ut_int_icd);
   
   cur = root;
   
	while(cur != NULL || !STACK_EMPTY(st)) {
		/* Push all the left node to stack */
		while(cur != NULL) {
			sel = malloc(sizeof(el_t));
			sel->tnode = cur;
			STACK_PUSH(st, sel);
         cur = cur->left;
		}
      STACK_POP(st, sel);
      cur = sel->tnode;
		utarray_push_back(nums, &cur->val);
      cur = cur->right;

      free(sel);
	}
	*returnSize = utarray_len(nums);
	ret = malloc(sizeof(int)*(*returnSize));
	
	i = 0;
	for(p=(int*)utarray_front(nums); p != NULL; p=(int*)utarray_next(nums,p), i++) {
      ret[i] = *p;
	}
	utarray_free(nums);
	return ret;
}

int main()
{
   int *p, retSize;
   struct TreeNode t1, t2, t3, t4, t5, t6, t7;
   t1.val = 1;
   t2.val = 2;
   t3.val = 3;
   /* t4.val = 4; */
   /* t5.val = 5; */
   /* t6.val = 6; */
   /* t7.val = 7; */

   t2.left = &t3;
   t2.right = NULL;

   t3.left = &t1;
   t3.right = NULL;

   t1.left = NULL;
   t1.right = NULL;

   /* t4.left = t4.right = NULL; */
   /* t5.left = t5.right = NULL; */
   /* t6.left = t6.right = NULL; */
   /* t7.left = t7.right = NULL; */

   p = inorderTraversal(&t2, &retSize);

   for(int i = 0; i < retSize; i++) {
      printf("%d ", p[i]);
   }
   printf("\n");
   free(p);
   return 0;
}
