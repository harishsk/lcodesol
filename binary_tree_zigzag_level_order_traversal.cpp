#include <bits/stdc++.h>
using namespace std;

/**
 * Definition for a binary tree node.
 */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
       vector<vector<int>> res;
       /* Double ended queue to store nodes at each level */
       deque<TreeNode*> dq;
       /* To count the level, since root is considered level is 1 */
       int level = 1;
       /* Trivial case if root is nullptr return */
       if (root == nullptr) return res;
       /* push root to queue */
       dq.push_back(root);
       while(!dq.empty()) {
          int size = dq.size();
          vector<int> row;
          /* Traverse node at each level 
             pushing its child nodes back to queue */
          for(int i = 0; i < size; i++) {
             TreeNode* f = dq.front();
             dq.pop_front();
             row.push_back(f->val);
             if (f->left) dq.push_back(f->left);
             if (f->right) dq.push_back(f->right);
          }
          /* Since we require zigzag, reverse in case of odd level */
          if((level + 1) % 2)
             reverse(row.begin(), row.end());
          res.push_back(row);
          level++;
       }
       return res;
    }
};

int main() {
   TreeNode t4(4), t5(5), t6(6), t7(7);
   TreeNode t2(2, &t4, &t5); 
   TreeNode t3(3, &t6, &t7);  
   TreeNode t1(1, &t2, &t3);

   Solution s;

   vector<vector<int>> res;

   res = s.zigzagLevelOrder(&t1);

   for( auto x: res){
      for(auto y: x)
         cout << y << " ";
      cout << endl;
    }
}

