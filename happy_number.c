#include <stdbool.h>
#include <stdio.h>
#include "uthash.h"

typedef struct hnum_tag {
   int n;
   UT_hash_handle hh;
}hnum_t;

bool isHappy(int n)
{
   int sum = n, ns, i = 0;
   hnum_t *list = NULL;

   while(sum != 1) {
      ns = sum;
      sum = 0;
      while(ns) {
         int r;
         r = ns % 10;
         sum = sum + r * r;
         ns = ns / 10;
      }
      hnum_t *node = NULL;

      HASH_FIND_INT(list, &n ,node);
      if(node)
         return false;
      
      node = malloc(sizeof(hnum_t));
      node->n = ns;
      HASH_ADD_INT(list, n, node);

      if(i++ > 1000)
         return false;
   }
   return true;
}
int main()
{
   int n = 2;
   
   printf("Is %d happy ? %d\n", n, isHappy(n));

   return 0;
}
