#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode* buildTree_r(int* postorder, int* inorder, int postStart, int postEnd,
                             int inStart, int inEnd) {
   
	if(postStart > postEnd || inStart > inEnd)
		return NULL;

	struct TreeNode* root = malloc(sizeof(struct TreeNode));
	root->val = postorder[postEnd];

   printf("Root Value is %d\n", root->val);
   
	int in_rootidx;
	for(int i = inStart; i <= inEnd; i++) {
		if(inorder[i] == root->val) {
			in_rootidx = i;
			break;
		}
	}
	
	root->left  = buildTree_r(postorder, inorder, postStart, postEnd - (inEnd - in_rootidx +  1),
                             inStart, in_rootidx - 1);
	root->right = buildTree_r(postorder, inorder, postStart, postEnd - 1,
                             in_rootidx + 1, inEnd);


	return root;
}

struct TreeNode* buildTree(int* inorder, int inorderSize, int* postorder, int postorderSize){
	if(inorder == NULL || postorder == NULL)
		return NULL;

	return buildTree_r(postorder, inorder, 0, postorderSize - 1, 0, inorderSize - 1);
}

int main()
{
   int postorder[] = {9,15,7,20,3};
   int inorder[] = {9,3,15,20,7};

   struct TreeNode* root = buildTree(inorder, sizeof(inorder)/sizeof(inorder[0]),
                                     postorder, sizeof(postorder)/sizeof(postorder[0]));

   return 0;
   
}
