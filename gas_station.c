#if defined(BRUTE_FORCE)
bool isCompleteCircuit(int *gas, int *cost, int start_idx, int num) {
    int n, idx = start_idx, gastank = 0;
    for(n = 0; n < num; n++) {
        gastank += gas[idx];
        gastank -= cost[idx];
        if(gastank < 0) 
            return false;
        idx++;
        idx = idx % num;
    }
    return gastank >= 0;
}

int canCompleteCircuit(int* gas, int gasSize, int* cost, int costSize){
    for(int idx = 0; idx < gasSize; idx++){
        if(gas[idx] >= cost[idx] && isCompleteCircuit(gas, cost, idx, gasSize)) {
            return idx;            
        }
    }
    return -1;
}
#else // O(n)
int canCompleteCircuit(int* gas, int gasSize, int* cost, int costSize){
   int gastank = 0, total_rem = 0, start_idx = 0;
   
   for (int idx = 0; idx < gasSize; idx++) {
      int cur_rem = gas[idx] - cost[idx];
      
      if (gastank + cur_rem < 0){
         gastank = 0;
         start_idx = idx + 1;
      }else{
         gastank += cur_rem;
      }
      total_rem += cur_rem;
   }
   return total_rem >= 0 ? start_idx: -1;
}
#endif
