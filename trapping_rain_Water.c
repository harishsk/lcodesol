#include <stdio.h>
#include <stdbool.h>

#if defined(DOESNOT_WORK)
bool get_water_stored(int *h, int s, int idx, int *ni, int *water) {
   *ni = idx + 1;
   if(idx == 0 || idx == s - 1)
      return false;

   int cur_water = 0;
   bool left_wall = false;
   int left_wall_idx = idx;
   for(int i = idx - 1 ; i >=0; i--) {
      if(h[i] > h[idx] && h[i] >= h[left_wall_idx]) {
         left_wall = true;
         left_wall_idx = i;
      }else if(h[i] < h[left_wall_idx]) {
         break;
      }
   }
   bool right_wall = false;
   int right_wall_idx = idx;
   for(int i = idx + 1 ; i < s; i++) {
      if(h[i] > h[idx] && h[i] >= h[right_wall_idx]) {
         right_wall = true;
         right_wall_idx = i;
      }else if(h[i] < h[right_wall_idx]) {
         break;
      }
   }
   if(left_wall && right_wall) {
      printf("%d %d LEFT\n", idx, left_wall_idx);
      printf("%d %d RIGT\n", idx, right_wall_idx);
      *ni = right_wall_idx + 1;
      int min = h[left_wall_idx] < h[right_wall_idx] ? h[left_wall_idx] : h[right_wall_idx];
      //printf("%d MIN\n", min);
      for(int i = left_wall_idx + 1; i <= right_wall_idx -1; i++) {
         if(min - h[i] > 0) {
            //printf("i %d[%d]\n", i , h[i]);
            cur_water = cur_water + (min - h[i]);
         }
      }
      //printf("CUR_WATER %d\n", cur_water);
      *water = *water + cur_water;
      return true;
   }
   return false;
}

int trap(int* height, int heightSize){
   int idx = 0, local_min = height[0], left_max = height[0];
   int water;
   for(idx = 0; idx < heightSize;) {
      int nidx;
      if(get_water_stored(height, heightSize, idx, &nidx, &water)){
         printf("%d %d YES\n", idx, height[idx]);
      }else {
         printf("%d %d NO\n", idx, height[idx]);
      }
      //get_water_stored(height, heightSize, idx, &nidx, &water);
      idx = nidx;
   }
   return water;
}
#elif RIGHT_LEFT_MAX_ARRAY // RIGHT, LEFT MAX ARRAY
#define MIN(x, y) ((x) < (y) ? (x) : (y))
int trap(int* height, int s){
   if(height == NULL || s < 3)
      return 0;
   int lmax[s];
   
   lmax[0] = height[0];
   for(int i = 1; i < s; i++) {
      if(height[i] > lmax[i-1]) {
         lmax[i] = height[i];
      }else {
         lmax[i] = lmax[i-1];
      }
   }

   int rmax[s];
   rmax[s-1] = height[s-1];
   for(int i = s-2; i >=0 ; i--)  {
      if(height[i] > rmax[i+1]) {
         rmax[i] = height[i];
      }else {
         rmax[i] = rmax[i+1];
      }
   }

   int water = 0;
   for(int i = 1; i < s-1 ; i++) {
      water += (MIN(lmax[i], rmax[i]) - height[i]);
   }
   return water;
}
#else // RIGHT LEFT STACK
#include <utstack.h>
#include <stdlib.h>
struct intstack {
   int num;
   int idx;
   struct intstack *next;
};
void stack_push(struct intstack **istack, int n, int i) {
   struct intstack *el;
   el = malloc(sizeof(struct intstack));
   el->num = n;
   el->idx = i;
   STACK_PUSH(*istack, el);
   return;
}
int stack_pop(struct intstack **istack) {
   int ret;
   struct intstack *el;
   STACK_POP(*istack, el);
   ret = el->num;
   free(el);
   return ret;
}
#define MIN(x, y) ((x) < (y) ? (x) : (y))
int trap(int* height, int s){
   if(height == NULL || s < 3)
      return 0;

   struct intstack *lmax = NULL;
   stack_push(&lmax, height[0], 0);
   for(int i = 1; i < s; i++) {
      if(height[i] > STACK_TOP(lmax)->num) {
         stack_push(&lmax, height[i], i);
      }
   }

   struct intstack *rmax = NULL;
   stack_push(&rmax, height[s-1], s-1);
   for(int i = s-2; i >=0 ; i--)  {
      if(height[i] > STACK_TOP(rmax)->num) {
         stack_push(&rmax, height[i], i);         
      }
   }
   struct intstack *lmax_rev = NULL;
   struct intstack *el = NULL;   
   while(!STACK_EMPTY(lmax)) {
      STACK_POP(lmax, el);
      STACK_PUSH(lmax_rev, el);      
   }
   lmax = lmax_rev;

   int water = 0;
   int rpop_idx = STACK_TOP(rmax)->idx;
   int rval = stack_pop(&rmax);
   int lval = stack_pop(&lmax);
   for(int i = 1; i < s - 1 ; i++) {
      if(!STACK_EMPTY(lmax) && i == STACK_TOP(lmax)->idx){
         lval = stack_pop(&lmax);
      }
      
      if(!STACK_EMPTY(rmax) && i >= rpop_idx) {
         rpop_idx = STACK_TOP(rmax)->idx;
         rval = stack_pop(&rmax);
      }
      int cur_water = MIN(lval, rval) - height[i];
      if(cur_water > 0)
         water += cur_water;
   }
   return water;
}
#endif
int main() {
   //int h[] = {0,1,0,2,1,0,1,3,2,1,2,1};
   //int h[] = {0,1,0,2,1,0,0,1,3,3,3,2,1,2,1};
   //int h[] = {4,2,0,3,2,5};
   int h[] = {3,3,3};
   printf("Trapped water is %d\n", trap(h, sizeof(h)/ sizeof(h[0])));
   return 0;
}
