#if defined(BRUTE_FORCE)
bool containsNearbyDuplicate(int* nums, int numsSize, int k){
   if(nums == NULL || numsSize < 2)
      return false;

   for(int i = 0; i < numsSize - k; i++) {
      for(int j = i + 1; j < i + k; j++) {
         if(nums[j] == nums[j-1]) {
            return true;
         }
      }
   }
   return false;
}
#else
struct inthash {
    int num;
    int idx;
    UT_hash_handle hh;
};

static inline bool hash_add(struct inthash **ihash, int n, int i, int k) {
   struct inthash *el = NULL;
   HASH_FIND_INT(*ihash, &n, el);
   if(el == NULL) {
       el = malloc(sizeof(struct inthash));
       el->num = n;
       el->idx = i;
       HASH_ADD_INT(*ihash, num, el);
       return false;
   }else {
      if(i - el->idx <=k) {
         el->idx = i;
         return true;
      }else {
         el->idx = i;
         return false;         
      }
   }
}
static inline void hash_cleanup(struct inthash **ihash) {
   struct inthash *temp = NULL, *el = NULL;
   HASH_ITER(hh, *ihash, el, temp) {
        HASH_DEL(*ihash, el);
   } 
}
bool containsNearbyDuplicate(int* nums, int numsSize, int k){
   if(nums == NULL || numsSize < 2)
      return false;

   struct inthash *ihash = NULL;
   bool found_duplicate = false; 

   for(int i = 0; i < numsSize; i++) {
      if(hash_add(&ihash, nums[i], i, k)) {
           found_duplicate = true;
           break;
       } 
   }
   hash_cleanup(&ihash); 
   
   return found_duplicate;
}

#endif
