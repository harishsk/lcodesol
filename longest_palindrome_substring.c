/*

Link:
https://leetcode.com/problems/longest-palindromic-substring/

Question:
Given a string s, return the longest palindromic substring in s.

Example 1:

Input: s = "babad"
Output: "bab"
Note: "aba" is also a valid answer.

Example 2:

Input: s = "cbbd"
Output: "bb"

Example 3:

Input: s = "a"
Output: "a"

Example 4:

Input: s = "ac"
Output: "a"
 */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/* Solution: */
//#define BRUTE_FORCE
//#define DYNAMIC_PROGRAMMING
#define EXPAND

#if defined(BRUTE_FORCE)
static inline bool is_palindrome(char *s, int start, int end)
{
   int i, num;
   num = end - start + 1;
   for(i = 0; i <= num/2; i++) {
      if(s[i + start] != s[end - i]) {
         return false;
      }
   }
   return true;
}
   
char r[1001];
char * longestPalindrome(char * s)
{
   int i, j, len = 0, len_i, len_j, max_len = 0, max_i, max_j;
   int len_s = strlen(s);
   
   for(i = 0; i < len_s; i++) {
      for(j = i + max_len; j < len_s; j++) {
         if(is_palindrome(s, i, j)) {
            len = j - i + 1;
            len_i = i;
            len_j = j;
         }
         if(len > max_len) {
            max_len = len;
            max_i = len_i;
            max_j = len_j;
         }
      }
   }

   strncpy(r, s + max_i, max_len);
   r[max_len] ='\0';
   return r;
}
#elif defined(DYNAMIC_PROGRAMMING)


char r[1001];
char * longestPalindrome(char * s)
{
   int i , j, d;
   int  len, len_i, len_j, max_len = 0, max_i, max_j;
   int s_len = strlen(s);
   bool pmat[s_len][s_len];

   for(d = 0; d < s_len; d++) {
      for(i = 0; (i < s_len) && ((i + d) < s_len); i++) {
         j = i + d;
         if(i == j) {
            pmat[i][j] = 1;
         }else if((j - i) == 1) {
            pmat[i][j] = (s[i] == s[j]);
         }else {
            pmat[i][j] = (pmat[i+1][j-1] && (s[i] == s[j]));
         }
         //printf("i %d  j %d p? %d\n", i, j, pmat[i][j]);
         if(pmat[i][j]) {
            len = j - i + 1;
            len_i = i;
            len_j = j;
            if(len > max_len) {
               max_len = len;
               max_i = len_i;
               max_j = len_j;
            }    
         }
      }
   }
   strncpy(r, s + max_i, max_len);
   r[max_len] ='\0';
   return r; 
}
#else // EXPAND

int find_longest_palindrome(char*s, int len, int start, int end)
{
   while(start >=0 && end <len && s[start]==s[end]) {
      start--;
      end++;
   }
   return end-start-1;
}

char r[1001];
char * longestPalindrome(char * s)
{
   int i, s_len = strlen(s);
   int len, max_len = 0, max_idx;

   if(s_len < 2) {
      return s;
   }
   
   for(i = 0; i < s_len - 1; i++) {
      len = find_longest_palindrome(s, s_len, i, i);
      if(len > max_len) {
         max_len = len;
         max_idx = i;
      }
      len = find_longest_palindrome(s, s_len, i, i + 1);
      if(len > max_len) {
         max_len = len;
         max_idx = i + 1;
      }      
   }
   strncpy(r, s + max_idx - max_len/2, max_len);
   r[max_len] ='\0';
   return r;  
}
#endif

/* Driver Code */
int main()
{
   char *s = "babbab";
   char *p;
   printf("Max palindrome is %s\n", p = longestPalindrome(s));
   return 0;
}
