/*
https://leetcode.com/problems/symmetric-tree/

Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3
 

But the following [1,2,2,null,3,null,3] is not:

    1
   / \
  2   2
   \   \
   3    3

Follow up: Solve it both recursively and iteratively.
 
*/

#include <stdio.h>
#include <stdbool.h>

/**
 * Definition for a binary tree node.
 */

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

bool compare_node(struct TreeNode* a,struct TreeNode* b);

bool isSymmetric(struct TreeNode* root)
{
   if(root == NULL) {
      return true;
   }
   if(compare_node(root->left, root->right)) {
      return true;
   }else {
      return false;
   }
}

bool compare_node(struct TreeNode* a,struct TreeNode* b)
{
   if(a == NULL && b == NULL) {
      return true;
   }else if(a == NULL || b == NULL) {
      return false;
   }

   if(a->val != b->val) {
      return false;
   }else {
      return compare_node(a->left, b->right) && compare_node(a->right, b->left);
   }
}

int main()
{
   struct TreeNode r1, a1, b1;
   
   r1.left = &a1;
   r1.right = &b1;
   a1.left = NULL;
   b1.left = NULL;
   a1.right = NULL;
   b1.right = NULL;   

   r1.val = 2;
   a1.val = 1;
   b1.val = 1;   

   if(isSymmetric(&r1)) {
      printf("Tree is symmetrical\n");
   }else {
      printf("Tree is not symmetrical\n");
   }
   return 0;
}
