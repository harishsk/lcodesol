#include <stdio.h>

#if defined(FIRST_SOLUTION)
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

int minCostClimbingStairs(int* cost, int costSize){
   int dp[costSize + 1][2], i;
   dp[0][0] = cost[0];
   dp[0][1] = cost[0];   
   dp[1][0] = cost[1];
   dp[1][1] = cost[1];

   for(i = 2; i < costSize; i++) {
      dp[i][0] = cost[i] + MIN(dp[i-1][0], dp[i-1][1]);
      dp[i][1] = cost[i] + MIN(dp[i-2][0], dp[i-2][1]);      
   }
   dp[i][0] = MIN(dp[i-1][0], dp[i-1][1]);
   dp[i][1] = MIN(dp[i-2][0], dp[i-2][1]);
   
   return MIN(dp[i][0], dp[i][1]);
   
}
#elif OPTMISED_DP
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

int minCostClimbingStairs(int* cost, int costSize){
   int dp[costSize];
   dp[0] = cost[0]; /* Jump's directly to 0 */
   dp[1] = cost[1]; /* Jump's directly to 1 */

   for(int i = 2; i < costSize; i++) {
      dp[i] = cost[i] + MIN(dp[i-1], dp[i-2]);
   }
   return MIN(dp[costSize-1], dp[costSize-2]);
   
}
#else
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

int minCostClimbingStairs(int* cost, int costSize){
   int dp[3];
   dp[0] = cost[0]; /* Jump's directly to 0 */
   dp[1] = cost[1]; /* Jump's directly to 1 */

   for(int i = 2; i < costSize; i++) {
      dp[2] = cost[i] + MIN(dp[1], dp[0]);
      dp[0] = dp[1];
      dp[1] = dp[2];
   }
   return MIN(dp[0], dp[1]);
   
}
#endif
int main() {
   int cost[] = {1, 100, 1, 1, 1, 100, 1, 1, 100, 1, 1};

   printf("minCostClimbingStairs %d\n", minCostClimbingStairs(cost, sizeof(cost)/sizeof(cost[0])));

   return 0;
}
