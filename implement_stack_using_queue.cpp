#include <queue>
#include <iostream>
using namespace std;

class MyStack {
   queue<int> q, hq;
public:
    MyStack() {
        
    }
    
    void push(int x) {
       hq.push(x);
       while(!q.empty()){
          hq.push(q.front());
          q.pop();
       }
       while(!hq.empty()){
          q.push(hq.front());
          hq.pop();
       }
    }
    
    int pop() {
       int n = q.front();
       q.pop();
       return n;
    }
    
    int top() {
       return q.front();
    }
    
    bool empty() {
       return q.empty();
    }
};
int main() {
   MyStack *ms = new MyStack();

   ms->push(10);
   ms->push(20);
   cout << ms->pop() << endl;
   cout << ms->top() << endl;
   cout << ms->pop() << endl;
   cout << ms->empty() <<endl;

   return 0;
}
