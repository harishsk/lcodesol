struct ListNode* removeElements(struct ListNode* head, int val){
   /* Noting to remove in case of empty linked list*/
	if(head == NULL)
		return head;

	struct ListNode dummy, *prev, *cur;
	
	dummy.next = head;
	prev = &dummy;
	cur = head;

	while(cur != NULL) {
		if(cur->val == val)
			prev->next = cur->next;
		else
			prev = cur;
		cur = cur->next;
	}
	return dummy.next;
}
