/*
https://leetcode.com/problems/two-sum/

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef brute_force
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int numsSize, int target, int* returnSize){

    int *p;
    int i, j;
    
    p = (int*)malloc(2*sizeof(int));
    
    if(p == NULL) {
        *returnSize = 0;
        return NULL;
    }
    
    for(i = 0; i < numsSize; i++) {
      for(j = 0; j < numsSize; j++) {
        if(i == j){
          continue;
        }
        if((nums[i] + nums[j]) == target) {
          p[0] = i;
          p[1] = j;
          break;
        }
      }
    }
    *returnSize = 2*sizeof(int);
    return p;
}
#elif ARRAY_HASH
// HASH method

#include <stdio.h>
#include <stdlib.h>

#define HASH_SIZE 5000

static int hash[HASH_SIZE] = {0};

void hash_init()
{
  int i;
  for(i = 0; i < HASH_SIZE; i++)
    hash[i] = 0;
}

int hash_find(int n)
{
   int hash_index = n % HASH_SIZE;
   
   if(hash[hash_index] != 0)
      return hash[hash_index] - 1;
   else
      return -1;
}

int hash_add(int n, int i)
{
   int hash_index = n % HASH_SIZE;

   if(hash[hash_index] == 0){
      hash[hash_index] = i + 1;
      return n;
   }else {
      return -1;
   }
}
/* void print_hash(void) */
/* { */
/*    int i; */
/*    for(i = 0; i < HASH_SIZE; i++) { */
/*       printf("[%d]: %d ", i, hash[i]); */
/*    } */
/*    printf("\n"); */
/* } */


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int numsSize, int target, int* returnSize){

    int *p;
    int i, remain, j;

    *returnSize = 0;
    hash_init();
    
    p = (int*)malloc(2*sizeof(int));
    
    if(p == NULL) {
       *returnSize = 0;
       return NULL;
    }
    
    for(i = 0; i < numsSize; i++) {
       remain = target - nums[i];
       if((remain >= 0) && ((j = hash_find(remain)) != -1)) {
          p[0] = i;
          p[1] = j;
          *returnSize = 2;          
          break;
       }else {
          hash_add(nums[i], i);
       }
       /* print_hash(); */
    }

    return p;
}


#else /* UT hash method*/
#include <uthash.h>

typedef struct {
	int key;
	int index;
	UT_hash_handle hh;
} hint_t;

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
	hint_t *hint = NULL, *elem, *tmp;

	if(nums == NULL || numsSize == 0) {
		*returnSize = 0;
		return NULL;
	}

	int *res = malloc(sizeof(int) * 2);
	memset(res, 0, sizeof(int) * 2);
	*returnSize = 2;

	
	for(int i = 0; i < numsSize; i++) {
		int lookup_key = target - nums[i];
		HASH_FIND_INT(hint, &lookup_key, elem);
		if(elem != NULL) {
			res[0] = elem->index;
			res[1] = i;
			return res;
		}
		elem = malloc(sizeof(hint_t));
		elem->key = nums[i];
		elem->index = i;
		HASH_ADD_INT(hint, key, elem);
	}

	HASH_ITER(hh, hint, elem, tmp) {
		free(elem);
	}
	return res;
}

#endif

int main()
{
   int x[] = {2, 7, 11, 15};
   int retsize;
   int *p;

   p = twoSum(x, 4, 9, &retsize);

   if(p != NULL) {
      printf("%d, %d\n", p[0], p[1]);
      free(p);
   }
   return 0;
}
