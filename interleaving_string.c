#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#if defined(BRUTE_FORCE)
bool isInterleave_r(char * s1, int s1_idx, int s1_len, 
                    char * s2, int s2_idx, int s2_len,
                    char * s3, int s3_idx, int s3_len, bool *done){
    if(*done == true)
        return true;
    if(s1_idx == s1_len && s2_idx == s2_len && s3_idx == s3_len) {        
        *done = true;
        return true;
    }
    bool ret1 = false, ret2 = false;
    if(s1_idx < s1_len && s1[s1_idx] == s3[s3_idx]) {        
        ret1 = isInterleave_r(s1, s1_idx +1, s1_len, 
                  s2, s2_idx, s2_len, s3, s3_idx + 1, s3_len, done);
    }
    if(s2_idx < s2_len && s2[s2_idx] == s3[s3_idx]) {        
        ret1 = isInterleave_r(s1, s1_idx, s1_len, 
                  s2, s2_idx + 1, s2_len, s3, s3_idx + 1, s3_len, done);
    }
    return ret1 || ret2;
}

bool isInterleave(char * s1, char * s2, char * s3){
    int s1_len = strlen(s1), s2_len = strlen(s2), s3_len = strlen(s3);
    
    if(s1_len + s2_len != s3_len)
        return false;
    
    bool done = false;
    return isInterleave_r(s1, 0, s1_len, s2, 0, s2_len, s3, 0, s3_len, &done);

}
#else // 2-D memo
int memo[101][101];
bool isInterleave_r(char * s1, int s1_idx, int s1_len, 
                    char * s2, int s2_idx, int s2_len,
                    char * s3, int s3_idx, int s3_len,
                    bool *done){
    if(*done == true)
        return true;
    if(s1_idx == s1_len && s2_idx == s2_len && s3_idx == s3_len) {        
        *done = true;
        return true;
    }
    if(s1_idx < s1_len && s2_idx < s2_len && memo[s1_idx][s2_idx] != -1) 
       return memo[s1_idx][s2_idx];
       
    bool ret1 = false, ret2 = false;
    if(s1_idx < s1_len && s1[s1_idx] == s3[s3_idx]) {        
        ret1 = isInterleave_r(s1, s1_idx +1, s1_len, 
                              s2, s2_idx, s2_len, s3, s3_idx + 1, s3_len, done);
        memo[s1_idx][s2_idx] = ret1;
    }
    if(s2_idx < s2_len && s2[s2_idx] == s3[s3_idx]) {        
        ret2 = isInterleave_r(s1, s1_idx, s1_len, 
                              s2, s2_idx + 1, s2_len, s3, s3_idx + 1, s3_len, done);
        memo[s1_idx][s2_idx] = ret2;
    }
    return ret1 || ret2;
}


bool isInterleave(char * s1, char * s2, char * s3){
    int s1_len = strlen(s1), s2_len = strlen(s2), s3_len = strlen(s3);
    
    if(s1_len + s2_len != s3_len)
        return false;
    
    for (int i = 0; i < s1_len; i++) {
       for (int j = 0; j < s2_len; j++) {
          memo[i][j] = -1;
       }
    }
    
    bool done = false;
    return isInterleave_r(s1, 0, s1_len, s2, 0, s2_len, s3, 0, s3_len, &done);

}
#endif

int main() {
   char *a = "aabcc";
   char *b = "dbbca";
   char *c = "aadbbcbcac";

   printf("Is intervleave %d\n", isInterleave(a, b, c));

   return 0;
          
}
