bool isMatch(char * s, char * p) {
	int adv = 1; 
	if (p[0] == '\0') 
		return s[0] == '\0';
	if (p[1] != '\0' && p[1] == '*') {
		/* Skip since * may be zero or more occurance */
		if (isMatch(s, p + 2)) 
			return true; 
		adv = 0; 
	}
	if (s[0] != '\0' && p[0] == '.' || s[0] == p[0]) {
		/* Compare s with p/. */
		return isMatch(s + 1, p + adv);
	}
	return false;
}
