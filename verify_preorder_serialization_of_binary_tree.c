#include <utstack.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

typedef struct el_tag {
	int val;
	struct el_tag *next;
}el_t;


bool isValidSerialization(char * preorder){
	int i = 0, num;
	bool is_hash_clean = false;
	el_t *st = NULL, *cel = NULL;
   char temp_str[100] = "";
   int temp_cnt = 0;
    bool done = false;
   
	while(preorder[i] != '\0' && !done) {
		if(preorder[i] == ',') {
         temp_str[temp_cnt] = '\0';
         num = atoi(temp_str);
         temp_cnt = 0;
			cel = malloc(sizeof(el_t));
			cel->val = num;
			STACK_PUSH(st, cel);
		}else if(preorder[i] == '#') {
			while(STACK_TOP(st) != NULL && 
			   STACK_TOP(st)->val == '#') {
				STACK_POP(st, cel);
				free(cel);
                if(STACK_EMPTY(st)) {
                    done = true;
                    break;
                }
				STACK_POP(st, cel);
				free(cel);
			}
			cel = malloc(sizeof(el_t));
			cel->val = '#';
			STACK_PUSH(st, cel);
            i++;
		}else {
         temp_str[temp_cnt++] = preorder[i];
      }
		i++;
	}
	if(!done && !STACK_EMPTY(st) && STACK_TOP(st)->val == '#') {
		STACK_COUNT(st, cel, i);
		return i == 1 ? true: false;
	}else {
		return false;
	}
}

int main()
{
   char *p = "9,3,4,#,#,1,#,#,2,#,6,#,#";

   printf("Serilization is %d\n", isValidSerialization(p));
      
}
