/* 
https://leetcode.com/problems/split-a-string-into-the-max-number-of-unique-substrings/ 
*/

struct sub_string {
    char str[17];
    int id;
    UT_hash_handle hh;
};

int maxUniqueSplit(char * s){
    if(s == NULL)
        return 0;
	int itr = 0;
	char temp[17], temp_idx;
	struct sub_string *shash = NULL, *sel = NULL, *tel = NULL;
	int max_unq_split = 0;

	temp[temp_idx = 0] = '\0';
	while(s[itr] != '\0') {
		temp[temp_idx] = s[itr];
		temp[temp_idx + 1] = '\0';
		HASH_FIND_STR(shash, temp, sel);
		if(sel == NULL) {
			sel = malloc(sizeof(struct sub_string));
			strcpy(sel->str, temp);
			HASH_ADD_STR(shash, str, sel);
			temp[temp_idx = 0] = '\0';
		}else {
			temp_idx++;
		}
		itr++;
	}
    if(temp_idx > 0) {
        temp[temp_idx + 1] = '\0';
        HASH_FIND_STR(shash, temp, sel);
		if(sel == NULL) {
			sel = malloc(sizeof(struct sub_string));
			strcpy(sel->str, temp);
			HASH_ADD_STR(shash, str, sel);
			temp[temp_idx = 0] = '\0';
		}        
    }
    
	max_unq_split = HASH_COUNT(shash);

	/* free the hash table contents */
	HASH_ITER(hh, shash, sel, tel) {
		HASH_DEL(shash, sel);
		free(sel);
	}


	return max_unq_split;
}


