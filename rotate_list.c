#include <stdio.h>
#include <stdlib.h>

struct ListNode {
   int val;
   struct ListNode *next;
};

struct ListNode * create_node(int val)
{
   struct ListNode *p = malloc(sizeof(struct ListNode));
   p->next = NULL;
   p->val = val;
   return p;
}

void append_node(struct ListNode *p, int a)
{
   while(p->next) {
      p = p->next;
   }
   p->next = create_node(a);

}

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}
#if define(FIRST_ATTEMPT)
static inline struct ListNode* moveRight(struct ListNode* head, int k, 
							  struct ListNode** p_kth,
					 		  struct ListNode** last, int *num){
	struct ListNode *cur = head, *kth = NULL, *prev_kth = NULL, *tail = NULL;
	int count = k, num_node = 0;
	
	while(cur != NULL) {
		count--;
		if(count == 0) {
			kth = head;
		}else if (count < 0)  {
			prev_kth = kth;
			kth = kth->next;
		}
		tail = cur;
		cur = cur->next;
		num_node ++;
	}
	*p_kth = prev_kth;
	*num = num_node;
	*last = tail;
	return kth;
}
struct ListNode* rotateRight(struct ListNode* head, int k){
	struct ListNode *cur = head, *kth = NULL, *prev_kth = NULL, *tail = NULL;
	int count, num_node = 0;
	
	if(head == NULL || k == 0)
		return head;
	
	kth = moveRight(head, k, &prev_kth, &tail, &num_node);
	if(kth == NULL) {
		count = k % num_node;
		if(count == 0)
			return head;
		kth = moveRight(head, count, &prev_kth, &tail, &num_node);
	}
	
	if(prev_kth!= NULL) {
        prev_kth->next = NULL;
        tail->next = head;
    }
	return kth;
}
#else
struct ListNode* rotateRight(struct ListNode* head, int k){
   /* Nothing to rotate for empty list */
   if(head == NULL)
      return NULL;

   /* Rotating single node list any number of times
      result in same list */
   if(head->next == NULL) 
      return head;

   struct ListNode *p = head, *tail = NULL;
   int len = 0, count;
   while(p != NULL) {
      len++;
      tail = p;
      p = p->next;
   }
   /* If k is larger than len then calculate remainder of rotation */ 
   count = k % len;
   /* Need to move to len - count nodes */ 
   count = len - count;
   /* No rotation required */ 
   if(count == len) {
      return head;
   } 

   /* Move now node and make pointer manipulation */
   p = head;
   while(count!=1) {
      count--;
      p = p->next;
   }   
   struct ListNode *ret = p->next;
   p->next = NULL;
   tail->next = head;
   return ret;

}

#endif

int main()
{
   struct ListNode *a;
   /* a = create_node(1); */
   /* append_node(a, 2); */
   /* append_node(a, 3); */
   /* append_node(a, 4); */
   /* append_node(a, 5); */
   /* append_node(a, 6); */
   /* append_node(a, 7);  */
   a = create_node(0);
   append_node(a, 1);
   append_node(a, 2);   

   print_list(a);
   
   print_list(rotateRight(a, 2));

   return 0;
}

