#include <string.h>
#include <uthash.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct strhash {
	char str[100];
	UT_hash_handle hh;
};

bool in_dict(char *s, int wlen, struct strhash **shash)
{
	struct strhash *el = NULL;
	char str[wlen + 1];
	
	strncpy(str, s, wlen);
	str[wlen] = '\0';
	
	HASH_FIND_STR(*shash, str, el);
	//printf("str %s %d\n", str, el == NULL);
	if(el == NULL) {
		return false;
	}
	return true;
}

bool word_break(int *dp, char * s, int slen, int i, struct strhash **shash)
{
	int j;
	
	if (i == slen)
		return true;
	
	if (dp[i] != -1)
		return dp[i];
	
	for (j = i; j < slen; j++) {
		if (in_dict(s + i, j - i + 1, shash)) {
			if (word_break(dp, s, slen, j + 1, shash)) {
				dp[i] = true;
				return dp[i];
			}
		}
   }
	
	dp[i] = false;
	return dp[i];
}

bool wordBreak(char * s, char ** wordDict, int wordDictSize)
{
	int slen = strlen(s);
	int *dp = malloc(sizeof(int) * slen);
	int i;
	bool ret;
	struct strhash *shash = NULL, *el, *temp;
	
	for(i = 0; i < wordDictSize; i++) {
		el = malloc(sizeof(struct strhash));
		strcpy(el->str, wordDict[i]);
		HASH_ADD_STR(shash, str, el);
	}
	
   for(i = 0; i < slen; i++)
      dp[i] = -1; // unset
   
   ret = word_break(dp, s, slen, 0, &shash);
	
   HASH_ITER(hh, shash, el, temp) {
      free(el);
   }
   
   return ret; 
}

int main()
{
	char *s = "leetcode";
	char *w[] = {"leet", "code"};
	
	printf("%s\n", wordBreak(s, w, sizeof(w) / sizeof(w[0])) == true ? "true" : "false");
   
	return 0;
}
