#include <iostream>
#include <string>
using namespace std;

#if defined( METHOD1)
class Solution {
private:
   int convLetterToNum(char c) {
      switch(c){
      case 'M':
         return 1000;
      case 'D':
         return 500;
      case 'C':
         return 100;
      case 'L':
         return 50;
      case 'X':
         return 10;
      case 'V':
         return 5;
      case 'I':
         return 1;
      }
      return 0;
   }
public:
   int romanToInt(string s) {
      int fir = 0, sec = 0, num = 0;
      if(s.size() == 0)
         return 0;
      fir = convLetterToNum(s[0]);
      if(s.size() == 1)
         return fir;
      for(int i = 1; i < s.size(); i++) {
         sec = convLetterToNum(s[i]);
         if(fir >= sec){
            num += fir;
         }else {
            num -= fir;
         }
         fir=sec;
      }
      num += fir;
      return num;
   }
};
#elif defined(METHOD2)
class Solution {
private:
   int convLetterToNum(char c) {
      switch(c){
      case 'M':
         return 1000;
      case 'D':
         return 500;
      case 'C':
         return 100;
      case 'L':
         return 50;
      case 'X':
         return 10;
      case 'V':
         return 5;
      case 'I':
         return 1;
      }
      return 0;
   }
public:
   int romanToInt(string s) {
      int prev = 0, cur = 0, num = 0;
      if(s.size() == 0)
         return 0;
      for(int i = 0; i < s.size(); i++) {
         cur = convLetterToNum(s[i]);
         if(prev >= cur){
            num += prev;
         }else {
            num -= prev;
         }
         prev = cur;
      }
      num += cur;
      return num;
   }
};
#elif defined(METHOD3)

class Solution {
public:
   int romanToInt(string s) {
      int prev = 0, cur = 0, num = 0;
      unordered_map<char, int> T = { { 'I' , 1 },
                                     { 'V' , 5 },
                                     { 'X' , 10 },
                                     { 'L' , 50 },
                                     { 'C' , 100 },
                                     { 'D' , 500 },
                                     { 'M' , 1000 } };
      if(s.size() == 0)
         return 0;
      for(int i = 0; i < s.size(); i++) {
         cur = T[s[i]];
         if(prev >= cur){
            num += prev;
         }else {
            num -= prev;
         }
         prev = cur;
      }
      num += cur;
      return num;
   }
};
#else //(METHOD4)
class Solution {
public:
   int romanToInt(string s) {
      int res=0;
      s+=' ';
      for(int i = 0; i < s.size(); ){
         if(s[i]=='I' && s[i+1]=='V')      { res+=4;   i+=2;}
         else if(s[i]=='I' && s[i+1]=='X') { res+=9;   i+=2;}
         else if(s[i]=='I')                { res+=1;   i++;}
         else if(s[i]=='V')                { res+=5;   i++;}
         else if(s[i]=='X' && s[i+1]=='L') { res+=40;  i+=2;}
         else if(s[i]=='X' && s[i+1]=='C') { res+=90;  i+=2;}
         else if(s[i]=='X')                { res+=10;  i++;} 
         else if(s[i]=='L')                { res+=50;  i++;}
         else if(s[i]=='C' && s[i+1]=='M') { res+=900; i+=2;}
         else if(s[i]=='C' && s[i+1]=='D') { res+=400; i+=2;}
         else if(s[i]=='C')                { res+=100; i++;}
         else if(s[i]=='D')                { res+=500; i++;}
         else if(s[i]=='M')                { res+=1000; i++;}
         else if(s[i]==' ')                { break; }
      }
      return res;
   }
};
#endif
int main() {
   Solution s;
   string str("XXIV");
   int i;

   i = s.romanToInt(str);

   cout << "Integer value of Roman is "<< i << endl;

   return 0;
}
