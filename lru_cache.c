/* Each node is part of HASH(key,value) and double-linked list(next,prev) */
typedef struct node {
	int key;
	int val;
	struct node *next;
	struct node *prev;
}node_t;

/* LRUCache is implemented using
	1. Double link list 
		head -> recently used items
		tail -> least used items
	2. hash 
		simple array of pointer to node
*/
typedef struct {
	node_t *head;
	node_t *tail;
	int cap;
	int curr;
	node_t **hash;
	int hash_size;
} LRUCache;

/* Utility function to create a node*/
node_t * node_new(){
	node_t *n = malloc(sizeof(node_t));
	n->next = NULL;
	n->prev = NULL;
	return n;
}

/* Utility function to delete a node*/
void node_del(node_t *n){
	n->prev->next = n->next;
	n->next->prev = n->prev;
}

/* Utility function to delete from lrucache tail*/
void lrucache_del_tail(LRUCache *obj){
	node_t *temp, *tail;
	
	tail = obj->tail;
	temp = tail->prev;

	obj->hash[temp->key] = NULL;
	node_del(temp);
	free(temp);
}

/* Utility function to add to lrucache head*/
void lrucache_add_front(LRUCache *obj, node_t *n){
	node_t *head, *tail;
	
	head = obj->head;
	tail = obj->tail;

	n->next = head->next;
	n->prev = head;
	
	head->next->prev = n;
	head->next = n;
}

LRUCache* lRUCacheCreate(int capacity) {
	LRUCache* cache;
	
	cache = malloc(sizeof(LRUCache));
	cache->cap = capacity;
	cache->curr = 0;

	cache->head = node_new();
	cache->tail = node_new();
	cache->head->next = cache->tail;
	cache->tail->prev = cache->head;

	cache->hash_size = 10000;
	cache->hash = malloc(sizeof(node_t*) * cache->hash_size);

	for(int i = 0; i < cache->hash_size; i++) {
		cache->hash[i] = NULL;
	}
	return cache;
}

int lRUCacheGet(LRUCache* obj, int key) {
	node_t *temp;
	int val;

	if(obj->curr == 0) 
		return -1;

	if(obj->hash[key] == NULL)
		return -1;

	val = obj->hash[key]->val;
	node_del(obj->hash[key]);
	lrucache_add_front(obj, obj->hash[key]);
  	return val;
}

void lRUCachePut(LRUCache* obj, int key, int value) {
	
	if(obj->hash[key] == NULL){
		/* key is not present */
		if(obj->curr == obj->cap){
			/* If the capacity is reached */
			lrucache_del_tail(obj);
			obj->curr--;
		}
		obj->curr++;
		node_t *n = node_new();
		n->key = key;
		n->val = value;
		obj->hash[key] = n;
		lrucache_add_front(obj, n);
	}else {
		/* key is present */
		node_del(obj->hash[key]);
		obj->hash[key]->val = value;
		lrucache_add_front(obj, obj->hash[key]);
	}
  
}

void lRUCacheFree(LRUCache* obj) {
    for(int i = 0; i < obj->hash_size; i++) {
		if(obj->hash[i] != NULL)
			free(obj->hash[i] = NULL);
	}
	free(obj->head);
	free(obj->tail);
	free(obj);
}

/**
 * Your LRUCache struct will be instantiated and called as such:
 * LRUCache* obj = lRUCacheCreate(capacity);
 * int param_1 = lRUCacheGet(obj, key);
 
 * lRUCachePut(obj, key, value);
 
 * lRUCacheFree(obj);
*/

