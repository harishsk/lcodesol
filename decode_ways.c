/*
Link: 
https://leetcode.com/problems/decode-ways/

Question:
A message containing letters from A-Z is being encoded to numbers using the following mapping:

'A' -> 1
'B' -> 2
...
'Z' -> 26

Given a non-empty string containing only digits, determine the total number of ways to decode it.

The answer is guaranteed to fit in a 32-bit integer.

 

Example 1:

Input: s = "12"
Output: 2
Explanation: It could be decoded as "AB" (1 2) or "L" (12).

Example 2:

Input: s = "226"
Output: 3
Explanation: It could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).

Example 3:

Input: s = "0"
Output: 0
Explanation: There is no character that is mapped to a number starting with '0'. We cannot ignore a zero when we face it while decoding. So, each '0' should be part of "10" --> 'J' or "20" --> 'T'.

Example 4:

Input: s = "1"
Output: 1

 

Constraints:

    1 <= s.length <= 100
    s contains only digits and may contain leading zero(s).


 */
#include <stdio.h>
#include <string.h>
/* Solution: */
//#define RECURSION
#define DYNAMIC_PROGRAMMING
//#define DYNAMIC_PROGRAMMING_OPTIMIZE

#if defined(RECURSION)
void numDecodings_r(char * s, int idx, int len, int *num){
    if(idx == len) {
        *num = *num + 1;
        return;
    }
    
    int n = s[idx] - '0';
    if(n >= 1 && n <= 26) {
        //printf("idx %d, n %d\n", idx, n);
        numDecodings_r(s, idx + 1, len , num);
    }
        
    if(idx + 1 >= len || n == 0) 
        return;
    
    n = (s[idx] - '0') * 10 + (s[idx + 1] - '0');
    if(n >= 1 && n <= 26) {
        //printf("idx %d, n %d\n", idx, n);
        numDecodings_r(s, idx + 2, len , num);
    }
    
    return;
}

int numDecodings(char * s){
    int num = 0, len = strlen(s);
    
    numDecodings_r(s, 0, len, &num);
    
    return num;
}
#elif defined(DYNAMIC_PROGRAMMING)
int numDecodings(char * s){
   if(s[0] == '0')
       return 0;

   int dp[100], idx = 1;
   memset(dp, 0, sizeof(int)*100);

   dp[0] = 1;
   
   while(s[idx] != '\0') {      
      int cnt = 0;
      if(s[idx] != '0') 
         dp[idx] = dp[idx -1];
      
      if(s[idx-1]=='1'||(s[idx-1]=='2' && s[idx]<='6')) {
         if(idx-2>=0) 
             dp[idx]+=dp[idx-2];
         else
             dp[idx]+=1;
         
      }      
      idx++;
   }
   return dp[idx-1];
}
#else /* DYNAMIC_PROGRAMMING_OPTIMIZE*/
int numDecodings(char * s){
   if(s[0] == '0')
       return 0;

   int dp[3] = {0, 1, 0}, idx = 1;
   
   while(s[idx] != '\0') {      
      if(s[idx] != '0') 
         dp[2] = dp[1];
      
      if(s[idx-1]=='1'||(s[idx-1]=='2' && s[idx]<='6')) {
         if(idx-2>=0) 
             dp[2] += dp[0];
         else
             dp[2] += 1;
         
      }
      dp[0] = dp[1];
      dp[1] = dp[2];
      dp[2] = 0;
      idx++;
   }
   return dp[1];
}
#endif

/* Driver code: */
int main(){
   char *str = "226";
   printf("Decode ways - %d\n", numDecodings(str));
   return 0;
}
