#include <stdio.h>

int get_version(char *version, int *ver, int idx) {
   int num = 0;
   if(idx == -1) {
      *ver = 0;
      return -1;
   }
   
   while(version[idx] != '.' && version[idx] != '\0')  {
      num = num * 10 + (version[idx] - '0');
      idx++;
   }
   *ver = num;
   if(version[idx] == '\0')
      return -1;
   else 
      return idx+ 1;
}

int compareVersion(char * version1, char * version2){
   int idx1 = 0, idx2 = 0;
   int ver1, ver2;
   
   while(!(idx1 == -1 && idx2 == -1)) {
      idx1 = get_version(version1, &ver1, idx1);
      idx2 = get_version(version2, &ver2, idx2);
      if(ver1 < ver2) 
         return -1;
      else if(ver1 > ver2)
         return 1;
   }
   return 0;
}
int main()
{
   char *v1 = "1.0.1", *v2 = "1";
   int c;

   c = compareVersion(v1, v2);

   printf("compare is %d\n", c);

   return 0;
   
}
