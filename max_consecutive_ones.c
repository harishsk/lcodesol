/* Link:
   https://leetcode.com/problems/max-consecutive-ones/
*/

/* Problem:
   Given a binary array, find the maximum number of consecutive 1s in this array.

Example 1:

Input: [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s.
    The maximum number of consecutive 1s is 3.

Note:

    The input array will only contain 0 and 1.
    The length of input array is a positive integer and will not exceed 10,000

*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
#define MAX(x, y) ((x) > (y) ? (x) : (y))
int findMaxConsecutiveOnes(int* nums, int numsSize){
   int max = 0, cnt = 0;
   for(int idx = 0; idx < numsSize; idx++) {
      if(nums[idx] == 1){
         cnt++;
      }else {
         max = MAX(max, cnt);
         cnt = 0;
      }
   }
   max = MAX(max, cnt);
   return max;
}
/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = { 1,1,0,1,1,1 };
   printf("Max number of ones %d\n", findMaxConsecutiveOnes(a, sizeof(a) / sizeof(a[0])));
   
   return 0;
}
#endif

