/*
https://leetcode.com/problems/length-of-last-word/
Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word (last word means the last appearing word if we loop from left to right) in the string.

If the last word does not exist, return 0.

Note: A word is defined as a maximal substring consisting of non-space characters only.

Example:

Input: "Hello World"
Output: 5
 */

#include <stdio.h>

int lengthOfLastWord(char * s)
{
   int len = 0, i, prev_len = 0;
   if(s[0] == 0)
      return 0;

   for(i = 0; s[i] != 0; i++) {
      if(s[i] == ' ') {
         prev_len = (len != 0 ? len : prev_len);
         len = 0;
      }else {
         prev_len = 0;
         len++;
      }
   }
   return prev_len > len ? prev_len : len;
}

int main()
{
   char *p = "Hello World       a ";
   int wl = 0;

   wl = lengthOfLastWord(p);

   printf("Length of last word in sentence \"%s\" is %d\n", p , wl);
}
