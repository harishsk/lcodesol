/*
https://leetcode.com/problems/reverse-integer/
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h> 

void strrev(char *s)
{
  int i = 0, n = strlen(s);
  char t;
  for(i = 0; i < n/2; i++) {
    t = s[i];
    s[i] = s[n-i-1];
    s[n-i-1] = t;
  }
}

int reverse(int x){
  char str_x[128] = {0};
  long r = 0;
  
  snprintf(str_x, 128, "%d", x);
  
  if(str_x[0] == '-') {
    strrev(&str_x[1]);
  }else {
    strrev(str_x);
  }
  r = atol(str_x);
  
  if(r > INT_MAX || r < INT_MIN) {
    return 0;
  }else{
    return (int)r;
  }
}

int main()
{
   int a = 1234;
   printf("reverse of %d is %d\n", a, reverse(a));
   return 0;
}
