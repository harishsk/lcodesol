/* Link:
https://leetcode.com/problems/replace-elements-with-greatest-element-on-right-side/   
*/

/* Problem:
Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.

After doing so, return the array.

 

Example 1:

Input: arr = [17,18,5,4,6,1]
Output: [18,6,6,6,1,-1]
Explanation: 
- index 0 --> the greatest element to the right of index 0 is index 1 (18).
- index 1 --> the greatest element to the right of index 1 is index 4 (6).
- index 2 --> the greatest element to the right of index 2 is index 4 (6).
- index 3 --> the greatest element to the right of index 3 is index 4 (6).
- index 4 --> the greatest element to the right of index 4 is index 5 (1).
- index 5 --> there are no elements to the right of index 5, so we put -1.
Example 2:

Input: arr = [400]
Output: [-1]
Explanation: There are no elements to the right of index 0.
  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int find_max(int *arr, int len){
   int max = -1;
   for(int i = 0; i < len; i++) {
      if(arr[i] > max)
         max = arr[i];
   }
   return max;
}
int* replaceElements(int* arr, int arrSize, int* returnSize){
   if(arrSize < 1){
      *returnSize = 0;
      return NULL;
   }
   int *rarr = malloc(sizeof(int) * arrSize);
   memcpy(rarr, arr, sizeof(int) * arrSize);
   *returnSize = arrSize;
   
   int max = -1;
   rarr[0] = max;
   
   for(int i = 0; i < arrSize; i++) {
      if(rarr[i] == max) {
         max = find_max(arr + i + 1, arrSize - 1 - i);
         rarr[i] = max;
      }else {
         rarr[i] = max;         
      }
   }
   return rarr;
}

/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = {400};
   int *r = NULL, num;
   
   r = replaceElements(a, sizeof(a)/sizeof(a[0]), &num);
   for(int i = 0; i < num; i++) {
      printf("%d ", r[i]);
   }
   printf("\n");
   free(r);
   return 0;
}
#endif
