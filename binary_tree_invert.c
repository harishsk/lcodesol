struct TreeNode* invertTree(struct TreeNode* root){
	if(root == NULL) 
		return root;

	struct TreeNode* leftTemp = root->left;
	root->left = root->right;
	root->right = leftTemp;
	
	invertTree(root->left);
	invertTree(root->right);
   
   return root;
}
