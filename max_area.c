#define MIN(a, b) ((a) < (b) ? (a): (b))
int maxArea(int* height, int heightSize){
	int low_idx, high_idx, maxArea = 0;
	
	if(heightSize == 0 || heightSize == 1) {
		return 0;
	}else if(heightSize == 2) {
		return MIN(height[0], height[1]);
	}

	low_idx = 0;
	high_idx = heightSize - 1;
   
	while(low_idx < high_idx) {
		int area = MIN(height[low_idx], height[high_idx]) *
         (high_idx - low_idx + 1);
		if(area > maxArea)
			maxArea = area;
      
		if(height[low_idx] < height[high_idx]) 
			low_idx++;
		else if(height[low_idx] > height[high_idx])
			high_idx--;
      else
         low_idx++;		
   }
   return maxArea;
}
