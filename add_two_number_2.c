#if defined(LIMITED)
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* createNode(int val){
    struct ListNode* node = malloc(sizeof(struct ListNode));
    node->val = val;
    node->next = NULL;
    return node;
}
uint64_t getNum(struct ListNode* l) {
    struct ListNode* t = l;
    uint64_t num = 0;
    
    while(t != NULL) {
        num = num * 10;
        num = num + t->val;
        t = t->next;
    }
    return num;
}
struct ListNode* createLinkList(uint64_t n){
    if(n == 0) {
        struct ListNode*node = createNode(0);
        return node;
    }
    struct ListNode *head = NULL;
    while(n) {
        uint64_t v = n %10;
        struct ListNode*node = createNode(v);
        if(head == NULL){
            head = node;
        }else {
            node->next = head;
            head = node;
        }
        n = n/10;
    }
    return head;
}
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2){
    if(l1 == NULL && l2 != NULL) {
        return l2;
    }else if(l1 != NULL && l2 == NULL) {
        return l1;
    }else if(l1 == NULL && l2 == NULL) {
        return NULL;
    }
    uint64_t num1 = getNum(l1);
    uint64_t num2 = getNum(l2);    
    
    uint64_t sum = num1 + num2;
    
    struct ListNode* ans_head = createLinkList(sum);

    return ans_head;
}
#else
struct ListNode* reverse(ListNode* head){
   struct ListNode* prev = NULL, *curr = head,* nxt;
   while(curr){
      nxt = curr->next;
      curr->next = prev;
      prev = curr;
      curr = nxt;
   }
   return prev;
}
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
   int rem = 0, temp;
   l1 = reverse(l1);
   l2 = reverse(l2);
   struct ListNode* ans = l1;
   while(l1 && l2){
      temp = l1->val + l2->val + rem;
      if(temp > 9)
         rem = 1;
      else
         rem = 0;
      l1->val = temp%10;
      
      l1 = l1->next;
      l2 = l2->next;
   }
   while(l1){
      temp = l1->val + rem;
      if(temp > 9) rem = 1;
      else rem = 0;
      l1->val = temp%10;
      l1 = l1->next;
   }
   struct ListNode* last = l2;
   while(last){
      temp = last->val + rem;
      if(temp > 9) rem = 1;
      else rem = 0;
      last->val = temp%10;
      last = last->next;
   }
   ListNode* last2 = ans;
   while(last2->next){
      last2 = last2->next;
   }
   last2->next = l2;
   ans =  reverse(ans);
   if(rem){
      ListNode* t = new ListNode(1);
      t->next = ans;
      ans = t;
   }
   return ans;
}
#endif
