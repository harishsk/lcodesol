struct TreeNode* createNode(int num) {
	struct TreeNode* node = NULL;
	node = malloc(sizeof(struct TreeNode));
	node->right = NULL;
	node->left = NULL;
	node->val = num;
	return node; 
}
struct TreeNode* insertBST(struct TreeNode *root, int num) {
	if(root == NULL)
		return createNode(num);

	if(num < root->val ){
		if(root->left == NULL) 
			root->left = createNode(num);
		else
			insertBST(root->left, num);
	}else {
		if(root->right == NULL) 
			root->right = createNode(num);
		else
			insertBST(root->right, num);
	}
	return root;
}

struct TreeNode*insertmidToBST(struct TreeNode* root, int *nums, int low, int high) {
	int mid = 0;

	if(low <=high) {
		mid = low + (high - low) /2;
		root = insertBST(root, nums[mid]);
		insertmidToBST(root, nums, low, mid -1);
		insertmidToBST(root, nums, mid + 1, high);
	}
	return root;
}

struct TreeNode* sortedArrayToBST(int* nums, int numsSize){
	struct TreeNode *root = NULL;
	
	root = insertmidToBST(root, nums, 0, numsSize - 1);
	return root;
}

