void reverse(char *s, int start, int end) {
	while(start < end) {
		char temp;
		temp = s[start];
		s[start] = s[end];
		s[end] = temp;
		start++;
		end--;
	}
}
void remove_white_space(char *s) {
   int idx = 0, k = 0;
   char prev = 'a';
   
   /* Remove Leading space */
   while(s[idx] == ' ' && s[idx] != '\0') 
      idx++;
   
   if(s[idx] == '\0') {
      s[0] = '\0';
      return;
   }
   while(s[idx] != '\0') {
      if(!(prev == ' ' && s[idx] == ' '))
         s[k++] = s[idx];
      prev = s[idx];
      idx++;
   }
   if(s[k -1] == ' ') {
      s[k -1] = '\0'; 
   }else {
      s[k] = '\0';
   }
}
char * reverseWords(char * s){
	int start = 0, idx = 0;
	if(s == NULL) 
		return s;
   
	while(s[idx] != '\0') {
		if(s[idx] == ' ' && start == idx){
			start++;
		}else if(s[idx] == ' ') {
			reverse(s, start, idx - 1);
			start = idx + 1;
		}
		idx++;
	}
   if(start != idx)
      reverse(s, start, idx - 1);
   
	reverse(s, 0, idx - 1);
   remove_white_space(s);
	return s;
}
