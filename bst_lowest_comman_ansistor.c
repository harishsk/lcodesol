
struct TreeNode* lowestCommonAncestor(struct TreeNode* root, struct TreeNode* p, struct TreeNode* q) {
    if(root == NULL)
        return root;
    
    if(root->val > p->val && root->val > q->val) {
        // Traverse left of tree
        return lowestCommonAncestor(root->left, p, q);
    }else if(root->val < p->val && root->val < q->val) {
        // Traverse right of tree
        return lowestCommonAncestor(root->right, p, q);
    }else {
        return root;
    }
    
}
