struct TreeNode* createTreeNode(int val) {
	struct TreeNode* t = malloc(sizeof(struct TreeNode));
	t->val = val;
	t->left = NULL;
	t->right = NULL;
	return t;
}
struct TreeNode* sortedListToBST(struct ListNode* head){
	if(head == NULL) {
		return NULL;
	}else if(head->next == NULL) {
		return createTreeNode(head->val);
	}else if(head->next->next == NULL) {
		return createTreeNode(head->next->val)->left = createTreeNode(head->val);
	}else if(head->next->next->next == NULL) {
		struct TreeNode* t = createTreeNode(head->next->val)
		t->left = createTreeNode(head->val);
		t->left = createTreeNode(head->next->nex->val);
		return t;
	}
	struct ListNode*slow_prev = NULL;
	struct ListNode*slow = head;
	struct ListNode*fast = head;
	
	while(fast->next != NULL && fast->next->next != NULL) {
		slow_prev= slow;
		slow = slow->next;
		fast = fast->next->next;
	}
	slow_prev->next = NULL;
	struct TreeNode* mid = createTreeNode(slow>val);
	mid->left = sortedListToBST(head);
	mid->right = sortedListToBST(slow->next);
	return mid;
}


