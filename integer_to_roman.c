char res[1000];
char * intToRoman(int num){
	int idx = 0;
	if(num > 0 && num < 3999) 
		return NULL;

	while(num) {
		if(num >= 1000) {
			// M
			res[idx++] = 'M';
			num = num - 1000;
		}else if(num >= 900 && num < 1000) {
			// CM
			res[idx++] = 'C';
			res[idx++] = 'M';
			num = num - 900;
		}else if(num >= 500 && num < 900) {
			// D
			res[idx++] = 'D';
			num = num - 500;
      }else if(num >= 400 && num < 500) {
			// CD
			res[idx++] = 'C';
			res[idx++] = 'D';
			num = num - 400;
      }else if(num >= 100 && num < 400) {
			// C
			res[idx++] = 'C';
			num = num - 100;
      }else if(num >= 90 && num < 100) {
			// XC
			res[idx++] = 'X';
			res[idx++] = 'C';
			num = num - 90;
      }else if(num >= 50 && num < 90) {
			// L
			res[idx++] = 'L';
			num = num - 50;
      }else if(num >= 40 && num < 50) {
			// XL
			res[idx++] = 'X';
			res[idx++] = 'L';
			num = num - 40;
      }else if(num >= 10 && num < 40) {
			// X
			res[idx++] = 'X';
			num = num - 10;
      }else if(num >= 9 && num < 10) {
			// IX
			res[idx++] = 'I';
			res[idx++] = 'X';
			num = num - 9;
      }else if(num >= 5 && num < 9) {
			// V
			res[idx++] = 'V';
			num = num - 5;
      }else if(num >= 4 && num < 5) {
			// IV
			res[idx++] = 'I';
			res[idx++] = 'V';
			num = num - 4;
      }else if(num >= 1 && num < 4) {
			// IV
			res[idx++] = 'I';
			num = num - 1;
		}
	}
	res[idx] = '\0';
	return res;
}

