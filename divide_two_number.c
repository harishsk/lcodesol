#include <stdio.h>

#if defined(SLOW_AND_EDGE_CASE_FAILURE)
#define MOD(x) ((x) < 0 ? -(x) : (x))

int divide(int dividend, int divisor){
	int quotient = 0, rem = MOD(dividend), div = MOD(divisor);
   int sign = dividend ^ divisor;
   
	while(rem > div) {
		rem = rem - div;
		quotient++;
	}
	if(sign < 0)
		return -quotient;
	else
		return quotient;
}
#else
#define ABS(x) ((x) >= 0 ? (x) : -(x))
int divide(int dividend, int divisor){

   if(divident == INT_MAX && divisor == -1) {
      return INT_MIN;
   }else if(divident == INT_MIN && divisor == 1) {
      return INT_MAX;
   }
   
	int sign = dividend ^ divisor;
	int udividend = ABS(dividend);
	int udivisor = ABS(divisor);
   
	int quotient = 0;
	for(int i = 31; i >= 0; i--) {
		if(udivisor & (1 << i)) {
			udividend = udividend - (1 << i);
			quotient = quotient + (1 << i); 
		}
	}
   if(sign >= 0)  {
      return quotient;
   }else {
      return -quotient;
   }
}
#endif

int main()
{
   int dividend = -10;
   int divisor = -3;
   
   printf("Quotient is %d\n", divide(dividend, divisor));
   
   return 0;
}
