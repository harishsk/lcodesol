/*
Link: https://leetcode.com/problems/minimum-path-sum/

Question: 
Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right which minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.

Example:

Input:
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
Output: 7
Explanation: Because the path 1->3->1->1->1 minimizes the sum.
 */

#include <stdio.h>
#include <limits.h>

/* Solution: */
//#define RECUSIVE_SOLUTION
#define DYNAMIC_PROGRAMMING

#if defined(RECUSIVE_SOLUTION)
void minPathSum_r(int **grid, int colMax, int rowMax,
                  int col, int row, int csum, int *min_path_sum) {
   
   csum = csum + grid[col][row];
   if(col == colMax - 1 && row == rowMax - 1) {
      if(csum < *min_path_sum) {
         *min_path_sum = csum;
      }
   }
   if(col + 1 < colMax) {
      minPathSum_r(grid, colMax, rowMax, col + 1, row,
                   csum, min_path_sum);
   }
   if(row + 1 < rowMax) {
      minPathSum_r(grid, colMax, rowMax, col, row + 1,
                   csum, min_path_sum);
   }
      
}
int minPathSum(int** grid, int gridSize, int* gridColSize){
   int min_path_sum = INT_MAX;
   minPathSum_r(grid, gridSize, *gridColSize, 0, 0, 0, &min_path_sum);
   return min_path_sum;
}
#else
#define MIN(x, y) ((x) < (y) ? (x) : (y))
int minPathSum(int** grid, int gridSize, int* gridColSize){
   int rowMax = *gridColSize;
   int colMax = gridSize;

   if(rowMax < 1 || colMax < 1)
      return 0;

   int min_path_sum[colMax][rowMax];
   
   min_path_sum[0][0] = grid[0][0];
   
   for(int col = 1; col < colMax; col++) {
      min_path_sum[col][0] = grid[col][0] + min_path_sum[col - 1][0];
   }

   for(int row = 1; row < rowMax; row++) {
      min_path_sum[0][row] = grid[0][row] + min_path_sum[0][row-1];
   }

   for(int row = 1; row < rowMax; row++) {
      for(int col = 1; col < colMax; col++) {
         min_path_sum[col][row] = grid[col][row] +
            MIN(min_path_sum[col-1][row], min_path_sum[col][row-1]);
      }
   }
   return min_path_sum[colMax -1][rowMax -1];   
}
#endif

/* Driver Code: */
int main() {
   int path0[] = {1,3,1};
   int path1[] = {1,5,1};
   int path2[] = {4,2,1};

   int *path[] = {path0, path1, path2};
   int colS = sizeof(path0)/ sizeof(path0[0]);

   printf("Minimum path sum is %d\n", minPathSum(path, sizeof(path)/sizeof(int*), &colS));

   return 0;
}
