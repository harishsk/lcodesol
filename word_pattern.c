bool wordPattern(char* pattern, char* str) {
    int sc = 0;
    int patternSize = strlen(pattern);
    char *map[26];
    char *token;
    
    for(int i = 0; i < 26; i++){
        map[i] = NULL;
    }    
    
    token = strtok(str, " ");    
    while( token != NULL && sc < patternSize)  {
        if(map[pattern[sc] - 'a'] == NULL){
            map[pattern[sc] - 'a'] = token;            
        }
        else{
            if(strcmp(map[pattern[sc] - 'a'], token) != 0)
                return false;            
        }
        token = strtok(NULL, " ");
        sc++;
    }
    
    
    if(sc < patternSize || token != NULL) 
        return false;
    
    for(int i = 0; i < 25 && (map[i] != NULL); i++) {
        for(int j = i + 1; j < 26 && (map[j] != NULL); j++) {            
            if(strcmp(map[i], map[j]) == 0)
                return false;                
        }        
    }
  
    return true;
}
