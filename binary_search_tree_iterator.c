#include "utstack.h"
#include <stdlimit.h>
#include <stdbool.h>

typedef struct el_tag {
	struct TreeNode* tnode;
	struct el_tag *next;
}el_t;

typedef struct {
	el_t *st;
} BSTIterator;

BSTIterator* bSTIteratorCreate(struct TreeNode* root) {
	struct TreeNode *cur = NULL;
	el_t *st = NULL, *sel = NULL;
	BSTIterator* bst_itr = malloc(sizeof(BSTIterator));
	bst_itr->st = NULL;

	// Do in-order traversal   
   	cur = root;   
	while(cur != NULL || !STACK_EMPTY(st)) {
		/* Push all the left node to stack */
		while(cur != NULL) {
			sel = malloc(sizeof(el_t));
			sel->tnode = cur;
			STACK_PUSH(st, sel);
			cur = cur->right;
		}
		STACK_POP(st, sel);
		cur = sel->tnode;
		STACK_PUSH(bst_itr->st, sel);
		cur = cur->left;
	}
	return bst_itr;
}

/** @return the next smallest number */
int bSTIteratorNext(BSTIterator* obj) {
	el_t *sel = NULL;
	int ret = -1;
	if(obj != NULL && !STACK_EMPTY(obj->st)) {
		STACK_POP(obj->st, sel);
		ret = sel->val;
		free(sel);
	}
	return ret;
}

/** @return whether we have a next smallest number */
bool bSTIteratorHasNext(BSTIterator* obj) {
	int ret = false;
	if(obj != NULL && !STACK_EMPTY(obj->st)) {
		ret = true;
	}
	return ret;
  
}

void bSTIteratorFree(BSTIterator* obj) {
    obj->st = NULL;
	free(obj);
}

/**
 * Your BSTIterator struct will be instantiated and called as such:
 * BSTIterator* obj = bSTIteratorCreate(root);
 * int param_1 = bSTIteratorNext(obj);
 
 * bool param_2 = bSTIteratorHasNext(obj);
 
 * bSTIteratorFree(obj);
*/

