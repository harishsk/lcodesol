#if defined(USING_HEAP)
void swap(int *x, int *y) {
   int temp = *x;
   *x = *y;
   *y = temp;
}
 
void heapify(int *arr, int n, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;
  
    if (left < n && arr[left] > arr[largest])
      largest = left;
  
    if (right < n && arr[right] > arr[largest])
      largest = right;
  
    if (largest != i) {
      swap(&arr[i], &arr[largest]);
      heapify(arr, n, largest);
    }
}
  
  
void heapSort(int *arr, int n) {

   for (int i = n / 2 - 1; i >= 0; i--)
      heapify(arr, n, i);
   
   for (int i = n - 1; i >= 0; i--) {
      swap(&arr[0], &arr[i]);
      
      heapify(arr, i, 0);
   }
}

bool containsDuplicate(int* nums, int numsSize){
   if(nums == NULL || numsSize < 2)
      return false;

   heapSort(nums, numsSize);

   for(int i = 1; i < numsSize; i++)
      if(nums[i] == nums[i-1])
         return true;

   return false;
}
#else
struct inthash {
    int num;
    UT_hash_handle hh;
};

static inline bool hash_add(struct inthash **ihash, int n) {
   struct inthash *el = NULL;
   HASH_FIND_INT(*ihash, &n, el);
   if(el == NULL) {
       el = malloc(sizeof(struct inthash));
       el->num = n;
       HASH_ADD_INT(*ihash, num, el);
       return false;
   }else {
       return true;
   }
}
static inline void hash_cleanup(struct inthash **ihash) {
   struct inthash *temp = NULL, *el = NULL;
   HASH_ITER(hh, *ihash, el, temp) {
        HASH_DEL(*ihash, el);
   } 
}
bool containsDuplicate(int* nums, int numsSize){
   if(nums == NULL || numsSize < 2)
      return false;

   struct inthash *ihash = NULL;
   bool found_duplicate = false; 

   for(int i = 0; i < numsSize; i++) {
       if(hash_add(&ihash, nums[i])) {
           found_duplicate = true;
           break;
       } 
   }
   hash_cleanup(&ihash); 
   
   return found_duplicate;
}

#endif
