void setZeroes(int** matrix, int matrixSize, int* matrixColSize){
   int rowMax = *matrixColSize;
   int colMax = matrixSize;

   if(rowMax == 0 || colMax == 0)
      return;

   int row_zero[rowMax];
   int col_zero[colMax];

   memset(row_zero, 0, sizeof(row_zero));
   memset(col_zero, 0, sizeof(col_zero));
   
   for(int row = 0; row < rowMax; row++) {
      for (int col = 0; col < colMax; col ++) {
         if(matrix[col][row] == 0) {
            row_zero[row] = 1;
            col_zero[col] = 1;
         }
      }
   }

   for(int row = 0; row < rowMax; row++) {
      if(row_zero[row] == 1)
         matrix[col][row] = 0;
   }
   for (int col = 0; col < colMax; col ++) {
      if(col_zero[col] == 1)
            matrix[col][row] = 0;
   }
   return;
}
