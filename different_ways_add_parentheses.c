#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void add_back(int val, int **ret, int *rS) {
    *ret = realloc(*ret, sizeof(int)* (*rS + 1));
    (*ret)[*rS] = val;
    *rS += 1;
    return;
}
int eval(int a, int b, int operator) {
    if(operator == '+') {
       return a + b;
    }else if(operator == '-') {
       return a - b; 
    }else if(operator == '*') {
       return a * b; 
    }else {
        return 0;
    }
}
void diffWaysToCompute_r(char * input, int **ret, int* rS, int start, int end, int sum){
    if(start >= end) {
        add_back(sum, ret, rS);
        return;
    }
    
    int fwd1 = 0;
    
    if(start == 0) {
        fwd1 = sscanf(input + start, "%d", &sum);   
    }
    char operator;
    int operand;
    
    int fwd2 = sscanf(input + start + fwd1, "%c", &operator);
    int fwd3 = sscanf(input + start + fwd1 +fwd2, "%d", &operand);

    sum = eval(sum, operand, operator);
    diffWaysToCompute_r(input, ret, rS, start + fwd1 + fwd2 + fwd3, end, sum);
    //diffWaysToCompute_r(input, ret, rS, start + fwd1, end, sum);
    return;
    //diffWaysToCompute_r(input, ret, rs, start + fwd1 + fwd2 + fwd3, end, eval(sum, operand, operator));
    
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* diffWaysToCompute(char * input, int* returnSize){
    *returnSize = 0;
    
    if(input == NULL)
        return NULL;
    
    int *ret = NULL;
    
    diffWaysToCompute_r(input, &ret, returnSize, 0, strlen(input)-1, 0);
    
    return ret;

}

int main() {
   char *str = "2+1";
   int *p = NULL;
   int rs;

   p = diffWaysToCompute(str, &rs);

   for(int i = 0; i < rs; i++) {
      printf("%d ", p[i]);
   }
   printf("\n");
   free(p);
   return 0;
}
