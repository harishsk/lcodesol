#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int gene_compare(char *start, char *end, int idx, char (*mutated_gene)[8]) {
	int i = 0; 
	for(i = idx ; i < 8; i++) {
		if(start[i] != end[i])
			break;
	}
	if(i >= 8) {
		return -1;
	}else {
		(*mutated_gene)[i] = end[i];
		return i;
	}
}
bool find_bank(char *mutated, char **bank, int bankSize) {
	int bank_cnt = 0;
	for(bank_cnt = 0; bank_cnt < bankSize; bank_cnt++) {
		if(strncmp(mutated, bank[bank_cnt], 8) == 0){
			return true;
		}
	}
	return false;
}
int minMutation(char * start, char * end, char ** bank, int bankSize){
	if(start == NULL || end == NULL || bank == NULL || bankSize < 1)
		return -1;

	char mutated_gene[8];
	int idx = 0, mutated_gene_cnt = 0;

	strncpy(mutated_gene, start, 8);
	while((idx = gene_compare(start, end, idx, &mutated_gene)) != -1) {
		if(find_bank(mutated_gene, bank, bankSize) == true) {
			mutated_gene_cnt++;
		}else {
			return -1;
		}
		mutated_gene[idx] = start[idx];
      idx++;
	}
	return mutated_gene_cnt;
}

int main()
{
   char *start = "AACCGGTT";
   char *end = "AAACGGTA";
   char *bank[3] = {"AACCGGTA", "AACCGCTA", "AAACGGTA"};
   int num;

   num = minMutation(start, end, bank, 3);

   printf("Minimum mutation is %d\n", num);

   return 0;
   
}

