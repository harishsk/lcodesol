int rangeBitwiseAnd(int m, int n) {
   int res = 0;
   for (int i = 31; i >= 0; i--) {
      unsigned int bit_vak = (unsigned int)1 << i;
      if ((n & bit) > 0) {
         if ((m & bit) == 0) { 
            break; 
         }
         res |= bit;
      }
   }
   return res;
}
