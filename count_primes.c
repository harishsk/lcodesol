int countPrimes(int n){
    int flag[n+1];
    memset(flag, 0, sizeof(flag));
    
    for(int i = 2; i * i < n; i++ ) {
        if (flag[i] == 1) continue;
        for(int j = i * i; j < n; j+=i) {
            flag[j] = 1;
        }
    }
    int count = 0;
    for(int i = 2; i < n; i++) {
        if(flag[i] == 0){
            count++;
        }
    }
    return count;
}
