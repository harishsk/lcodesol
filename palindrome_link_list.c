#include <stdio.h>
#include <stdbool.h>
/**
 * Definition for singly-linked list.
 */
struct ListNode {
   int val;
   struct ListNode *next;
};


struct ListNode* reverseList(struct ListNode* head){
	/* Empty LL, or single node LL, nothing to reverse */
	if(head == NULL || head->next == NULL)
		return head;
	
	struct ListNode* prev = NULL, *cur = head, *nxt = head->next;

	while(cur != NULL) {
		nxt = cur->next;
		cur->next = prev;
		prev = cur;
		cur = nxt;
	}

	return prev;
}

bool isPalindrome(struct ListNode* head){
    
   /* Single node LL are palindrome */
   if(head->next == NULL)
      return true;
    
   /* slow and fast pointer to divide ll into almost half*/
   struct ListNode *slow, *fast;
   slow = head;
   fast = head->next->next;
    
   int count = 1;
    
   while(fast != NULL && fast->next != NULL) {
      slow = slow->next;
      fast = fast->next->next;
      count++;
   }

   /* temp points to second half of ll */
   struct ListNode *temp = slow->next;
    
   /* Break two ll */
   slow->next = NULL;
    
   /* reverse second ll */
   temp = reverseList(temp);
    
   /* Check only count number of node val of two ll */
   for(int i = 0; i < count; i++) {
      if(head->val != temp->val){
         return false;
      }
      head = head->next;
      temp = temp->next;
   }
   return true;
}

int main() {
   struct ListNode a, b, c, d;
   a.val = 1;
   b.val = 1;
   c.val = 2;
   d.val = 1;

   a.next = &b;
   b.next = NULL;
   c.next = NULL;
   d.next = NULL;

   printf("Is palindrome ? %d\n", isPalindrome(&a));
}
