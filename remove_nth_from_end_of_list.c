#include <stdio.h>
#include <stdlib.h>

/**
 * Definition for singly-linked list. */
struct ListNode {
   int val;
   struct ListNode *next;
};

#define WITHOUT_DUMMY_NODE
struct ListNode* removeNthFromEnd(struct ListNode* head, int n)
{
   struct ListNode *pnth = NULL, *end = NULL;
   struct ListNode *t = NULL;
   int cnt = 1, remcnt = n;
   
   if( n == 0) return head;
   
   end = head;
   while(end->next) {
      if(remcnt ==0) pnth = head;
      end = end->next;
      if(pnth != NULL) pnth = pnth->next;
      remcnt--;
      cnt++;
   }
   if(remcnt == 0) pnth = head;
   
   if(cnt == n){
      t = head;
      head = head->next;
   }else if(pnth != NULL) {
      t = pnth->next;
      pnth->next = pnth->next->next;
   }
   //if(t) free(t);
   return head;
}
#else // WITH_DUMMY_NODE
struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
	struct ListNode dummy;
	dummy.next = head;

	struct ListNode *p = head, *q;
	int i = n - 1;
	while(p->next != NULL && i > 0) {
		p = p->next;
		i--;
	}

	q = &dummy;
	
	while(p->next != NULL) {
		p = p->next;
		q = q->next;
	}
	q->next = q->next->next;

	return dummy.next;
}
#endif

int main()
{
   struct ListNode a, b, c, d;
   a.val = 1;
   b.val = 10;
   c.val = 12;
   d.val = 15;

   a.next = &b;
   b.next = &c;
   c.next = &d;
   d.next = NULL;

   printf("List First element is %d\n", removeNthFromEnd(&a, 1)->val);

   return 0;
}
