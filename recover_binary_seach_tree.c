/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

#if defined(RECUSIVE_INORDER)

void recoverTree_r(struct TreeNode* root, struct TreeNode** fNode, 
                   struct TreeNode** sNode, struct TreeNode** pNode){
    
    if(root == NULL)
        return;
    
    recoverTree_r(root->left, fNode, sNode, pNode);
    
    if(*pNode == NULL)
        *pNode = root;        
    
    if((*pNode)->val > root->val) {
        if(*fNode == NULL)
            *fNode = *pNode;        
        *sNode = root;
    }
    *pNode = root;
    
    recoverTree_r(root->right, fNode, sNode, pNode);
    
}
void recoverTree(struct TreeNode* root){
    struct TreeNode* fNode = NULL, *sNode = NULL, *pNode = NULL;
    recoverTree_r(root, &fNode, &sNode, &pNode);
    int t = fNode->val;
    fNode->val = sNode->val;
    sNode->val = t;
    return;
}
#else
// MORRIS method

void recoverTree(struct TreeNode* root){
    struct TreeNode* fNode = NULL, *sNode = NULL, *pNode = NULL;
    struct TreeNode* cur = root;
    struct TreeNode* pred = NULL;
    
    /* Morris in order Traversal */
    while(cur != NULL){       
        if(cur->left == NULL){
            /* Node visit */
            if(pNode != NULL && cur->val <= pNode->val){
                if(fNode == NULL)
                    fNode = pNode;         
                sNode = cur;
            }
            pNode = cur;
            cur = cur->right;
        }else {
            pred = cur->left;
            while(pred->right != cur && pred->right != NULL){
                pred = pred ->right;
            }
            if(pred->right == NULL){
                pred->right = cur;                
                cur = cur->left;
            }else {
                /* Node visit */
                if(pNode != NULL && cur->val <= pNode->val){
                    if(fNode == NULL)
                        fNode = pNode;
                    sNode = cur;
                }
                pNode = cur;
                pred->right = NULL;                
                cur = cur->right;
            }
        }
        
    }
    
    /* Swap values of fNode and sNode */
    int t = fNode->val;
    fNode->val = sNode->val;
    sNode->val = t;
    
    return;
}
#endif
