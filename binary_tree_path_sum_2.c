#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

void pathSum_r(struct TreeNode* root, int sum, int ***arr, int *rs,
               int **cs, int csum, int *carr, int clen) {
   if(root == NULL)
      return;
   
   if(root->left == NULL && root->right == NULL) {
      if(csum + root->val == sum) {
         // Found the path
         *rs = *rs + 1;
         (*cs) = realloc(*cs, *rs * sizeof(int));
         (*arr) = realloc(*arr, *rs * sizeof(int*));         
         (*cs)[*rs - 1] = clen + 1;
         (*arr)[*rs - 1] = malloc((*cs)[*rs - 1] * sizeof(int));
         memcpy((*arr)[*rs - 1], carr, clen * sizeof(int));
         (*arr)[*rs - 1][clen] = root->val;
      }
   }else {
      int t[clen + 1];
      memcpy(t, carr, clen *sizeof(int));
      t[clen] = root->val;
      pathSum_r(root->left, sum, arr, rs, cs,
                csum + root->val, t, clen + 1);
      pathSum_r(root->right, sum, arr, rs, cs,
                csum + root->val, t, clen + 1);
   }
}

int** pathSum(struct TreeNode* root, int sum, int* returnSize, int** returnColumnSizes){
   int **ret;
   int carr[1];

   *returnSize = 0;
   *returnColumnSizes = NULL;
   ret = NULL;
   
   pathSum_r(root, sum, &ret, returnSize, returnColumnSizes, 0, carr, 0);
   return ret;
}

int main()
{
   struct TreeNode t1, t2, t3, t4, t5, t6, t7;
   t1.val = 1;
   t2.val = 2;
   t3.val = 3;
   t4.val = 4;
   t5.val = 5;
   t6.val = 6;
   t7.val = 7;

   t1.left = &t2;
   t1.right = &t3;   
   
   t2.left = &t4;
   t2.right = &t5;

   t3.left = &t6;
   t3.right = &t7;


   t4.left = t4.right = NULL;
   t5.left = t5.right = NULL;
   t6.left = t6.right = NULL;
   t7.left = t7.right = NULL;

   int retSize = 0, *reCol = NULL, **p = NULL;
   int sum = 7;
   
   p = pathSum(&t1, sum, &retSize, &reCol);

   for(int i = 0; i < retSize; i++) {
      for(int j = 0; j < reCol[i]; j++) {
         printf("%d ", p[i][j]);
      }
      free(p[i]);
      printf("\n");
   }
   free(reCol);
   //free(p);

   return 0;

      
}
