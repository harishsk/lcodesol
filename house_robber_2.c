#include <stdio.h>
#include <stdbool.h>

#define MAX(x, y) ((x) > (y) ? (x) : (y))
int rob(int* nums, int numsSize){
    if(nums == NULL || numsSize < 1)
        return 0;
    
    int dp[numsSize];
    bool select_first = false;
    
    if(nums[0] >= nums[numsSize -1]){
        dp[0] = nums[0];
        select_first = true;
    }else{
        dp[0] = 0;
    }
    if(numsSize == 1)
       return dp[0];
    
    dp[1] = MAX(nums[0], nums[1]);
    if(numsSize == 2)
        return dp[1];
        
    for(int i = 2; i < numsSize; i++) {
        dp[i] = MAX(nums[i] + dp[i-2], dp[i-1]);
    }
    if(select_first == true)
       return dp[numsSize-2];
    else
       return dp[numsSize-1];
}

int main() {
   int house[] = {1,2,3,1};
   printf("Max money robber robs is %d\n", rob(house, sizeof(house)/sizeof(house[0])));
   return 0;
}
