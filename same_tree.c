/*
https://leetcode.com/problems/same-tree/

Given two binary trees, write a function to check if they are the same or not.

Two binary trees are considered the same if they are structurally identical and the nodes have the same value.

Example 1:

Input:     1         1
          / \       / \
         2   3     2   3

        [1,2,3],   [1,2,3]

Output: true
Example 2:

Input:     1         1
          /           \
         2             2

        [1,2],     [1,null,2]

Output: false
Example 3:

Input:     1         1
          / \       / \
         2   1     1   2

        [1,2,1],   [1,1,2]

Output: false
 */

#include <stdio.h>
#include <stdbool.h>

/**
 * Definition for a binary tree node.
 */
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

bool check_node(struct TreeNode* p, struct TreeNode* q);

bool isSameTree(struct TreeNode* p, struct TreeNode* q)
{
   if(check_node(p, q)) {
      return true;
   }else {
      return false;
   }
}

bool check_node(struct TreeNode* p, struct TreeNode* q)
{
   if(p == NULL && q == NULL) {
      return true;
   }
   if(p == NULL || q == NULL) {
      return false;
   }   
   
   if(p->val != q->val) {
      return false;
   }else {
      return check_node(p->left, q->left) && check_node(p->right, q->right);
   }
}

int main()
{
   struct TreeNode r1, a1, b1;
   struct TreeNode r2, a2, b2;
   
   r1.left = &a1;
   r1.right = &b1;
   a1.left = NULL;
   b1.left = NULL;
   a1.right = NULL;
   b1.right = NULL;   

   r2.left = &a2;
   r2.right = &b2;
   a2.left = NULL;
   b2.left = NULL;
   a2.right = NULL;
   b2.right = NULL;

   r1.val = 1;
   a1.val = 2;
   b1.val = 3;

   r2.val = 1;
   a2.val = 2;
   b2.val = 3;

   if(isSameTree(&r1, &r2)) {
      printf("Trees are the same\n");
   }else {
      printf("Trees are not same\n");
   }
   return 0;
}
