#include "utstack.h"
#include "type.h"

typedef struct el_tag {
	int val;
	struct el_tag *next;
}el_t;

int evalRPN(char ** tokens, int tokensSize){
	el_t *st = NULL, *sel = NULL, *sel2 = NULL;
	int token_idx = 0, ret_ans;

	for(token_idx = 0; token_idx < tokensSize; token_idx++) {
		if(isdigit(tokens[token_idx][0])) {
			sel = malloc(sizeof(el_t));
			sel->val = atoi(tokens[token_idx]);
			STACK_PUSH(st, sel);
		}else if(strcmp(tokens[token_idx], "+")) {
			STACK_POP(st, sel2);
			STACK_POP(st, sel);
			sel->val = sel->val + sel2->val;
			STACK_PUSH(st, sel);
			free(sel2);
		}else if(strcmp(tokens[token_idx], "*")) {
			STACK_POP(st, sel2);
			STACK_POP(st, sel);
			sel->val = sel->val * sel2->val;
			STACK_PUSH(st, sel);
			free(sel2);
		}else if(strcmp(tokens[token_idx], "-")) {
			STACK_POP(st, sel2);
			STACK_POP(st, sel);
			sel->val = sel->val - sel2->val;
			STACK_PUSH(st, sel);
			free(sel2);
		}else if(strcmp(tokens[token_idx], "/")) {
			STACK_POP(st, sel2);
			STACK_POP(st, sel);
            printf("val %d val2 %d\n", sel->val, sel2->val);
			sel->val = sel->val / sel2->val;
            printf("val %d val2 %d\n", sel->val, sel2->val);
            
			STACK_PUSH(st, sel);
			free(sel2);
		}
	}
	STACK_POP(st, sel);
	ret_ans = sel->val;
	free(sel);
	return ret_ans;
}
