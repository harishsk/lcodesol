/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

struct ListNode* reverse(struct ListNode* head){
    struct ListNode* prev = NULL;
    struct ListNode* cur = head;
    struct ListNode* next = NULL;
    
    while(cur != NULL) {
        next = cur->next;
        cur->next = prev;
        prev = cur;
        cur = next;
    }
    return prev;
}

bool isSame(struct ListNode* head1, struct ListNode* head2){
    bool same = true;
    while(head1 != NULL && head2 != NULL){
        if(head1->val != head2->val) {
            same = false;
            break;
        }
        head1= head1->next;
        head2= head2->next;
    }
    return same;
}

bool isPalindrome(struct ListNode* head){
    if(head == NULL || head->next == NULL)
        return true;
    
    struct ListNode *slow, *fast, *prev;
    fast = slow = head;
    prev = NULL;
    
    while(fast != NULL && fast->next != NULL){
        prev = slow;
        slow = slow->next;
        fast = fast->next->next;
    }
    prev->next = NULL;
    struct ListNode *r = reverse(slow);
    
    return isSame(head, r);

}
