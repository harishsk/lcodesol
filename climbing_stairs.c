/*
https://leetcode.com/problems/climbing-stairs/
You are climbing a stair case. It takes n steps to reach to the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

Example 1:

Input: 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps
Example 2:

Input: 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step
 */

#include <stdio.h>
#include <stdlib.h>

int climbStairs(int n)
{
   int *a = malloc(sizeof(int)*(n+1));
   int i;
   int ways;
   
   a[0] = 1;
   a[1] = 1;
   for(i = 2; i <= n; i++) {
      a[i] = a[i-1] + a[i-2];
   }
   ways = a[n];
   free(a);
   return ways;

}
 
int main()
{
   int stairs = 5;
   int climb_ways = 0;

   climb_ways = climbStairs(stairs);

   printf("%d stairs can be climbed in %d ways\n", stairs, climb_ways);

   return 0;
}
