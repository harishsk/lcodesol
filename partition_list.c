#include <stdio.h>
#include <stdlib.h>

/**
 * Definition for singly-linked list.
 */
struct ListNode {
     int val;
     struct ListNode *next;
};

struct ListNode * create_node(int val)
{
   struct ListNode *p = malloc(sizeof(struct ListNode));
   p->next = NULL;
   p->val = val;
   return p;
}

void append_node(struct ListNode *p, int a)
{
   while(p->next) {
      p = p->next;
   }
   p->next = create_node(a);

}

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}


void append_list(struct ListNode**head, struct ListNode**tail, struct ListNode *node)
{
    if(*head == NULL) {
        *head = node;
    }
    if(*tail == NULL) {
        *tail = node;
    }else {
        (*tail)->next = node;
        *tail = node;
    }
    
}

void combine_list(struct ListNode**head, struct ListNode**tail, struct ListNode *chead, struct ListNode *ctail)
{
    if(*head == NULL || *tail == NULL) {
        *head = chead;
        *tail = ctail;
    }else {
        (*tail)->next = chead;
        *tail = ctail;
    }
}

struct ListNode* partition(struct ListNode* head, int x){
    struct ListNode* cur = head;
    struct ListNode* next = head;
    struct ListNode* l_head = NULL;
    struct ListNode* l_tail = NULL;
    struct ListNode* e_head = NULL;
    struct ListNode* e_tail = NULL;
    struct ListNode* g_head = NULL;
    struct ListNode* g_tail = NULL;
    struct ListNode* c_head = NULL;
    struct ListNode* c_tail = NULL;
    
    
    if(head == NULL || head->next == NULL) {
        return head;
    }
    
    while(cur) {
        next = cur->next;
        if(cur->val < x) {
            append_list(&l_head, &l_tail, cur);
        }else if(cur->val > x){
            append_list(&g_head, &g_tail, cur);
        }else {
            append_list(&e_head, &e_tail, cur);
        }
        cur = next;
    }
    combine_list(&c_head, &c_tail, l_head, l_tail);
    combine_list(&c_head, &c_tail, e_head, e_tail);
    combine_list(&c_head, &c_tail, g_head, g_tail);
    c_tail->next = NULL;
    
    return c_head;

}

int main()
{
   struct ListNode *a;
   a = create_node(1);
   append_node(a, 4);
   append_node(a, 3);
   append_node(a, 2);
   append_node(a, 5);
   append_node(a, 2);   

   print_list(a);
   
   print_list(partition(a, 3));

   return 0;
}
