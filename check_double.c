#define TEST

#if defined(TEST)
#include <stdio.h>
#include <stdbool.h>
#include "uthash.h"
#endif

bool checkIfExist(int* arr, int arrSize){
   for(int i = 0; i < arrSize; i++) {
      for(int j = i + 1; j < arrSize; j++) {
         if(arr[i] == (arr[j] * 2) || (arr[i] * 2) == arr[j])
            return true;
      }
   }
   return false;
}

#if defined(TEST)
int main()
{
   int a[] = {7,1,14,11};
   printf("exists %d\n", checkIfExist(a, sizeof(a)/sizeof(a[0])));
   return 0;
}
#endif
