/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

bool isValidBST_r(struct TreeNode* root, int *lbound, int *ubound){
    bool lcheck, rcheck;
    int left_lbound, left_ubound, right_lbound, right_ubound;
    
    if(root == NULL)
        return true;
    
    
    lcheck = isValidBST_r(root->left, &left_lbound, &left_ubound);
    if(root->left && root->val <= left_ubound)
        return false;
    
    rcheck = isValidBST_r(root->right, &right_lbound, &right_ubound);
    if(root->right && root->val >= right_lbound)
        return false;
    
    if(root->right == NULL) 
        *ubound = root->val;
    else 
        *ubound = right_ubound;
    
    if(root->left == NULL) 
        *lbound = root->val;
    else 
        *lbound = left_lbound;
    
    return lcheck & rcheck;
    
}
bool isValidBST(struct TreeNode* root){
    int lbound, ubound;
    
    return isValidBST_r(root, &lbound, &ubound);
    
}
