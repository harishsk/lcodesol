#include <bits/stdc++.h>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution{
   /* Recursive function of solution function */
   void isSymmetic_r(TreeNode* p, TreeNode *q, bool& sym) {
      /* If tree is sym no point in continuing */
      if(sym == false) return;
      /* If p and q are nullptr we have reached leaf node
         so dont continue */
      if(p == nullptr and q == nullptr) return;

      /* If any of p/q is nullptr other not then tree is not symmetric 
         If the val in p and q are not equal then tree is not symmetric */
      if((p == nullptr and q != nullptr)
         or (p != nullptr and q == nullptr)
         or (p->val != q->val)){
         sym = false;
         return;
      }
      /* Continue down the tree, 
         - comparing left to right 
         - comparing right to left */
      this->isSymmetic_r(p->left, q->right, sym);
      this->isSymmetic_r(p->right, q->left, sym);
      return;
   }
public:
    bool isSymmetric(TreeNode* root) {
       bool sym = true;
       /* If root is nullptr it is assumed to be symmetrical */
       if(root == nullptr)
          return sym;

       this->isSymmetic_r(root->left, root->right, sym);
       return sym;
    }
};
int main() {
   TreeNode t4(4), t5(5), t6(6), t7(7);
   TreeNode t2(2, &t4, &t5); 
   TreeNode t3(3, &t6, &t7);  
   TreeNode t1(1, &t2, &t3);

   Solution s;


   cout << s.isSymmetric(&t1) << "\n";  
}
