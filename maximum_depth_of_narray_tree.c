#define MAX(x, y) ((x) > (y)? (x) : (y))
void maxDepth_r(struct Node* root, int height, int *max) {
   if(root == NULL)
      return;
   if(root->numChildren == 0) {
      *max = MAX(height, *max);
      return;
   }
   for(int i = 0; i < root->numChildren; i++)
      maxDepth_r(root->children[i], height + 1, max);
   
   return;
}

int maxDepth(struct Node* root) {
   int max = 0;
   if(root == NULL)
      return max;
   maxDepth_r(root, 1, &max);
   return max;
}
