#if defined(ITERATIVE_METHOD)
struct ListNode* reverseList(struct ListNode* head){
	/* Empty LL, or single node LL, nothing to reverse */
	if(head == NULL || head->next == NULL)
		return head;
	
	struct ListNode* prev = NULL, *cur = head, *nxt = head->next;

	while(cur != NULL) {
      nxt = cur->next;
		cur->next = prev;
		prev = cur;
		cur = nxt;
	}

	return prev;
}
#else
struct ListNode* reverseList(struct ListNode* head){
 	/* Empty LL, nothing to reverse */
	if(head == NULL || head->next == NULL)
 		return head;
   
   struct ListNode *nhead = reverseList(head->next);
   head->next->next = head;
   head->next = NULL;
   return nhead;
}
#endif
