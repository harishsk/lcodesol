int minSubArrayLen(int s, int* nums, int numsSize){
	int csum = 0;
	int low, high, min_dist = INT_MAX;
	
    if(nums == NULL || numsSize <=0) 
        return 0;
    
	if(nums[0] == s)
		return 1;

	low = 0;
	high = 1;
	csum = nums[0] + nums[1];
	while(low <= high && high < numsSize) {
        printf("low %d high %d csum %d\n", low, high, csum);
		if(csum > s) {
			csum = csum - nums[low];
			if(low == high)
				high++;
			low++;
		}else if(csum < s){
			high++;
            if(high >= numsSize)
                break;
			csum = csum + nums[high];
		}else {
            if(high == low)
				return 1;
            
			if(high - low + 1  < min_dist) 
				min_dist = high - low + 1;
            csum = csum - nums[low];
            low++;
		}

	}
	if(min_dist == INT_MAX)
		return 0;
	else
		return min_dist;
}
