/*
https://leetcode.com/problems/merge-two-sorted-lists/
Merge two sorted linked lists and return it as a new sorted list. The new list should be made by splicing together the nodes of the first two lists.

Example:

Input: 1->2->4, 1->3->4
Output: 1->1->2->3->4->4
 */
#include <stdio.h>
#include <stdlib.h>

struct ListNode {
   int val;
   struct ListNode *next;
};


void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}

#if define(FIRST_TRY)
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2){

   struct ListNode* t = NULL;
   struct ListNode* reth = NULL, *ret = NULL;
   
   while(l1 != NULL && l2 != NULL) {
      t = (struct ListNode*)malloc(16);
      if(l1->val == l2->val) {   
         t->val = l1 ->val;
         l1 = l1->next;
      }else if(l1->val < l2->val){
         t->val = l1->val;
         l1 = l1->next;
      }else {
         t->val = l2->val;
         l2 = l2->next;
      }
      if(reth == NULL) {
         reth = ret = t;
         t->next = NULL;
      }else {
         ret->next = t;
         ret = ret->next;
      }         
   }
   while(l1) {
      t = (struct ListNode*)malloc(16);
      t->val = l1->val;
      if(reth == NULL) {
         reth = ret = t;
         t->next = NULL;
      }else {
         ret->next = t;
         ret = ret->next;
      }  
      l1 = l1->next;
   }
   while(l2) {      
      t = (struct ListNode*)malloc(16);
      t->val = l2->val;
      if(reth == NULL) {
         reth = ret = t;
         t->next = NULL;
      }else {
         ret->next = t;
         ret = ret->next;
      }   
      l2 = l2->next;
   }
   if(ret)
      ret->next = NULL; 
   
   return reth;
}
#else // REFINED
struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2){
    /* ml is dummy node ml.next will point to merged list */
	/* cur will help in merging list */
    struct ListNode ml, *cur;
    
    ml.next = NULL;
    cur = &ml;
    /* Merge comparing val */  
    while(l1 != NULL && l2 !=NULL) {
        if(l1->val <= l2->val) {
            cur->next = l1;
            l1 = l1->next;
        }else {
            cur->next = l2;
            l2 = l2->next;
        }
        cur = cur->next;
    }
    /* Append any remaining l1 */
    while(l1 != NULL){
        cur->next = l1;
        l1 = l1->next;
        cur = cur->next;
    }
	 /* Append any remaining l2 */
    while(l2 != NULL){
        cur->next = l2;
        l2 = l2->next;
        cur = cur->next;
    }
    return ml.next;

}
#endif

int main()
{
   struct ListNode a, b, c;
   struct ListNode x, y, z;
   
   struct ListNode* m = NULL;
   
   
   a.val = 1;
   a.next = &b;

   b.val = 2;
   b.next = NULL;

   c.val = 4;
   c.next = NULL;

   x.val = 1;
   x.next = &y;

   y.val = 3;
   y.next = &z;

   z.val = 4;
   z.next = NULL;   

   printf("A list is : ");
   print_list(&a);
   
   printf("X list is : ");
   print_list(&x);
   
   
   m = mergeTwoLists(&a, &x);
   
   printf("Merged list is: ");
   print_list(m);
   
   return 0;
}
