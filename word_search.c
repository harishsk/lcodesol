#include <stdio.h>
#include <stdbool.h>

static inline bool exist_r(char** board, int rowS, int colS, int s_ridx, int s_cidx,
	char * word) {
	if(word[0] == '\0')
		return true;
	if(s_ridx < 0 || s_ridx >= rowS) 
		return false;
	if(s_cidx < 0 || s_cidx >= colS) 
		return false;
	if (board[s_ridx][s_cidx] != word[0]) 
          return false;

	char old = word[0];
	board[s_ridx][s_cidx] = '-';

	if(exist_r(board, rowS, colS, s_ridx - 1, s_cidx, word + 1))
		return true;
	
	if(exist_r(board, rowS, colS, s_ridx + 1, s_cidx, word + 1))
		return true;

	if(exist_r(board, rowS, colS, s_ridx, s_cidx - 1, word + 1))
		return true;

	if(exist_r(board, rowS, colS, s_ridx, s_cidx + 1, word + 1))
		return true;
	
	board[s_ridx][s_cidx]= old;
	
	return false;
}
bool exist(char **board, int boardSize, int* boardColSize, char * word){
	int cidx, ridx;
	
	if(board == NULL || boardSize == 0 || *boardColSize == 0 || word == NULL)
		return false;

	for(ridx = 0; ridx < boardSize; ridx++) {
		for(cidx = 0; cidx < *boardColSize; cidx++) {
			if(exist_r(board, boardSize, *boardColSize,
			   	        ridx, cidx, word))
				return true;
		}
	}
	return false;
}
int main()
{
   char a[3][5] = { {'A','B','C','E'}, {'S','F','C','S'}, {'A','D','E','E'}};
   char *p = "SEE";
   int col = 5;
   printf("%s exists in a ? - %d", p, exist((char**)a, 3, &col, p));
   return 0;
}

