#include <bits/stdc++.h>
#include <string>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
       unordered_map<char,int> m;
       /* Initialize map for all combination of char to -1 */
       for(char i = numeric_limits<char>::min(); i < numeric_limits<char>::max() ; i++){
          m[i] = -1;
       }

       /* To find the maximum length */
       int maxlen = 0, len = 0;
       
       /* Index to left of string, from which new 
          sub-string length is marked */
       int left = 0;

       for(int i = 0 ; i < s.length(); i++){
          char c = s[i];
          if(m.find(c) == m.end()) {
             /* If charecter not found in map,
                it is unique hence increase the len */
             len++;
          }else {
             /* If charecter found in the map */
             if(m[c] >= left) {
                /* Update the length and left for new length 
                   calculation, as the charecter repeated */
                left = m[c] + 1;
                len = i - m[c];
             }else{
                /* Charecter is repeating but may be found before 
                   left, not the current length measurement */
                len++;
             }
          }
          /* Update map with c as key and i as value */
          m[c] = i;

          maxlen = max(maxlen, len);
       }
       return maxlen; 
    }
};
int main(){
   string str = "abcabcbb";
   Solution s;

   cout << s.lengthOfLongestSubstring(str) << "\n";

   return 0;
}
