struct inthash {
    int num;
    UT_hash_handle hh;
};

static inline void hash_insert(struct inthash **ihash, int num) {
     struct inthash *el;
     HASH_FIND_INT(*ihash, &num, el);
     if(el == NULL) {
          el = malloc(sizeof(struct inthash));
          el->num = num;
          HASH_ADD_INT(*ihash, num, el);
     }
     return;
}
int subarrayBitwiseORs(int* A, int ASize){
    struct inthash *ihash = NULL, *el = NULL;
    int count = 0;
    int or = 0;
    
    for(int idx = 0; idx < ASize; idx++){
        int j = idx - 1;
        int x = 0, y = A[idx];
        hash_insert(&ihash, y);
        while(j>=0 && x!=y){
                x|=A[j];
                y|=A[j];
                hash_insert(&ihash, y);
                j--;
        }
    }
    count = HASH_COUNT(ihash);
    struct inthash *temp;
    HASH_ITER(hh, ihash, el, temp) {
         HASH_DEL(ihash,el);
         free(el);
    }
    return count;
}
