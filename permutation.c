#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_clen(const char *s, int **c, int clen, int nS) {
   printf("%s", s);
   for(int i = 0; i < clen; i++) {
      printf("%d ", (*c)[i]);
   }
   for(int i = 0; i < nS - clen; i++) {
      printf("- ");
   }
   printf("\n");
}

void add_back(int ***ret, int *rS, int **rC, int **cur, int nS) {
   *rC = realloc(*rC, sizeof(int) * (*rS + 1));
   *ret = realloc(*ret, sizeof(int*) * (*rS + 1));
   (*rC)[*rS] = nS;
   (*ret)[*rS] = malloc(sizeof(int) * nS);
   memcpy((*ret)[*rS], *cur, sizeof(int)* nS);
   *rS = *rS + 1;
}
void permute_r(int *nums, int nS, int *rS, int **rC, int ***ret,
               int **p, int plen, int **r, int rlen)
{
   if(plen == nS) {
      add_back(ret, rS, rC, p, nS);
      return;
   }
   for (int i = 0; i < rlen; i++){
      (*p)[plen] = (*r)[i];    
      int *rnew = malloc(sizeof(int) * (rlen - 1));
      memcpy(rnew, *r, sizeof(int) * i);
      memcpy(rnew + i, *r + i + 1, sizeof(int) * (rlen - i -1));
      permute_r(nums, nS, rS, rC, ret, p, plen + 1, &rnew, rlen - 1);
      free(rnew);
   }
   return;
}

int** permute(int* nums, int numsSize, int* returnSize, int** returnColumnSizes){
   int **ret = NULL;
   
   int *p = malloc(sizeof(int) * numsSize);
   memset(p, 0, sizeof(int) * numsSize);  // p is empty
   
   int *r = malloc(sizeof(int) * numsSize);
   memcpy(r, nums, sizeof(int) * numsSize); // r has all the nums
   
   *returnColumnSizes = NULL;
   *returnSize = 0;

   permute_r(nums, numsSize, returnSize, returnColumnSizes, &ret, &p, 0, &r, numsSize);
   
   free(p);
   free(r);
   
   return ret;   
}

int main(){
   int a[] = {1, 2, 3};
   int *retCol;
   int **p;
   int rs;
   p = permute(a, sizeof(a)/ sizeof(a[0]), &rs, &retCol);

   for(int i = 0; i < rs; i++) {
      for(int j = 0; j < retCol[i]; j++) {
         printf("%d ", p[i][j]);
      }
      free(p[i]);
      printf("\n");
   }
   free(p);
   free(retCol);
   return 0;
}
