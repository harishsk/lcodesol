

int maxProfit(int* prices, int pricesSize){
    if(prices == NULL || pricesSize < 2)
        return 0;
    int minprice = INT_MAX, max_profit = 0;
    
    for(int i = 0; i < pricesSize; i++){
        if(prices[i] < minprice) {
           minprice = prices[i]; 
        }else if(prices[i] - minprice > max_profit) {
           max_profit =  prices[i] - minprice;
        }
    }
    return max_profit;
}
