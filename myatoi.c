#include <stdbool.h>
#include <type.h>
#include <stdtype.h>
#include <limits.h>
int myAtoi(char * str)
{
	bool is_done = false, ignore_ws = 1;
	int idx = 0, sign = 1;
	int64_t num = 0;
   
	while(!is_done && str[idx] != '\0') {
		if(isdigit(str[idx])) {
			ignore_ws = 0;
			num = num * 10;
			num = num + (str[idx] - '0');
			if(num != (int)num)
				is_done = true;
		}else if(str[idx] == ' ' && ignore_ws == 1) {
      }else if(str[idx] == ' ' && ignore_ws == 0) {
         is_done = true;
		}else if(str[idx] == '+' && ignore_ws == 1) {
			sign = 1;
			ignore_ws = 0;
		}else if(str[idx] == '-' && ignore_ws == 1) {
			sign = -1;
			ignore_ws = 0;
      }else{
         is_done = true;
      }
		idx++;
	}
   
	if(num == (int)num) {
		return (int)num * sign;
   }else {
      if(num * sign < 0) 
         return INT_MIN;
      else
         return INT_MAX;
   }
}

