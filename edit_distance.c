/* Link:
https://leetcode.com/problems/edit-distance
*/

/* Problem:
Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.

You have the following 3 operations permitted on a word:

    Insert a character
    Delete a character
    Replace a character

Example 1:

Input: word1 = "horse", word2 = "ros"
Output: 3
Explanation: 
horse -> rorse (replace 'h' with 'r')
rorse -> rose (remove 'r')
rose -> ros (remove 'e')

Example 2:

Input: word1 = "intention", word2 = "execution"
Output: 5
Explanation: 
intention -> inention (remove 't')
inention -> enention (replace 'i' with 'e')
enention -> exention (replace 'n' with 'x')
exention -> exection (replace 'n' with 'c')
exection -> execution (insert 'u')


*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

/* Solution: */
#define MIN_TWO(x, y) ((x) < (y) ? (x) : (y))
#define MIN(x, y, z) (MIN_TWO(x,MIN_TWO(y,z)))
int minDistance(char * word1, char * word2){
   int m = strlen(word1), n = strlen(word2);
   int **dp = malloc(sizeof(int*) * (m + 1));
   for(int r = 0; r < (m + 1); r++) 
      dp[r] = malloc(sizeof(int) * (n + 1));

   /* Three base cases */
   /* 1. Null string can be transformed to Null string in 0 steps */
   dp[0][0] = 0;

   /* 2. String of length r can be trasformed to Null string in r steps */
   for (int r = 1; r <= m; r++) 
      dp[r][0] = r;

   /* 3. Null string can be trasformed to string with len r in r steps */   
   for (int c = 1; c <= n; c++) 
      dp[0][c] = c;

   /* If letters at i - 1 and j -1 is 
      - Same : then no new steps required 
      - Different: Smallest of 3 corner + 1
   */
   for (int i = 1; i <= m; i++) {
      for (int j = 1; j <= n; j++) {
         if (word1[i - 1] == word2[j - 1]) {
            dp[i][j] = dp[i - 1][j - 1];
         } else {
            dp[i][j] = MIN(dp[i - 1][j - 1], dp[i][j - 1], dp[i - 1][j]) + 1;
         }
      }
   }
   int min_dist = dp[m][n];
   for(int r = 0; r < (m + 1); r++)
      free(dp[r]);
   free(dp);
   return min_dist;
}
/* Test Driver: */
int main() {
   char *word1 = "horse", *word2 = "gorse";
   printf("minimum distance is %d\n", minDistance(word1, word2));
   return 0;
}
