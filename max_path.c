public class Solution {
    int maxValue;
    
    public int maxPathSum(TreeNode root) {
        maxValue = Integer.MIN_VALUE;
        maxPathDown(root);
        return maxValue;
    }
    
    private int maxPathDown(TreeNode node) {
        if (node == null) return 0;
        int left = Math.max(0, maxPathDown(node.left));
        int right = Math.max(0, maxPathDown(node.right));
        maxValue = Math.max(maxValue, left + right + node.val);
        return Math.max(left, right) + node.val;
    }
}

#define MAX(a, b) ((a) > (b) ? (a) : (b))

int maxPathSumInt(struct TreeNode* root, int *max)
{
    if (root == NULL) {
        return 0;
    }
    
    int left = maxPathSumInt(root->left, max);
    int right = maxPathSumInt(root->right, max);
    
    left = MAX(left, 0);
    right = MAX(right, 0);

    // XXX: what about int overflows?
    int curr = left + root->val + right;
    *max = MAX(*max, curr);

    return root->val + MAX(left, right);
}
    

int maxPathSum(struct TreeNode* root)
{
    int max = INT_MIN;
    maxPathSumInt(root, &max);
    return max;
}


#define MAX(x,y) ((x) > (y) ? (x): (y))

int maxPathSum_r(struct TreeNode* root, int *max){
    if(root == NULL)
        return 0;
    int lsum = maxPathSum_r(root->left, max);
    int rsum = maxPathSum_r(root->right, max);

    lsum = MAX(lsum, 0);
    rsum = MAX(rsum, 0);    
    
    #if 0
    printf("node %d\n", root->val);
    printf("lsum %d\n", lsum);
    printf("rsum %d\n", rsum);
    #endif

    int cur = lsum + rsum + root->val;
    *max = MAX(*max, cur);
    
    return root->val + MAX(lsum, rsum);
}
int maxPathSum(struct TreeNode* root){
    int max = INT_MIN;
    maxPathSum_r(root, &max);
    return max;
}

