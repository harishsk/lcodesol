struct ListNode* merge(struct ListNode*l1, struct ListNode*l2){
	struct ListNode dummy, *cur = &dummy;
	while(l1 != NULL && l2 != NULL) {
		if(l1->val < l2->val) {
			cur->next = l1;
			l1 = l1->next;
		}else {
			cur->next = l2;
			l2 = l2->next;
		}
		cur = cur->next;
	}
	if(l1 != NULL)
		cur->next = l1;
	if(l2 != NULL)
		cur->next = l2;
	return dummy.next;
}
struct ListNode* sortList(struct ListNode* head){
	if(head == NULL || head->next == NULL)
		return head;
	if(head->next->next == NULL) {
        if(head->val > head->next->val) {
		    struct ListNode* temp = head->next;
		    head->next = NULL;
		    temp->next = head;
		    return temp; 
        }else {
            return head;
        }
	}
	struct ListNode *slow_prev = NULL, *slow = head, *fast = head;
	
	while(fast != NULL && fast->next != NULL) {
		slow_prev = slow;
		slow = slow->next;
		fast = fast->next->next;
	}
	slow_prev->next = NULL;
	
	struct ListNode *l1, *l2;
	l1 = sortList(head);
	l2 = sortList(slow);
	l1 = merge(l1, l2);
	return l1;
}
