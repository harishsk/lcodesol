void countXandO(char **board, int boardSize, int *cnt_x, int *cnt_o) {
   for(int row = 0; row < boardSize; row++) {
      for(int col = 0; col < boardSize; col++) {
         if(board[row][col] == 'X') {
            *cnt_x = *cnt_x + 1;
         }else if(board[row][col] == 'O') {
            *cnt_o = *cnt_o + 1;
         }
      }
   }
   return;
}
bool checkWinner(char **board, int boardSize, char ch){
   for(int row = 0; row < boardSize; row++) {
      int found = true;
      for(int col = 0; col < boardSize; col++) {
         if(ch != board[row][col]) {
            found = false;            
            break;
         }
      }
      if(found == true) {
         //printf("%c row %d match\n", ch, row); 
         return true;
      }
   }
   for(int col = 0; col < boardSize; col++) {
      int found = true;
      for(int row = 0; row < boardSize; row++) {
         if(ch != board[row][col]) {
            found = false;
            break;
         }
      }
      if(found == true) {
          //printf("%c col %d match\n", ch, col);
         return true;         
      }
   }
   int found = true;
    for(int row = 0, col = 0; row < boardSize; row++, col++) {
       if(ch != board[row][col]) {
          found = false;
          break;
       }
    }
    if(found == true) {
       //printf("%c diag match - 1\n", ch);
       return true;
    }    
    found = true;
    for(int row = boardSize -1, col = boardSize -1; row >= 0; row--, col--) {
       if(ch != board[row][col]) {
          found = false;
          break;
       }
    }
    if(found == true) {
       // printf("%c diag match - 2\n", ch);
       return true;       
    }
    
    return false;
}

bool validTicTacToe(char ** board, int boardSize){
    int cnt_x = 0, cnt_o = 0;
    bool is_x_winner = false, is_o_winner = false;
    countXandO(board, boardSize, &cnt_x, &cnt_o);
    if(!(cnt_x == cnt_o || cnt_x == cnt_o + 1)) {
       //printf("Count missmatch\n");
       return false;
    }
    is_x_winner = checkWinner(board, boardSize, 'X');
    is_o_winner = checkWinner(board, boardSize, 'O');
    //printf("%d %d\n", is_x_winner, is_o_winner);
    if(is_o_winner && is_x_winner){
       //printf("Both winner\n");
       return false;
    }
    if(is_x_winner && cnt_x == cnt_o) {
        return false;
    }
    if(is_o_winner && cnt_x == (cnt_o + 1)) {
        return false;
    }
    return true;
}
int main()
{
   
}
