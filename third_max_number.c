/* Link:
   https://leetcode.com/problems/third-maximum-number/
*/

/* Problem:
Given integer array nums, return the third maximum number in this array. If the third maximum does not exist, return the maximum number.

 

Example 1:

Input: nums = [3,2,1]
Output: 1
Explanation: The third maximum is 1.
Example 2:

Input: nums = [1,2]
Output: 2
Explanation: The third maximum does not exist, so the maximum (2) is returned instead.
Example 3:

Input: nums = [2,2,3,1]
Output: 1
Explanation: Note that the third maximum here means the third maximum distinct number.
Both numbers with value 2 are both considered as second maximum.
 

Constraints:

1 <= nums.length <= 104
-231 <= nums[i] <= 231 - 1
  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
int thirdMax(int* nums, int numsSize){
   long m[3] = { LONG_MIN, LONG_MIN, LONG_MIN };
   int cnt = 0;

   for(int i = 0; i < numsSize; i++) {
      if(nums[i] == m[0] || nums[i] == m[1] || nums[i] == m[2])
         continue;
      if(nums[i] > m[0]) {
         m[2] = m[1];
         m[1] = m[0];
         m[0] = nums[i];
         cnt++;
      }else if(nums[i] > m[1]) {
         m[2] = m[1];
         m[1] = nums[i];
         cnt++;
      }else if(nums[i] > m[2]) {
         m[2] = nums[i];
         cnt++;
      }
   }
   if(cnt < 3) {
      return m[0];
   }else {
      return m[2];
   }
}

/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = {1,2,-2147483648};

   printf("3rd Max is %d\n", thirdMax(a, sizeof(a)/sizeof(a[0])));
   
   return 0;
}
#endif

#if defined comment
* `m` contains three max, `m[0]` - largest, `m[1]` - second largest, `m[2]` - third largest.
* `m` is `long` since the nums lowest value can be `-2^31` which is `INT_MIN`.
* `cnt` is used to count number of updates to `m`. Since three update to m is required to get the third largest.
```
int thirdMax(int* nums, int numsSize){
   long m[3] = { LONG_MIN, LONG_MIN, LONG_MIN };
   int cnt = 0;

   for(int i = 0; i < numsSize; i++) {
      /* to make sure unique elements are counted */
      if(nums[i] == m[0] || nums[i] == m[1] || nums[i] == m[2])
         continue;
      if(nums[i] > m[0]) {
         m[2] = m[1];
         m[1] = m[0];
         m[0] = nums[i];
         cnt++;
      }else if(nums[i] > m[1]) {
         m[2] = m[1];
         m[1] = nums[i];
         cnt++;
      }else if(nums[i] > m[2]) {
         m[2] = nums[i];
         cnt++;
      }
   }
   if(cnt < 3) {
      return m[0];
   }else {
      return m[2];
   }
}
```
#endif
