struct ListNode * detectCycle(struct ListNode *head) {
	if(head == NULL || head->next == NULL) 
		return false;
	struct ListNode *slow = head->next, *fast = head->next->next;
	bool found_cycle = false;
	
	while(fast != NULL && fast->next != NULL) {
		if(slow == fast) {
			found_cycle = true;
			break;
		}
		slow = slow->next;
		fast = fast->next->next;
	}
	if(!found_cycle)
		return NULL;
	struct ListNode *cur = head;
	while(cur != slow) {
		cur = cur->next;
		slow = slow->next;
	}
	return cur;
}
