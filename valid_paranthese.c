/*
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct node_t {
   int c;
   struct node_t *next;
   struct node_t *prev;   
};

struct node_t *head = NULL;
struct node_t *tail = NULL;

int push(char c)
{
   struct node_t *pn = malloc(sizeof(struct node_t ));
   if(pn == NULL) {
      printf("Heap is full\n");
      return -1;
   }
   pn->c = c;
   if(head == NULL) {
      head = pn;
      tail = pn;
      tail->next = NULL;
      tail->prev = NULL;
   }else {
      tail->next = pn;
      pn->prev = tail;
      pn->next = NULL;
      tail = pn;
   }
   return 0;
}
int pop(char *pc)
{
   struct node_t *pn = NULL;
   if(tail == NULL) {
      printf("Heap is empty\n");
      return -1;
   }
   *pc = tail->c;
   if(tail->prev == NULL) {
      tail = NULL;
      head = NULL;
   }else {
      pn = tail;
      tail = pn->prev;
      tail->next =NULL;
      free(pn);
   }
   return 0;
}
bool empty()
{
   return (head == NULL);
}
void init()
{
   struct node_t *pn = NULL;
   while(head) {
      pn = head;
      head = head->next;
      free(pn);
   }
   tail = NULL;
   head = NULL;
      
}

inline static bool isOpen(char s) {
   return s == '(' || s == '{' || s == '[';
}

inline static bool isClose(char s) {
   return s == ')' || s == '}' || s == ']';
}
inline static char getOpen(char s) {
   char r;
   switch(s) {
   case ')':
      r = '(';
      break;
   case '}':
      r = '{';
      break;
   case ']':
      r = '[';
      break;
   default:
      r = 0;
      break;
   }
   return r;
}


bool isValid(char * s){
  int i, len;
  char c, p;
  bool val = true;

  init();
  len = strlen(s);
  
  for(i = 0; i < len; i++) {
     if(isOpen(s[i])) {
        push(s[i]);
     }
     if(isClose(s[i])){
        c = getOpen(s[i]);
        pop(&p);
        if(c != p) {
           val = false;
           break;
        }
     }
  }
  if(empty() == true) {
     return val;
  }else {
     return false;
  }

}

int main()
{
   char *p = "{[]}";
   char c;

   printf("Is valid %d\n", isValid(p));

}
