int cmpfunc(int** a, int** b)
{
   if((*a)[0]==(*b)[0]) 
      return (*a)[1] - (*b)[1];
   else             
      return (*a)[0] - (*b)[0];
}

int overlapped(int** intervals, int idx1, int idx2) {
	bool is_overlap = false;
	if((intervals[idx2][1] > intervals[idx1][0] &&
       intervals[idx2][1] < intervals[idx1][1]) ||
		(intervals[idx2][0] > intervals[idx1][0] &&
       intervals[idx2][0] < intervals[idx1][1])) {
		is_overlap = true;
	}else if((intervals[idx1][1] > intervals[idx2][0] &&
             intervals[idx1][1] < intervals[idx2][1]) ||
            (intervals[idx1][0] > intervals[idx2][0] &&
             intervals[idx1][0] < intervals[idx2][1])) {
		is_overlap = true;
	}else if((intervals[idx1][0] == intervals[idx2][0]) &&
            (intervals[idx1][1] == intervals[idx2][1])) {
		is_overlap = true;
	}
	if(!is_overlap)
		return -1;
	if((intervals[idx1][1] - intervals[idx1][0]) > 
		(intervals[idx2][1] - intervals[idx2][0])) {
		return idx1;
	}else {
		return idx2;
	}
}
int eraseOverlapIntervals(int** intervals, int intervalsSize, int* intervalsColSize){
   
   if(intervals == NULL || intervalsSize == 0 || *intervalsColSize != 2)
      return 0;
   
   bool itr_ex[intervalsSize];
	int idx1, idx2, overlapped_idx, num_of_overlap = 0;
   
	memset(itr_ex, 0, sizeof(bool) * intervalsSize);
	
	qsort(intervals, intervalsSize, sizeof(int*), cmpfunc);
   
   
	for(idx1 = 0; idx1 < intervalsSize ; idx1++) {
		for(idx2 = idx1 + 1; 
          idx2 < intervalsSize; 
          idx2++) {
         if(itr_ex[idx1] != true && itr_ex[idx2] != true) {
            overlapped_idx= overlapped(intervals, idx1, idx2);                
            if(overlapped_idx != -1) {
               num_of_overlap++;
               itr_ex[overlapped_idx]=true;
            }
            }
		}
	}
	return num_of_overlap;
}

