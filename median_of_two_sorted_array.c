/*
https://leetcode.com/problems/median-of-two-sorted-arrays/
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

You may assume nums1 and nums2 cannot be both empty.

Example 1:

nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:

nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

#define OPTIM

#if defined(OPTIM)

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

void get_pair(int *a, int asize, int i, int *left, int *right) {
   if(i == 0) {
      *left = INT_MIN;
      *right = a[i];
   }else if(i == asize) {
      *left = a[i-1];
      *right = INT_MAX;
   }else {
      *left = a[i-1];
      *right = a[i];
   }
   return;
}
double findMedian(int* p, int psize, int i, int* q, int qsize, int j){
   double med;
   int pleft, pright, qleft, qright;
   
   get_pair(p, psize, i, &pleft, &pright);
   get_pair(q, qsize, j, &qleft, &qright);
   
   if((psize + qsize)%2) {
      // Odd 1
      med = MIN(pright, qright);
   }else {
      // Even 2
      med = (MAX(pleft, qleft) + MIN(pright, qright)) / 2.0;
   }
   return med;
}
#if defined(LINEAR)
bool isSorted(int* p, int psize, int i, int* q, int qsize, int j)
{
   int pleft, pright, qleft, qright;
   
   get_pair(p, psize, i, &pleft, &pright);
   get_pair(q, qsize, j, &qleft, &qright);

   if((pleft <= qright) && (qleft <= pright)) {
      return true;
   }else {
      return false;
   }
}
#endif
int array_comp(int* p, int psize, int i, int* q, int qsize, int j)
{
   int pleft, pright, qleft, qright;
   
   get_pair(p, psize, i, &pleft, &pright);
   get_pair(q, qsize, j, &qleft, &qright);

   if((pleft <= qright) && (qleft <= pright)) {
      return 0;
   }else if(pleft > qright) {
      return -1;
   }else {
      return 1;
   }
   
}
double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size)
{
   double med = 0;
   int *p = NULL, *q = NULL;
   int psize, qsize;
   int i, j;

   if((nums1 == NULL || nums1Size < 1) &&
      (nums2 == NULL || nums2Size < 1)) {
      return 0.0;
   }else if(nums1 == NULL || nums1Size < 1) {
      med = (nums2[nums2Size/2] + nums2[(nums2Size -1)/2])/2.0;
      return med;
   }else if(nums2 == NULL || nums2Size < 1) {
      med = (nums1[nums1Size/2] + nums1[(nums1Size -1)/2])/2.0;
      return med;      
   }
   
   p = nums1; psize = nums1Size;
   q = nums2; qsize = nums2Size;
   if(nums1Size > nums2Size) {
      p = nums2; psize = nums2Size;
      q = nums1; qsize = nums1Size;
   }

#if defined(LINEAR)
   for(i = 0; i <= psize; i++) {
      j = (psize + qsize)/2 - i;
      if(isSorted(p, psize, i, q, qsize, j)) {
         break;
      }
   }
   return findMedian(p, psize, i, q, qsize, j);
#else
   int mid = 0, start, end, ret;
   start = 0;
   end = psize;
   mid = (start + end)/2;
   while(mid >= 0 && mid <=psize) {
      j = (psize + qsize)/2 - mid;

      #if 0      
      printf("start %d mid %d end %d j %d\n", start, mid, end, j);

      if(isSorted(p, psize, mid, q, qsize, j)) {
         break;
      }
      #endif
      ret = array_comp(p, psize, mid, q, qsize, j);
      if(ret == 0) {
         break;
      }else if(ret < 0) {
         end = mid;
         mid = (start + end - 1)/2;
      }else if(ret > 0) {
         start = mid;
         mid = (start + end + 1)/2;
      }
   }
   return findMedian(p, psize, mid, q, qsize, j);
#endif

   
}
#else
double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size)
{
   int *r = malloc(sizeof(int) * (nums1Size + nums2Size));
   int ir = 0, i1 = 0, i2 = 0;

   if(r == NULL && nums1 == NULL && nums2 == NULL) {
      return 0.0;
   }

   while(i1 < nums1Size && i2 < nums2Size) {
      if(nums1[i1] < nums2[i2]) {
         r[ir++] = nums1[i1++];
      }else {
         r[ir++] = nums2[i2++];
      }
   }

   while(i1 < nums1Size) {
      r[ir++] = nums1[i1++];
   }

   while(i2 < nums2Size) {
      r[ir++] = nums2[i2++];
   }

   return (r[(nums1Size + nums2Size - 1)/2] + r[( nums1Size + nums2Size)/2]) / 2.0;
   
}
#endif

int main()
{
   int n1[] = {1, 2};
   int n2[] = {3, 4};
   double m;

   m = findMedianSortedArrays(n1, sizeof(n1)/sizeof(n1[0]), n2, sizeof(n2)/sizeof(n2[0]));

   printf("Median is %f\n", m);

   return 0;
}

