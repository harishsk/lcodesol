/*
Link: https://leetcode.com/problems/unique-paths/

Question:
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

How many possible unique paths are there?

 

Example 1:

Input: m = 3, n = 7
Output: 28

Example 2:

Input: m = 3, n = 2
Output: 3
Explanation:
From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -> Down -> Down
2. Down -> Down -> Right
3. Down -> Right -> Down

Example 3:

Input: m = 7, n = 3
Output: 28

Example 4:

Input: m = 3, n = 3
Output: 6

 

Constraints:

    1 <= m, n <= 100
    It's guaranteed that the answer will be less than or equal to 2 * 109.


 */
/* Solution: */
#define RECURSIVE_SOLUTION
//#define DYNAMIC_PROGRAMMING

#include <stdio.h>


#if defined(RECURSIVE_SOLUTION)
static inline void uniquePaths_r(int m, int n, int x, int y, int *nums_paths) {
    if(x == n && y == 0) {
        *nums_paths += 1;
        return;
    }
    if(y > 0) { // Down
        uniquePaths_r(m, n, x, y - 1, nums_paths);
    }
    if(x < n) { // Right
        uniquePaths_r(m, n, x + 1, y, nums_paths);
    }
    return;
}

int uniquePaths(int m, int n){
    int num_paths = 0;
    // (0,2) -> (6,0)
    uniquePaths_r(m - 1, n - 1, 0, m - 1, &num_paths);
    return num_paths;
}
#else // DYNAMIC_PROGRAMMING
int uniquePaths(int m, int n)
{
   if(m == 0 || n == 0)
      return 0;
   int path_matrix[m][n];
   for(int row = 0; row < n; row ++) {
      for(int col = 0; col < m; col++) {
         if(row == 0 || col == 0) {
            path_matrix[col][row] = 1;
         }else {
            path_matrix[col][row] = path_matrix[col-1][row] +
                                    path_matrix[col][row-1];
         }
      }
   }
   return path_matrix[m-1][n-1];
}
#endif

/* Driver Code */
int main(){
   int m = 7, n = 3;
   printf("Unique Paths are %d\n", uniquePaths(m, n));
   return 0;
}
