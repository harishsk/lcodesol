#include <stdio.h>
#include <string.h>
#include <limits.h>


int firstUniqChar(char * s)
{
   int cnt[256], i, low = INT_MAX;

   memset(cnt, -1, sizeof(cnt));

   for(i = 0; s[i] != 0; i++) {
      if(cnt[s[i]] == -1)
         cnt[s[i]] = i;
      else if(cnt[s[i]] >= 0)
         cnt[s[i]] = -2;
   }

   for(i = 0; i < sizeof(cnt)/sizeof(cnt[0]) ; i++) {
      if(cnt[i] >= 0) {
         if(low < cnt[i]) {
            low = cnt[i];
         }
      }
   }
   if(low == INT_MAX) {
      return -1;
   }else {
      return low;
   }
}

int main()
{
   int *p = "leetcode";
}
