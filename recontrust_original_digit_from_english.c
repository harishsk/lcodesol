#include <stdio.h>

char str[50000/3];
char * originalDigits(char * s){
	if(s == NULL)
		return NULL;
	int idx, len = 0;
	int char_cnt[26] = {0};
	int digit[10] = {0};
   
   str[0] = '\0';
   
	for(idx = 0; s[idx] != '\0'; idx++) {
		char_cnt[s[idx] - 'a']++;
	}
   digit[0] = char_cnt['z' - 'a'];
	digit[2] = char_cnt['w' - 'a'];
	digit[4] = char_cnt['u' - 'a'];
	digit[6] = char_cnt['x' - 'a'];
	digit[8] = char_cnt['g' - 'a'];

	digit[7] = char_cnt['s' - 'a'] - digit[6];
	digit[1] = char_cnt['o' - 'a'] - digit[2] - digit[0] - digit[4];
	digit[3] = char_cnt['h' - 'a'] - digit[8];
	digit[5] = char_cnt['f' - 'a'] - digit[4];
	digit[9] = char_cnt['i' - 'a'] - digit[5] - digit[6] - digit[8];   
	
	for(idx = 0; idx < 10; idx++) {
      for(int rep = 0; rep < digit[idx]; rep++) {
         //len += sprintf(str + len, "%d", idx);
         str[len++] = idx + '0';
      }
	}
   str[len] = '\0';
   return str;
}

int main()
{
   char *p = "sixninefiveeight";
   //char *p = "owoztneoer";

   p = originalDigits(p);
   
   printf("%s\n", p);
   
   return 0;
}


