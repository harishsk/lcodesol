/*
Given two binary strings, return their sum (also a binary string).

The input strings are both non-empty and contains only characters 1 or 0.

Example 1:

Input: a = "11", b = "1"
Output: "100"
Example 2:

Input: a = "1010", b = "1011"
Output: "10101"
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 10000

char arr[MAX_LEN];

char * addBinary(char * a, char * b)
{
   int k = 0, i , j, s, r = 0;
   char *p = &arr[0];
   
   memset(arr, 0 , MAX_LEN);

   k = MAX_LEN - 2;
   p[k + 1] = '\0';

   i = strlen(a) - 1;
   j = strlen(b) - 1;
   
   while(i >=0 && j >=0) {
      s = r + (a[i]-'0') + (b[j] - '0');
      p[k] = (s % 2) + '0';
      r = s / 2;
      s = 0;
      k--; i--; j--;
   }
   while(i >=0 ){
      s = r + (a[i] - '0');
      p[k] = (s % 2) + '0';
      r = s / 2;
      s = 0;
      k--; i--;      
   }
   while(j >=0 ){
      s = r + (b[j] - '0');
      p[k] = (s % 2) + '0';
      r = s / 2;
      s = 0;
      k--; j--;
   }
   if(r > 0){
      p[k] = '1';
      k--;
   }

   return &p[k+1];
}
int main()
{
   char *a = "1010", *b = "101";
   char *p = NULL;

   p = addBinary(a, b);
   
   printf("Sum of %s and %s is %s\n", a, b, p);

   return 0;
   
}
