#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(NCR_METHOD)
#define MAX 21
long fact[MAX];
bool is_fact_int = false;

void calc_fact(void) {
	int i = 1;
	fact[0] = 1;
	for(i = 1; i < MAX; i++) {
		fact[i] = fact[i -1] * i;
	}
}
int ncr(int n, int r) {
	return fact[n]/(fact[n-r] * fact[r]);
}
void add_back(int ***ret, int *rS, int **rCS, int val, int level) {
	if(level > *rS) {
		*ret = realloc(*ret, sizeof(int*) * (*rS + 1));
		(*ret)[*rS] = NULL;
		*rCS = realloc(*rCS, sizeof(int) * (*rS + 1));
		(*rCS)[*rS] = 0;
		(*ret)[*rS] = realloc((*ret)[*rS], sizeof(int) * ((*rCS)[*rS] + 1));
		(*ret)[*rS][(*rCS)[*rS]] = val;
		(*rCS)[*rS] += 1;
		*rS += 1;
	}else {
		int row = *rS - 1;
		(*ret)[row] = realloc((*ret)[row], sizeof(int) * ((*rCS)[row] + 1));
		(*ret)[row][(*rCS)[row]] = val;
		(*rCS)[row] += 1;
	}
	return;
}

int** generate(int numRows, int* returnSize, int** returnColumnSizes){
	*returnSize = 0;
	*returnColumnSizes = NULL; 
	if(numRows == 0)
		return NULL;
	if(is_fact_int == false) {
		calc_fact();
		  is_fact_int = true;
	 }
	int **ret = 0;

	for(int row_cnt = 0; row_cnt < numRows; row_cnt++) {
		for(int col_cnt = 0; col_cnt <= row_cnt; col_cnt++) {
			int num = ncr(row_cnt, col_cnt);
			add_back(&ret, returnSize, returnColumnSizes, num, row_cnt + 1);
		}
	}
	return ret;
}
#else
int** generate(int numRows, int* returnSize, int** returnColumnSizes) {
	*returnSize = 0;
	*returnColumnSizes = NULL;
	
	if(numRows == 0)
		return NULL;
	
	int **ret = NULL;
	
	*returnSize = numRows;
	
	ret =	 malloc (sizeof(int *) * numRows);
	for (int row = 0; row < numRows; row++) {
		ret[row] = malloc (sizeof(int) * (row + 1));
		ret[row][0] = 1;   // First col is 1
		ret[row][row] = 1; // Last col is 1
	}
		
	*returnColumnSizes = malloc(sizeof(int) * numRows);
	for (int row = 0; row < numRows; row++) 
		(*returnColumnSizes)[row] = row + 1;

   /* Middle col calculation */
	for (int row = 2; row < numRows; row++) {
		for (int col = 1; col < row; col++){
			ret[row][col] = ret[row - 1][col - 1] + ret[row - 1][col];
		}
	}
	return ret;
}
#endif
int main() {
	int n = 10;

	int S, *CS = NULL;
	int **p = generate(n, &S, &CS);

	for(int i = 0; i < S; i++) {
		for(int j = 0; j < CS[i]; j++) {
			printf("%d ", p[i][j]);
		}
		printf("\n");
		free(p[i]);
	}
	free(CS);
	free(p);
	return 0;
}
