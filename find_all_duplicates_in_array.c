#include <stdio.h>
#include <stdlib.h>
#define ABS(x) ((x) >= 0 ? (x) : -(x))
int* findDuplicates(int* nums, int numsSize, int* returnSize){
	if(nums == NULL || numsSize < 2) {
		*returnSize = 0;
		return NULL;
	}
	*returnSize = 0;
	int *ret = (int*)malloc(sizeof(int)* numsSize);;
	for(int idx = 0; idx < numsSize; idx++) {
      int pos = ABS(nums[idx]) - 1;
		if(nums[pos] < 0){
			ret[*returnSize] = pos + 1;
			*returnSize = *returnSize + 1;
		}else {
			nums[pos] = nums[pos] * -1;
		}
	}
	return ret;
}

int main()
{
   int arr[] = {4,3,2,7,8,2,3,1};
   int *p, size, idx;

   p = findDuplicates(arr, sizeof(arr)/sizeof(arr[0]), &size);

   for(idx = 0; idx < size; idx++) {
      printf("%d ", p[idx]);
   }
   printf("\n");
   free(p);

   return 0;
}
