#include <iostream>
#include <vector>
using namespace std;
class Solution {
public:
   int removeDuplicates(vector<int>& nums) {
      int k = 1;
      for(int i = 1; i < nums.size(); i ++){
         if(nums[i] != nums[k]) {
            nums[k] = nums[i];
            k++;
         }
      }
      return k;
   }
};

int main()
{
   Solution s;
   vector <int>v{1,1,2};
   int r;
   r = s.removeDuplicates(v);
   for(int i = 0; i < r; i++){
      cout << v[i] <<" ";
   }
   cout << endl;
   return 0;
}
