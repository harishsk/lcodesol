#include <stdio.h>
#include <stdlib.h>

int main()
{
#if defined(IN_STACK_VAR)
   int a = 10;
   int *p = &a;

   *p = 20;
   
   printf("a is %d, &a is %p, p is %p, *p is %d\n", a, &a, p, *p);
#elif defined (IN_HEAP_VAR)
   int *p = malloc(sizeof(int));

   *p = 20;
   
   printf("p is %p, *p is %d\n", p, *p);
#elif defined(IN_STACK_ARR)
   int a[3] = {1, 2, 3};
   int *p = a;

   p[0] = 10;
   p[1] = 20;
   a[2] = 30;

   printf("a is %p p is %p\n", a , p);
   for(int i = 0; i < sizeof(a)/sizeof(a[0]); i++) {
      printf("i is %d, a[i] is %d, &a[i] %p, p[i] is %d, p + i is %p\n",
             i, a[i], &a[i], p[i], p + i);
      printf("&p[i] %p\n", &p[i]);
   }
#elif defined(IN_HEAP_ARR)
   int len = 3;
   int *p = malloc(sizeof(int)*len);

   p[0] = 10;
   p[1] = 20;
   p[2] = 30;

   printf("p is %p\n", p);
   for(int i = 0; i < len; i++) {
      printf("i is %d, p[i] is %d, p + i is %p &p[i] %p\n",
             i, p[i], p + i, &p[i]);
   }

#elif defined(IN_STACK_ARR_PTR)
   int *a[3] = {NULL}, x, y, z;
   int **p = a;

   a[0] = &x;
   a[1] = &y;
   p[2] = &z;

   *p[0] = 10;
   *p[1] = 20;
   *a[2] = 30;

   printf("x is %d, &x is %p\n", x, &x);
   printf("y is %d, &x is %p\n", y, &y);
   printf("z is %d, &x is %p\n", z, &z); 

   printf("a is %p p is %p\n", a , p);
   for(int i = 0; i < sizeof(a)/sizeof(a[0]); i++) {
      printf("i is %d, a[i] is %p, *a[i] %d, p[i] is %p, p + i is %p\n",
             i, a[i], *a[i], p[i], p + i);
      printf("*p[i] %d, *p + i %p\n", *p[i], *p + i);
   }
#elif defined(IN_HEAP_ARR_PTR)
   int len = 3;
   int **p = malloc(sizeof(int*) * len);

   p[0] = malloc(sizeof(int));
   p[1] = malloc(sizeof(int));
   p[2] = malloc(sizeof(int));   
   
   *p[0] = 10;
   *p[1] = 20;
   *p[2] = 30;

   printf("p is %p\n", p);
   for(int i = 0; i < len; i++) {
      printf("i is %d, p[i] is %p, p + i is %p\n",
             i, p[i], p + i);
      printf("*p[i] %d, *p + i %p\n", *p[i], *p + i);
   }
#elif defined(IN_HEAP_ARR_PTR_FUNC)
   void change_values(int **p);
   int len = 3;
   int **p = malloc(sizeof(int*) * len);

   p[0] = malloc(sizeof(int));
   p[1] = malloc(sizeof(int));
   p[2] = malloc(sizeof(int));   

   
   *p[0] = 10;
   *p[1] = 20;
   *p[2] = 30;

   change_values(p);

   printf("p is %p\n", p);
   for(int i = 0; i < len; i++) {
      printf("i is %d, p[i] is %p, p + i is %p\n",
             i, p[i], p + i);
      printf("*p[i] %d, *p + i %p\n", *p[i], *p + i);
   }
#else
   void change_values(int **ptr, int idx, int val);
   int len = 3;
   int **p = malloc(sizeof(int*) * len);

   change_values(p, 0, 100);
   change_values(p, 1, 200);
   change_values(p, 2, 300);   

   printf("p is %p\n", p);
   for(int i = 0; i < len; i++) {
      printf("i is %d, p[i] is %p, p + i is %p\n",
             i, p[i], p + i);
      printf("*p[i] %d, *p + i %p\n", *p[i], *p + i);
   }
#endif

   return 0;
}

#if defined(IN_HEAP_ARR_PTR_FUNC)
void change_values(int **ptr)
{
   *ptr[0] = 100;
   *ptr[1] = 200;
   *ptr[2] = 300;   
}
#else
void change_values(int **ptr, int idx, int val)
{
   int *p = malloc(sizeof(int));
   ptr[idx] = p;
   *ptr[idx] = val;
}
#endif
