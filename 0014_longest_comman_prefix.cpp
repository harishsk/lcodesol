#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
   string longestCommonPrefix(vector<string>& strs) {
      int i, j;
      string ret="";
      if(strs.size() == 0)
         return ret;
      if(strs.size() == 1)
         return strs[0];
      for(i = 0; i < 200; i++) {
         int f = 1;
         for(j = 0; j < strs.size() - 1; j++) {
            if((strs[j][i] == 0) || (strs[j][i] != strs[j + 1][i])) {
               return ret;
            }
         }
         ret += strs[j][i];
      }
      return ret;
   }
};

int main()
{
   Solution s;
   vector <string> strs{"flower","flow","flight"};
   string r;
   r = s.longestCommonPrefix(strs);
   cout << "Longest common prefix is " << r << endl;
   return 0;
}
