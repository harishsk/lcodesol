#include <stdio.h>

int removeDuplicates(int* nums, int numsSize){
   int i, k = 2;
   if(numsSize < 3) {
      return numsSize;
   }
   for(i = 2; i < numsSize; i++) {
      if(nums[i] != nums[i - 1] ||
         nums[i] != nums[i - 2]) {
         
         nums[k] = nums[i];
         k++;
      }
      for(int j = 0; j < numsSize; j++) 
         printf("%d ", nums[j]);
      printf("\n");
   }
   return k;
}

int main()
{
   int a[] = {1, 1, 1, 2, 2, 3};
   int n = 0, i;

   n = removeDuplicates(a, sizeof(a)/sizeof(int));

   for(i = 0; i < n; i++) {
      printf(" %d,", a[i]);
   }
   printf("\n");
}
