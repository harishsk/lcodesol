#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;
class Solution {
public:
   vector<int> twoSum(vector<int>& nums, int target) {
      /* Map will hold key:number and value:index pair */
      unordered_map<int,int> map;
      vector<int> ret = {};
      for(int i = 0; i < nums.size(); ++i) {
         /* Find the number that will add to target */
         if(map.find(target - nums[i]) != map.end()){
            /* If found return both the indexes */
            ret.push_back(map[target-nums[i]]);
            ret.push_back(i);
            return ret;
         }
         /* Add it to map for further iteration */
         map.insert({nums[i], i});
      }
      /* Return empty vector in case its not match is not found */
      return ret;
   }
};
int main() {
   Solution s;
   vector<int> n{1, 3, 4, 5, 2 };
   vector<int> pair = {};

   pair = s.twoSum(n, 5);

   cout << endl;
   for(int i = 0; i < pair.size(); i++) {
      cout << pair[i] << " " ;
   }
   cout << endl;

   return 0;
}
