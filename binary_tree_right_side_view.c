/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


int height(struct TreeNode *root) {
   if(root == NULL) {
      return 1;
   }
   int lh = height(root->left) + 1;
   int rh = height(root->right) + 1;

   return (lh > rh ? lh : rh);
}

void rightSideView(struct TreeNode* root, int level, int **ret){
   if(root == NULL)
      return;

   if((*ret)[level] == 0)
      (*ret)[level] = root->val;
   rightSideView(root->right, level + 1, ret);
   rightSideView(root->left, level + 1, ret);   
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* rightSideView(struct TreeNode* root, int* returnSize){
   if(root == NULL) {
      *returnSize = 0;
      return NULL;
   }
   int h = height(root);
   int *ret = calloc(sizeof(int), h);
   *returnSize = h;
   rightSideView_r(root, 0, &ret);
   return ret;
}
