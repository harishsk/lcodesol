#include <stdio.h>

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

typedef struct queue_node_tag {
   int val;
   struct queue_node_tag *next;
}queue_node_t;

typedef struct queue_tag {
   queue_node_t *head;
   queue_node_t *tail;
}queue_t;

static inline void init_queue(queue_t *q)
{
   q->head = NULL;
   q->tail = NULL;
}

static inline void en_queue(queue_t *q, int val)
{
   queue_node_t *t = malloc(sizeof(queue_node_t));

   t->val = val;
   t->next = NULL;   
   
   if(q->head == NULL) 
      q->head = t;
   
   if(q->tail != NULL)
      q->tail->next = t;
      
   q->tail = t;
}

static inline int de_queue(queue_t *q)
{
   int ret;
   queue_node_t *t = q->head;

   if(t == NULL)
      return -1;
   
   q->head = t->next;
   ret = t->val;
   free(t);
   
   return ret;
}

char r[1000];

char * convert(char * s, int numRows)
{
   int slen = strlen(s), dir = -1, i, j, c;
   queue_t qrow[numRows];

   if(slen < 2 || numRows == 1 || slen < numRows)
      return s;
   
   for(i = 0; i < numRows; i++) 
      init_queue(&qrow[i]);
   
   for(i = 0, j = 0; i < slen; i++, j+=dir) {
      if((i % (numRows - 1)) == 0)
         dir = dir * -1;      
      en_queue(&qrow[j], s[i]);
   }
   for(i = 0, j = 0; i < numRows; i++) {
      while((c = de_queue(&qrow[i])) != -1)
         r[j++] = (char)c;
   }
   r[j] = '\0';
   return r;
}

int main()
{
   char *p = "PAYPALISHIRING";
   int row = 3;

   printf("ZigZag converted string is %s\n", convert(p, row));

   return 0;
   
}
