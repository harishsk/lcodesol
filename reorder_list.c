/* Link:
https://leetcode.com/problems/reorder-list/
*/

/* Problem:
Given a singly linked list L: L0->L1->...->Ln-1->Ln,
reorder it to: L0->Ln->L1->Ln-1->L2->Ln-2->

You may not modify the values in the list's nodes, only nodes itself may be changed.

Example 1:

Given 1->2->3->4, reorder it to 1->4->2->3.

Example 2:

Given 1->2->3->4->5, reorder it to 1->5->2->4->3.


*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

/* Solution: */
struct ListNode* reverse(struct ListNode* head) {
    struct ListNode *prev = NULL,*cur = head, *next;
    
    while(cur != NULL){
        next = cur->next;
        cur->next = prev;
        prev = cur;
        cur = next;
    }
    return prev;
}
void interleave(struct ListNode* head1, struct ListNode* head2) {
    struct ListNode* cur1 = head1, *cur2 = head2; 
    while(cur1) {
        struct ListNode*cur1_next = cur1->next;
        cur1->next = cur2;
        struct ListNode*cur2_next = cur2->next;
        if(cur1_next == NULL) {
            cur2->next = cur2_next;
        }else {
            cur2->next = cur1_next;
        }
        cur1 = cur1_next;
        cur2 = cur2_next;
    }
    return;
}
void reorderList(struct ListNode* head){
    if(head == NULL || head->next == NULL)
        return head;
    struct ListNode* slow = head, *sprev = NULL, *fast = head;
    
    while(fast != NULL && fast->next != NULL){
        sprev = slow;
        slow = slow->next;
        fast = fast->next->next;
    }
    sprev->next = NULL;
    slow = reverse(slow);
    interleave(head, slow);
    return;
}

/* Test Driver: */

