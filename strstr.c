/*
Implement strStr().
Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

Example 1:

Input: haystack = "hello", needle = "ll"
Output: 2
Example 2:

Input: haystack = "aaaaa", needle = "bba"
Output: -1
 */

#include <stdio.h>

int strStr(char * haystack, char * needle)
{
   int i, j;

   if(needle[0] == 0) {
      return 0;
   }
   
   for(i = 0; haystack[i] !=0 ; i++) {
      if(haystack[i] != needle[0]){
         continue;
      }
      for(j = 1; needle[j] != 0; j++) {
         if(haystack[i + j] != needle[j]) {
            break;
         }         
         if(haystack[i + j] == 0){
            return -1;
         }

      }
      if(needle[j] == 0) {
         return i;
      }
   }
   return -1;
}

int main()
{
   char *haystack = "aaa";
   char *needle = "aaaa";

   printf("Found %s in %s at %d\n", needle, haystack, strStr(haystack, needle));

   return 0;

}
