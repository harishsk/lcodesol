/* Link:
https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/   
*/

/* Problem:
Given an array of integers where 1 <= a[i] <= n (n = size of array), some elements appear twice and others appear once.

Find all the elements of [1, n] inclusive that do not appear in this array.

Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count as extra space.

Example:

Input:
[4,3,2,7,8,2,3,1]

Output:
[5,6]  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <unistd.h>
#endif
#if defined(TEST)
void print_array(int *p, int n) {
   for(int i = 0; i < n; i++) 
      printf("%d ", p[i]);
   printf("\n");
   return;
}
#endif

/* Solution: */

void add_to_missing_array(int **r, int *rS, int i) {
   *rS+=1;
   *r = realloc(*r, sizeof(int) * (*rS));
   (*r)[*rS - 1] = i;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* findDisappearedNumbers(int* nums, int numsSize, int* returnSize){
   for(int i = 0; i < numsSize; i++) {
      if(nums[i] > numsSize){
         continue;
      }else if(nums[i] == (i + 1)) {
         continue;
      }else if(nums[i] != (i + 1)) {
         int temp = nums[nums[i] - 1];
         if(nums[nums[i] - 1] != nums[i]) {
            nums[nums[i] - 1] = nums[i];
            nums[i] = temp;
            i--;
         }
      }
   }
   int *ret = NULL;
   *returnSize = 0;
   for(int i = 0; i < numsSize; i++) {
      if(nums[i] != (i + 1))
         add_to_missing_array(&ret, returnSize, i + 1);
   }
   return ret;
}

/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = {4,3,2,7,8,2,3,1};
   int *r, s;

   r = findDisappearedNumbers(a, sizeof(a)/sizeof(a[0]), &s);

   for(int i = 0; i < s; i++) {
      printf("%d ", r[i]);
   }
   printf("\n");
   free(r);
   
   return 0;
}
#endif
