/*
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
 */
#include <stdio.h>
#include <string.h>

char ret[1024];

char * longestCommonPrefix(char ** strs, int strsSize){
  int i, j = 0;
  int x = 0, y = 0;
  
  memset(ret, 0, 1024);

  if(strsSize == 0) {
     return ret;
  }
  
  while(1) {
    y = strs[0][j];
    for(i = 0; i < strsSize; i++){
      if(strs[i][j] == 0)
         goto done;
      
      x ^= strs[i][j];
    
      if(!((x == 0) || (x == y))) {
         goto done;
      }
    }
    x = 0;
    j++;    
  }
 done:
  strncpy(ret, strs[0], j);
  return ret;
}

int main()
{
   char *str[] = {"","",""};

   printf("Longest common prefix %s\n", longestCommonPrefix((char**)&str, 3));

   return 0;
}
