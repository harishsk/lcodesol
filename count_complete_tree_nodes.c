/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

void countNodes_r(struct TreeNode* root, int *num) {
   if(root == NULL)
      return;
   *num = *num + 1;
   countNodes_r(root->left, num);
   countNodes_r(root->right, num);   
   return;
}

int countNodes(struct TreeNode* root){
   int num = 0;
   countNodes(root, &num);
   return num;
}
