/*
https://leetcode.com/problems/longest-substring-without-repeating-characters/
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */

#include <stdio.h>
#include <string.h>


#if defined(OPTIMIZE)

int lengthOfLongestSubstring(char * s)
{
   int cnt[256];
   int i, max_len = 0, len = 0, x = 0;

   memset(cnt, -1, sizeof(cnt));
   
   for(i = 0; s[i] != 0; i++) {      
      if (cnt[s[i]] == -1 || cnt[s[i]] < x) {
         printf("%c %i", s[i], i);
         cnt[s[i]] = i;
         len++;
         printf(" len %d\n", len);
      }else {
         x = cnt[s[i]] + 1;
         len = i - x + 1;
         cnt[s[i]] = i;
         printf(" len %d\n", len);         
      }
      
      if(len > max_len) {
         max_len = len;
      }
   }
   return max_len;

}

#elif OLD_METHOD

int lengthOfLongestSubstring(char * s)
{
   int cnt[256];
   int i, j, max_len = 0, len = 0;

   for(i = 0; s[i] != 0; i++) {
      memset(cnt, 0, sizeof(cnt));
      for(j = i; s[j] != 0; j++) {
         if (cnt[s[j]] == 0) {
            cnt[s[j]]++;
            len++;
         }else {
            break;
         }
      }
      if(len > max_len) {
         max_len = len;
      }
      len = 0;
   }
   return max_len;

}
#else /*FURTHER_IMPROVED */
int lengthOfLongestSubstring(char * s){
	int map[256];
	int max_len = 0, len = 0, low = 0, hig = 0, i = 0;
	memset(map, -1 , sizeof(map));
	/* Loop through every character */
	while(s[i] != '\0') {
		/* If present in map low index + 1*/
		if(map[s[i]] == -1) {
			map[s[i]] = i;
			hig = i;
			len++;
		}else {
			int x = map[s[i]];
			for(int j = low; j <= x; j++) {
				map[s[j]] = -1;
			}
			map[s[i]] = i;
			low = x + 1;
         hig = i;
			len = hig - low + 1;
		}
      i++;
		if(len > max_len) 
			max_len = len;
	}
	return max_len;
}


#endif

int main()
{
   //char *p = "pwwkew";
   char *p = "tmmzuxt";
   //char *p = "abcabcbb";
   //char *p = "bbtablud";
   //char *p = "  ";   
   int lss = 0;

   lss = lengthOfLongestSubstring(p);

   printf("Lenght of Longest substring of %s is %d\n", p, lss);

   return 0;
   
}
