#include <stdio.h>

int multiply_r(int a, int b) {
   if(a == 0) return 0;
   if(a == 1) return b;
   
   if(a & 1) // ODD
      return (multiply_r(a >> 1, b) << 1) + b;
   else
      return (multiply_r(a >> 1, b) << 1);

}
int multiply(int a, int b) {
   /* x is smaller of a and b */
   int x = a < b ? a : b;
   int y = a > b ? a : b;

   return multiply_r(x, y);

}
int main() {
   int a = 10000, b = 5000;
   printf("Multiplication of %d and %d is %d\n", a, b, multiply(a, b));
   return 0;
}
