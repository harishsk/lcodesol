void hasPathSum_r(struct TreeNode* root, int csum, int sum, bool *is_sum){
	if(*is_sum == true) 
		return;
	if(root->left == NULL && root->right == NULL) {
		*is_sum = (csum + root->val) == sum;
        return;
	}
    if(root->left != NULL)
	    hasPathSum_r(root->left, csum + root->val, sum, is_sum);
    if(root->right != NULL)
	    hasPathSum_r(root->right, csum + root->val, sum, is_sum);
	return;
}
bool hasPathSum(struct TreeNode* root, int sum){
	bool is_sum = false;
    
    if(root == NULL)
     	return false;
    
    if(root->right == NULL && root->left == NULL)
        return sum == root->val;
    
	if(root->left != NULL)
		hasPathSum_r(root->left, root->val, sum, &is_sum);
	if(root->right != NULL)
		hasPathSum_r(root->right, root->val, sum, &is_sum);
    
	return is_sum;
}
