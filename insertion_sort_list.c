/* Link:
https://leetcode.com/problems/insertion-sort-list/
*/

/* Problem:
Sort a linked list using insertion sort.


A graphical example of insertion sort. The partial sorted list (black) initially contains only the first element in the list.
With each iteration one element (red) is removed from the input data and inserted in-place into the sorted list
 

Algorithm of Insertion Sort:

    Insertion sort iterates, consuming one input element each repetition, and growing a sorted output list.
    At each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list, and inserts it there.
    It repeats until no input elements remain.


Example 1:

Input: 4->2->1->3
Output: 1->2->3->4

Example 2:

Input: -1->5->3->4->0
Output: -1->0->3->4->5


*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>


struct ListNode {
    int val;
    struct ListNode *next;
};
struct ListNode * create_node(int val)
{
   struct ListNode *p = malloc(sizeof(struct ListNode));
   p->next = NULL;
   p->val = val;
   return p;
}

void append_node(struct ListNode *p, int a)
{
   while(p->next) {
      p = p->next;
   }
   p->next = create_node(a);

}

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d ->", p->val);
      p = p->next;
   }
   printf("\n");

}

/* Solution: */
struct ListNode* insertionSortList(struct ListNode* head){
   struct ListNode dummy = {-1, NULL};
   struct ListNode *cur, *next;
   while(head) {
      cur = &dummy;
      next = head->next;
      while(cur->next && cur->next->val < head->val) {
         cur = cur->next;
      }
      head->next = cur->next;
      cur->next = head;
      head = next;
   }
   return dummy.next;
}

/* Test Driver: */

int main()
{
   struct ListNode *a;
   a = create_node(1);
   append_node(a, 4);
   append_node(a, 2);
   append_node(a, 5);
   /* append_node(a, 1); */
   /* append_node(a, 6); */
   /* append_node(a, 7); */


   print_list(a);
   
   print_list(insertionSortList(a));

   return 0;
}
