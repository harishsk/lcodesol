/* Link:
 https://leetcode.com/problems/valid-mountain-array/
*/

/* Problem:
Given an array of integers arr, return true if and only if it is a valid mountain array.

Recall that arr is a mountain array if and only if:

arr.length >= 3
There exists some i with 0 < i < arr.length - 1 such that:
arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
arr[i] > arr[i + 1] > ... > arr[arr.length - 1]

Example 1:

Input: arr = [2,1]
Output: false
Example 2:

Input: arr = [3,5,5]
Output: false
Example 3:

Input: arr = [0,3,2,1]
Output: true
 

Constraints:

1 <= arr.length <= 104
0 <= arr[i] <= 104
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
typedef enum { UP, DOWN} dir_t;
bool validMountainArray(int* arr, int arrSize){

   if(arrSize < 3)
      return false;
   
   dir_t d = UP;
   for(int i = 0; i < arrSize - 1; i++) {
      if(d == UP) {
         /* Climbing the mountain */
         if(arr[i + 1] == arr[i]) {
            /* If value is equal then not stictly climbing*/
            return false;
         }else if(arr[i + 1] < arr[i]){
            /* Can decend the mountain after atleast 1 element*/
            if(i > 0)
               d = DOWN;
            else
               return false;
         }
      }else {
         /* Decending the mountain */
         if(arr[i + 1] == arr[i]) {
            /* If value is equal then not stictly decending*/
            return false;
         }else if(arr[i + 1] > arr[i]){
            /* If value increases then cannot be decending*/
            return false;
         }
      }
   }
   /* if we have not decended then it means there was no mountain*/
   if(d == UP)
      return false;
   return true;
}

/* Test Driver: */
#if defined(TEST)
int main() {

   int a[] = {2,9,8,7,6,5,4,3,2,1,0};
   printf("Valid %d\n", validMountainArray(a, sizeof(a)/sizeof(a[0])));
   return 0;
}
#endif
