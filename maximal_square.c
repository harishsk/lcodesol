/* Link: https://leetcode.com/problems/maximal-square/
*/

/* Problem:
Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

Example:

Input: 

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0

Output: 4

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

/* Solution: */
//#define BRUTE_FORCE
#define DYNAMIC_PROGRAMMING
#if defined(BRUTE_FORCE)
void expand(char** matrix, int row, int col, int rowMax, int colMax, int** dp){
   if(matrix[row][col] != '1')
      return;
   int rowi = row + 1, coli = col + 1;
   while(rowi < rowMax && coli < colMax) {
      bool is_square = 1;
      if(matrix[rowi][coli] == '1') {
         for(int rowj = row; rowj < rowi; rowj++){
            if(matrix[rowj][coli] != '1'){
               is_square = 0;
               break;
            }
         }
         for(int colj = col; colj < coli; colj++){
            if(matrix[rowi][colj] != '1'){
               is_square = 0;
               break;
            }
         }
      }else {
         break;
      }
      if(is_square == 0) {
         break;
      }else {
         dp[row][col] = rowi - row + 1;
         rowi++;
         coli++;
      }
   }
}
#define MAX(a,b) ((a) > (b) ? (a) : (b))
int maximalSquare(char** matrix, int matrixSize, int* matrixColSize){
   if(matrix == NULL || matrixSize < 1 || *matrixColSize < 1)
      return 0;
   int rowMax = matrixSize, colMax = *matrixColSize;

   int **dp, max = 0;
   dp = malloc(sizeof(int*) * rowMax);
   for(int row = 0; row < rowMax; row++) {
      dp[row] = malloc(sizeof(int) * colMax);
   }
   
   for(int row = 0; row < rowMax; row++) {
      for(int col = 0; col < colMax; col++) {          
         if(matrix[row][col] == '1') {
            dp[row][col] = 1; 
            expand(matrix, row, col, rowMax, colMax, dp);
         }else {
            dp[row][col] = 0; 
         }
         max = MAX(dp[row][col], max);
      }
   }
   return max*max;
}
#else /* DYNAMIC_PROGRAMMING */
#define MIN_TWO(a,b) ((a) < (b) ? (a) : (b))
#define MIN(a, b, c) (MIN_TWO(a,MIN_TWO(b,c)))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

int maximalSquare(char** matrix, int matrixSize, int* matrixColSize){
   if(matrix == NULL || matrixSize < 1 || *matrixColSize < 1)
      return 0;
   int rowMax = matrixSize, colMax = *matrixColSize;

   int **dp, max = 0;
   dp = malloc(sizeof(int*) * rowMax);
   for(int row = 0; row < rowMax; row++) {
      dp[row] = malloc(sizeof(int) * colMax);
   }   
   
   for(int row = 0; row < rowMax; row++)
      dp[row][0] = matrix[row][0] -'0';

   for(int col = 0; col < colMax; col++)   
      dp[0][col] = matrix[0][col] -'0';
   
    for(int row = 1; row < rowMax; row++){
        for(int col = 1; col < colMax; col++){
            if(matrix[row][col]=='1')
                dp[row][col] = MIN(dp[row-1][col], dp[row][col-1], dp[row-1][col-1])+1;
            else
                dp[row][col] = 0;
        }
    }
    for(int row = 1; row < rowMax; row++){
       for(int col = 1; col < colMax; col++){
          max = MAX(max, dp[row][col]);
       }
    }
    return max * max;
}
#endif

/* Test Driver: */
int main() {
   char a0[] = {'1', '0', '1', '0', '0'};
   char a1[] = {'1', '0', '1', '1', '1'};
   char a2[] = {'1', '1', '1', '1', '1'};
   char a3[] = {'1', '0', '0', '1', '0'};

   char *a[] = {a0, a1, a2, a3};
   int colS = sizeof(a0) / sizeof(a0[0]);

   printf("Maximum Sqaure is %d \n", maximalSquare(a, sizeof(a)/sizeof(a[0]), &colS));
   
   return  0;
}
