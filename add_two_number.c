/*
https://leetcode.com/problems/add-two-numbers/
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
 */

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

void print_list(struct ListNode *p)
{
   while(p) {
      printf("%d -> ", p->val);
      p = p->next;
   }
   printf("NULL\n");

}

#if defined(FIRST_TRY)
#define CREATE_NODE(x, v) \
   x = (struct ListNode *)malloc(16); \
   x->val = v; \
   x->next = NULL;

#define LINK_NODE(h, t , p) \
   if(h == NULL) { \
      h = t = p; \
   }else { \
      t->next = p; \
      t = t->next; \
   } 
   

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2)
{
   int s = 0, r = 0;
   int d = 1;
   
   struct ListNode *h = NULL;
   struct ListNode *t = NULL;   
   struct ListNode *p = NULL;
   struct ListNode *tt = NULL;   
   
   while(l1 && l2) {
      s = s +  l1->val + l2->val;
      r = s % 10;
      s = s / 10;

      l1 = l1->next;
      l2 = l2->next;

      CREATE_NODE(p, r);

      LINK_NODE(h, t, p);
      
   }
   
   tt = (l1 != NULL) ? l1 : l2;
   while(tt) {
      s = s +  tt->val;
      r = s % 10;
      s = s / 10;

      tt = tt->next;

      CREATE_NODE(p, r);      

      LINK_NODE(h, t, p);
   }
   if(s) {
      CREATE_NODE(p, s);      
      LINK_NODE(h, t, p);
   }
   return (h);
}
#elif REFINED 
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2){
    /* sum is dummy header, where sum.next will point to sum link list */
    struct ListNode sum, *cur;
    
    sum.next = NULL;
    cur = &sum;
    
	/* r will hold remainder */
    int r = 0;
	/* Add numbers of l1, l2 and r, store result in l1 node*/
    while(l1 != NULL && l2 != NULL) {
        cur->next = l1;
        int s = l1->val + l2->val + r;
        l1->val = s % 10;
        r = s / 10;
        l1 = l1->next;
        l2 = l2->next;
        cur = cur->next;
    }
	/* Add numbers of l1 and r, store result in l1 node*/
    while(l1 != NULL) {
        cur->next = l1;
        int s = l1->val + r;
        l1->val = s % 10;
        r = s / 10;
        l1 = l1->next;
        cur = cur->next;
    }
	/* Add numbers of l2 and r, store result in l2 node*/
    while(l2 != NULL) {
        cur->next = l2;
        int s = l2->val + r;
        l2->val = s % 10;
        r = s / 10;
        l2 = l2->next;
        cur = cur->next;
    }
	/* If any remainder is left, then append it sum list */
    if(r > 0){
        cur->next = malloc(sizeof(struct ListNode));
        cur->next->val = r;
        cur->next->next = NULL;
    }
    return sum.next;
}
#else /* STILL_REFINED */
int get_len(struct ListNode* l) {
	if(l == NULL) return 0;
	
	int len = 0;
	while(l != NULL) {
		len++;
		l = l->next;
	}
	return len;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2){
	if( l1 == NULL || l2 == NULL) return NULL;

	/* Find length of l1 */
	int l1_len = get_len(l1);

	/* Find length of l2 */
	int l2_len = get_len(l2);
	
	/* result is stored in longer link list */
	struct ListNode *res = (l1_len > l2_len) ? l1 : l2;
	struct ListNode *oth = (l1_len <= l2_len) ? l1 : l2;
	struct ListNode *tmp = res, *last = NULL;

	/* Traverse link list adding nodes */
	int r = 0;
	while(tmp != NULL) {
		if(oth != NULL) {
			r = r + tmp->val + oth->val;
			oth = oth->next;
		}else {
			r = r + tmp->val;
		}
		tmp->val = r % 10;
		r = r / 10;
      last = tmp;
		tmp = tmp->next;
	}

	/* Add result if anything pending */
	if( r != 0) {
		struct ListNode *t = malloc(sizeof(struct ListNode));
		t->val = r;
		t->next = NULL;
		last->next = t;
	}
	/* Return res */
	return res;
}

#endif

int main()
{
   struct ListNode a, b, c;
   struct ListNode x, y, z;
   
   struct ListNode* m = NULL;
   
   
   a.val = 2;
   a.next = &b;

   b.val = 4;
   b.next = &c;

   c.val = 3;
   c.next = NULL;

   x.val = 5;
   x.next = &y;

   y.val = 6;
   y.next = &z;

   z.val = 4;
   z.next = NULL;   

   printf("A list is : ");
   print_list(&a);
   
   printf("X list is : ");
   print_list(&x);
   
   
   m = addTwoNumbers(&a, &x);
   
   printf("Added number is: ");
   print_list(m);
   
}
