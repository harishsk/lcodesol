int minSubArrayLen(int s, int* nums, int numsSize) {
   int left=0, right=0;
   int sum=0, min_length = numsSize+1;
   
   while(right < numsSize){ 
      sum += nums[right++];
      
      while(sum >= s){
         if(right - left < min_length) 
            min_length =  right - left;
         sum -= nums[left++];
      }
   }
   if(min_length == numsSize + 1) {
      return 0;// Not found
   }else {
      return min_length;
   }
}
