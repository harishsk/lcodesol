#if defined(DP_ARRAY)
#define MAX(a,b) ((a) > (b) ? (a): (b));
int rob(int* nums, int numsSize){
    printf("%p, %d\n", nums, numsSize);
    if(nums == NULL || numsSize < 1)
        return 0;
    
    int dp[numsSize], max = INT_MIN;
    
    dp[0] = nums[0];
    max = MAX(max, dp[0]);
    if(numsSize == 1) return max;
    
    dp[1] = nums[1];
    max = MAX(max, dp[1]);
    if(numsSize == 2) return max;

    for(int i = 2; i < numsSize; i++) {
        
       dp[i] = MAX(nums[i], nums[i-1]);
       max = MAX(max, dp[i]);
    }
    return max;
}
#else // Reduce memory
#define MAX(a,b) ((a) > (b) ? (a): (b));
int rob(int* nums, int numsSize){    
    /* No house condition, there is nothing to rob */
    if(nums == NULL || numsSize < 1)
        return 0;
    
    int dp[3]; // Save only last 3 house in history
    
    /* One house , he will rob it */
    dp[0] = nums[0];    
    if(numsSize == 1) return dp[0];

    /* Two house available, he will rob maximum of it */
    dp[1] = MAX(nums[0], nums[1]);    
    if(numsSize == 2) return dp[1];
 
    for(int i = 2; i < numsSize; i++) {        
        dp[2] = MAX(nums[i] + dp[0], dp[1]);
        dp[0] = dp[1];
        dp[1] = dp[2];
    }
    return dp[2];
}

#endif
