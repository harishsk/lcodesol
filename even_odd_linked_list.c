/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

#if defined(USING_ODD_EVEN_HEAD_POINTER)
bool is_odd(int n) {
    return n % 2 == 1;
}
struct ListNode* oddEvenList(struct ListNode* head){
    if(head == NULL || head->next == NULL)
        return head;
    
    struct ListNode *odd = head;
    struct ListNode *even_head = head->next;
    struct ListNode *even = even_head;
    struct ListNode *curr = even->next;
    
    int cnt = 1;
    while(curr) {
        if(is_odd(cnt)) {
            odd->next = curr;
            odd = odd->next;
        }else {
            even->next = curr;
            even = even->next;
        }
        cnt++;
        curr = curr->next;
    }
    even->next = NULL;
    odd->next = even_head;
    
    return head;
}
#else
struct ListNode* oddEvenList(struct ListNode* head){
	struct ListNode odd_head, even_head;
	struct ListNode *odd = &odd_head, *even = &even_head;

	odd_head.next = NULL;
	even_head.next = NULL;

	int count = 1;
	while(head != NULL) {
		if(count % 2 == 0) {
			even->next = head;
			even = even->next;
		}else {
			odd->next = head;
			odd = odd->next;
		}
		count++;
		head = head->next;
	}
	odd->next = even_head.next;
	even->next = NULL;
	return odd_head.next;
}

#endif
 
