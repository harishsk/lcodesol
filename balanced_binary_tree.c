#if defined(SLOWER)
int maxDepth(struct TreeNode* root){
	int ldept = 1, rdept = 1;
	if(root == NULL) {
		return 0;
	}
	if(root->left)
		ldept = maxDepth(root->left) + 1;
	if(root->right)
		rdept = maxDepth(root->right) + 1;
	
	return ldept > rdept ? ldept : rdept;
}
#define ABS(x) ((x) > 0 ? (x) : -(x))
bool isBalanced(struct TreeNode* root){
	int ldept, rdept;
	bool lbal, rbal, cbal;
	
	if(root == NULL)
		return true;
	
	ldept = maxDepth(root->left);
	rdept = maxDepth(root->right);
	
	
	lbal = isBalanced(root->left);
	rbal = isBalanced(root->right);
	
	if(ABS(rdept - ldept) < 2)
		cbal = true;
	else
		cbal = false;
	
	return cbal && lbal && rbal;
}
#else
int isBalanced_r(struct TreeNode* root, bool *is_bal){
	int ldept = 1, rdept = 1;
	
	if(root == NULL || *is_bal == false)
		return 0;
	 
	ldept = isBalanced_r(root->left, is_bal) + 1;
	rdept = isBalanced_r(root->right, is_bal) + 1;

	if(abs(rdept - ldept) > 1) {
		*is_bal = false;
		return 0;
	}
	
	return ldept > rdept ? ldept : rdept;
}

bool isBalanced(struct TreeNode* root){
	bool is_bal = true;
	isBalanced_r(root, &is_bal);
	return is_bal;
}
#endif
