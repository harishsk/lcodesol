
#if defined(OLD_IMPLEMENTATION)
struct intstack {
    int val;
    struct intstack *next;
}intstack_t;


typedef struct {
    struct intstack *st1;
    struct intstack *st2;
    
} MyQueue;

/** Initialize your data structure here. */

MyQueue* myQueueCreate() {
    MyQueue *q = malloc(sizeof(MyQueue));
    q->st1 = NULL;
    q->st2 = NULL;
    return q;
}

/** Push element x to the back of queue. */
void myQueuePush(MyQueue* obj, int x) {
    struct intstack *el = NULL;
    el = malloc(sizeof(struct intstack));
    el->val = x;
    STACK_PUSH(obj->st1, el);
    return;
    
}

/** Removes the element from in front of queue and returns that element. */
int myQueuePop(MyQueue* obj) {
    struct intstack *el = NULL;
    int val;
    while(!STACK_EMPTY(obj->st1)) {
        STACK_POP(obj->st1, el);
        STACK_PUSH(obj->st2, el);
    }
    STACK_POP(obj->st2, el);
    val = el->val;
    free(el);
    while(!STACK_EMPTY(obj->st2)) {
        STACK_POP(obj->st2, el);
        STACK_PUSH(obj->st1, el);
    }
    return val;
}

/** Get the front element. */
int myQueuePeek(MyQueue* obj) {
    struct intstack *el = NULL;
    int val;
    while(!STACK_EMPTY(obj->st1)) {
        STACK_POP(obj->st1, el);
        STACK_PUSH(obj->st2, el);
    }
    val = STACK_TOP(obj->st2)->val;
    while(!STACK_EMPTY(obj->st2)) {
        STACK_POP(obj->st2, el);
        STACK_PUSH(obj->st1, el);
    }
    return val;
}

/** Returns whether the queue is empty. */
bool myQueueEmpty(MyQueue* obj) {
  return STACK_EMPTY(obj->st1);
}

void myQueueFree(MyQueue* obj) {
    struct intstack *el = NULL;
    while(!STACK_EMPTY(obj->st1)) {
        STACK_POP(obj->st1, el);
        free(el);
    }
    free(obj);
    return;
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);
 
 * int param_2 = myQueuePop(obj);
 
 * int param_3 = myQueuePeek(obj);
 
 * bool param_4 = myQueueEmpty(obj);
 
 * myQueueFree(obj);
*/

#else /* USING_PUSH_POP_STACK */

#include <utstack.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct intstack {
    int val;
    struct intstack *next;
}intstack_t;


typedef struct {
    intstack_t *st1;
    intstack_t *st2;
} MyQueue;

void stack_push(intstack_t *st, int val){
   struct intstack *el = NULL;
   el = malloc(sizeof(intstack_t));
   el->val = val;
   STACK_PUSH(st, el);
   return;
}
int stack_pop(intstack_t *st){
   struct intstack *el = NULL;
   int val;
   STACK_POP(st, el);
   val = el->val;
   free(el);
   return val;
}
int stack_peek(intstack_t *st){
   return STACK_TOP(st)->val;
}
void stack_free(intstack_t *st){
   struct intstack *el = NULL;   
   while(!STACK_EMPTY(st)) {
      STACK_POP(st, el);
      free(el);
   }
}
bool stack_empty(intstack_t *st){
   return STACK_TOP(st) == NULL;
}
MyQueue* myQueueCreate() {
    MyQueue *q = malloc(sizeof(MyQueue));
    q->st1 = NULL;
    q->st2 = NULL;
    return q;
}


void myQueuePush(MyQueue* obj, int x) {
	stack_push(obj->st1, x);
}

int myQueuePop(MyQueue* obj) {
	if(!stack_empty(obj->st2)) {
		return stack_pop(obj->st2);
	}else {
		while(!stack_empty(obj->st1)){
			int v = stack_pop(obj->st1);
			stack_push(obj->st2, v);
		}
		return stack_pop(obj->st2);
	}
}

int myQueuePeek(MyQueue* obj) {
	if(!stack_empty(obj->st2)) {
		return stack_peek(obj->st2);
	}else {
		while(!stack_empty(obj->st1)){
			int v = stack_pop(obj->st1);
			stack_push(obj->st2, v);
		}
		return stack_peek(obj->st2);
	}  
}

bool myQueueEmpty(MyQueue* obj) {
  	return stack_empty(obj->st1) && stack_empty(obj->st2);
}

void myQueueFree(MyQueue* obj) {
	stack_free(obj->st1);
	stack_free(obj->st2);
	free(obj);
}
#endif

int main() {
   return 0;
}
