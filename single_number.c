#include <stdio.h>

int singleNumber(int* nums, int numsSize)
{
   int unq = 0, i;

   for(i = 0; i < numsSize; i++)
      unq = unq ^ nums[i];

   return unq;
}

int main()
{
   int a[] = {2, 2, 3, 4, 3, 4, 10};

   printf("Unique number is %d\n", singleNumber(a,  sizeof(a) / sizeof(a[0])));

   return 0;
}
