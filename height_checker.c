/* Link:
   https://leetcode.com/problems/height-checker/   
*/

/* Problem:
Students are asked to stand in non-decreasing order of heights for an annual photo.

Return the minimum number of students that must move in order for all students to be standing in non-decreasing order of height.

Notice that when a group of students is selected they can reorder in any possible way between themselves and the non selected students remain on their seats.

 

Example 1:

Input: heights = [1,1,4,2,1,3]
Output: 3
Explanation: 
Current array : [1,1,4,2,1,3]
Target array  : [1,1,1,2,3,4]
On index 2 (0-based) we have 4 vs 1 so we have to move this student.
On index 4 (0-based) we have 1 vs 3 so we have to move this student.
On index 5 (0-based) we have 3 vs 4 so we have to move this student.
Example 2:

Input: heights = [5,1,2,3,4]
Output: 5
Example 3:

Input: heights = [1,2,3,4,5]
Output: 0
 

Constraints:

1 <= heights.length <= 100
1 <= heights[i] <= 100  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
int cmp(const void *a, const void *b) {
   long la = *(int*)a;
   long lb = *(int*)b;   

   return (la - lb);
}
int heightChecker(int* heights, int heightsSize){
   if(heightsSize < 2) 
      return 0;
   
   int *ch = malloc(sizeof(int) * heightsSize);
   memcpy(ch, heights, sizeof(int) * heightsSize);

   qsort(ch, heightsSize, sizeof(int), cmp);

   int difference = 0;
   for(int i = 0; i < heightsSize; i++) 
      if(heights[i] != ch[i]) 
         difference++;

   return difference;
   
}
/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = {1,1,4,2,1,3};
   printf("Difference is %d\n", heightChecker(a, sizeof(a)/sizeof(a[0])));
   return 0;
}
#endif
