/* Link:
   https://leetcode.com/problems/trim-a-binary-search-tree/
*/

/* Problem:
Given the root of a binary search tree and the lowest and highest boundaries as low and high, trim the tree so that all its elements lies in [low, high]. Trimming the tree should not change the relative structure of the elements that will remain in the tree (i.e., any node's descendant should remain a descendant). It can be proven that there is a unique answer.

Return the root of the trimmed binary search tree. Note that the root may change depending on the given bounds.

 

Example 1:

Input: root = [1,0,2], low = 1, high = 2
Output: [1,null,2]

Example 2:

Input: root = [3,0,4,null,2,null,null,1], low = 1, high = 3
Output: [3,2,null,1]

Example 3:

Input: root = [1], low = 1, high = 2
Output: [1]

Example 4:

Input: root = [1,null,2], low = 1, high = 3
Output: [1,null,2]

Example 5:

Input: root = [1,null,2], low = 2, high = 4
Output: [2]

    The number of nodes in the tree in the range [1, 104].
    0 <= Node.val <= 104
    The value of each node in the tree is unique.
    root is guaranteed to be a valid binary search tree.
    0 <= low <= high <= 104

  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */
/**
 * Definition for a binary tree node.
 */
#if defined(TEST)
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};
#endif

#if defined(VERSION_1)
struct TreeNode* trimBST_r(struct TreeNode* parent, int dir, struct TreeNode* root, int low, int high){

   if(root == NULL)
      return NULL;
    
   if(root->val == low) {
      /* All nodes to left of root should be less than low,
         hence can be chopped off */
      root->left = NULL;
   }else if(root->val < low) {
      /* All nodes to left of root should be less than low,
         hence can be chopped off */
      root->left = NULL;
      /* Since here root node also has to chopped off */
      root = trimBST_r(root, 0, root->right, low, high);
      /* Rebasing the root branch */
      if(parent != NULL) {
         if(dir == 0)
            parent->right = root;
         else
            parent->left = root;
      }
   }else {
      /* Visit the left if there is any chopping required */
      trimBST_r(root, 1, root->left, low, high);
   }
   /* If root is removed with above logic, just return NULL */
   if(root == NULL)
      return NULL;
   
   if(root->val == high) {
      /* All nodes to right of root should be greater than high,
         hence can be chopped off */      
      root->right = NULL;
   }else if(root->val > high) {
      /* All nodes to right of root should be greater than high,
         hence can be chopped off */      
      root->right = NULL;
      /* Since here root node also has to chopped off */
      root = trimBST_r(root, 1, root->left, low, high);
      /* Rebasing the root branch */
      if(parent != NULL) {
         if(dir == 0) {
            parent->right = root;
         }else {
            parent->left = root;            
         }
      }      
   }else {
      /* Visit the right if there is any chopping required */      
       trimBST_r(root, 0, root->right, low, high);
   }
   return root;

}
struct TreeNode* trimBST(struct TreeNode* root, int low, int high){
   return trimBST_r(NULL, 0, root, low, high);
}
#else
struct TreeNode* trimBST(struct TreeNode* root, int low, int high) {
   if(root){      
      if(root->val < low)
         return trimBST(root->right, low, high);
      
      if(root->val > high)
         return trimBST(root->left, low, high);
      
      root->left = trimBST(root->left, low, high);
      root->right = trimBST(root->right, low, high);
   }
   return root;
}
#endif

/* Test Driver: */
#if defined(TEST)
int main() {

   struct TreeNode a1, a2, a3, a4, a5, a6, a7;

   a1.val = 1;
   a2.val = 2;
   a3.val = 3;
   a4.val = 4;
   a5.val = 5;
   a6.val = 6;
   a7.val = 7;      

   a4.left = &a2;
   a4.right = &a6;

   a2.left = &a1;
   a2.right = &a3;

   a6.left = &a5;
   a6.right = &a7;

   a1.left = NULL;
   a1.right = NULL;

   a3.left = NULL;
   a3.right = NULL;

   a5.left = NULL;
   a5.right = NULL;   
   
   a7.left = NULL;
   a7.right = NULL;   

   struct TreeNode *root = &a4;
   
   root = trimBST(root, 2, 4);

   return 0;
}
#endif
