#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
};

#if defined(OLD_IMPLEMENTATION)
/* QUEUE IMPLEMENTATION	 START*/
typedef struct queue_node_tag {
	void *p;
	struct queue_node_tag *next;
}queue_node_t;

typedef struct queue_tag {
	queue_node_t *head;
	queue_node_t *tail;
	int count;
}queue_t;

static inline queue_t * init_queue()
{
	queue_t *q = NULL;
	q = malloc(sizeof(queue_t));
	q->head = NULL;
	q->tail = NULL;
	q->count = 0;
	return q;
}
static inline void cleanup_queue(queue_t *q)
{
	free(q);
}

static inline void en_queue(queue_t *q, void *p)
{
	queue_node_t *t = malloc(sizeof(queue_node_t));
	
	t->p = p;
	t->next = NULL;
	
	if(q->head == NULL) 
		q->head = t;
	
	if(q->tail == NULL){
		q->tail = t;
	}else {
		q->tail->next = t;
		q->tail = t;		
	}
	q->count++;
}
static inline void* de_queue(queue_t *q)
{
	void *ret;
	queue_node_t *t = q->head;

	if(q->head == NULL)
		return NULL;
	
	ret = q->head->p;
	q->head = q->head->next;
	if(q->head == NULL)
		q->tail = NULL;
	
	free(t);
	q->count--;
	
	return ret;
}

static inline int count_queue(queue_t *q) {
	return q->count;
}
static inline bool is_empty_queue(queue_t *q) {
	return q->count == 0;
}
void add_front(int ***ret, int *rS, int **rCS, int level, int val) {
	if(level > *rS) {
		*ret = realloc(*ret, sizeof(int*) * (*rS + 1));
		memmove(*ret + 1, *ret, sizeof(int*) * (*rS));
		(*ret)[0] = NULL;
		*rCS = realloc(*rCS, sizeof(int) * (*rS + 1));
		memmove(*rCS + 1, *rCS, sizeof(int) * (*rS));
		(*rCS)[0] = 0;
		(*ret)[0] = realloc((*ret)[0], sizeof(int) * ((*rCS)[0] + 1));
		(*ret)[0][(*rCS)[0]] = val;
		(*rCS)[0] += 1;
		*rS += 1;
	}else {
		int row = 0;
		(*ret)[row] = realloc((*ret)[row], sizeof(int) * ((*rCS)[row] + 1));
		(*ret)[row][(*rCS)[row]] = val;
		(*rCS)[row] += 1;
	}
	return;
}
void levelOrderBottom_r(struct TreeNode* root, int ***ret, 
								int *rS, int **rCS) {
	
	if(root == NULL)
		return;
	
	queue_t *q = init_queue();
	
	en_queue(q, root);
	
	int level = 1;
	while(!is_empty_queue(q)) {
		int cnt = count_queue(q);
		for(int i = 0; i < cnt; i++) {
			struct TreeNode* node = de_queue(q);
			if(node->left)
				en_queue(q, node->left);				  
			if(node->right)
				en_queue(q, node->right);
			add_front(ret, rS, rCS, level, node->val);
		}
		level++;
	}
	cleanup_queue(q);
}
int** levelOrderBottom(struct TreeNode* root, int* returnSize,
							  int** returnColumnSizes){
	int **ret = NULL;
	*returnSize = 0;
	*returnColumnSizes = NULL;
	levelOrderBottom_r(root, &ret, returnSize, returnColumnSizes);
	return ret;
}
#else /* NEW_IMPLEMENTAION */

typedef struct node_tag {
	void *p;
	struct node_tag *next;
}node_t;

typedef struct queue_tag {
	node_t *head;
	node_t *tail;
	int len;
}queue_t;

static inline node_t* node_new(void) {
	node_t *t = malloc(sizeof(node_t));
	t->next = NULL;
	t->p = NULL;
	return t;
}
static inline queue_t* queue_new(void) {
	queue_t *q = malloc(sizeof(queue_t));
	q->head = NULL;
	q->tail = NULL;
	q->len = 0;
	return q;
}

static inline void print_queue(queue_t *q) {

   node_t *n = q->head;
   printf("\n");   
   while(n) {
      printf("%d->", ((struct TreeNode*)n->p)->val);
      n = n->next;
   }
   printf("\n");
}

static inline void enqueue(queue_t *q, void *p)
{
	node_t *t = node_new();
	t->p = p; 
	if(q->head == NULL) 
		q->head = t;   
	if(q->tail != NULL) {
		q->tail->next = t;
   }
	q->tail = t;
	q->len++;
   print_queue(q);
}

static inline void* dequeue(queue_t *q)
{
	void* ret;
	node_t *t = q->head;
	if(t == NULL)
 		return NULL;
   print_queue(q);   
	q->head = t->next;
	ret = t->p;
	free(t);
	q->len--;
   print_queue(q);
   
	return ret;
}

static inline bool queue_empty(queue_t *q){
	return q->len == 0;
}
static inline int queue_len(queue_t *q){
	return q->len;
}
void init_res(int ***r, int **rc, int level){
	*rc = realloc(*rc, sizeof(int*) * (level + 1));
	*r  = realloc(*r, sizeof(int**) * (level + 1));
	rc[level] = malloc(sizeof(int));
	(*rc)[level] = 0;
   (*r)[level] = NULL;
}
void update_res(int ***r, int **rc, int level, int val) {
	int level_len = (*rc)[level];
   //printf("\n - %d %d %d -\n", level, level_len, val);
	(*rc)[level] = ++level_len;
	(*r)[level] = realloc((*r)[level], sizeof(int) * level_len);
	(*r)[level][level_len - 1] = val;
   //printf("* %d *\n", (*r)[level][level_len - 1]);
}
int** levelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes){
	if(root == NULL) {
		return NULL;
	}
	
	int **res = NULL;
   
	/* Initialize a queue */
	queue_t *q = queue_new();

   *returnColumnSizes = NULL;
	enqueue(q, root);

	int level = 0;
	while(!queue_empty(q)) {
		int len = queue_len(q);
      init_res(&res, returnColumnSizes, level);
		for(int i = 0; i < len; i++) {
			struct TreeNode *n = dequeue(q);
			update_res(&res, returnColumnSizes, level, n->val);
         printf("%d ", n->val);
			if(n->left) {
            printf(" L: %d", n->left->val);
            enqueue(q, n->left);
         }
			if(n->right) {
            printf(" R: %d ", n->right->val);         
            enqueue(q, n->right);
         }
		}
      level++;
      printf("\n");
	}
   *returnSize = level;
	return res; 
}


#endif
int main()
{
	struct TreeNode t1, t2, t3, t4, t5, t6, t7;
	t1.val = 1;
	t2.val = 2;
	t3.val = 3;
	t4.val = 4;
	t5.val = 5;
	t6.val = 6;
	t7.val = 7;

	t1.left = &t2;
	t1.right = &t3;

	t2.left = &t4;
	t2.right = &t5;

	t3.left = &t6;
	t3.right = &t7;

	t4.left = t4.right = NULL;
	t5.left = t5.right = NULL;
	t6.left = t6.right = NULL;
	t7.left = t7.right = NULL;

	int S, *CS;
	int **p = levelOrder(&t1, &S, &CS);

	printf("Size is %d, %p\n", S, CS);
	
	for(int i = 0; i < S; i++) {
      printf("%d: ", CS[i]);
		for(int c = 0; c < CS[i]; c++) {
			printf("%d ", p[i][c]);
		}
		printf("\n");
		free(p[i]);
	}
	free(p);
	free(CS);

	return 0;

		
}
