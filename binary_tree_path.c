struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int createpath(char ***arr, char *prefix, int* returnSize) {
	*arr = realloc(*arr, sizeof(char*) * (*returnSize + 1));
   (*arr)[*returnSize] = malloc(strlen(prefix) + 1);
   strcpy((*arr)[*returnSize], prefix);
	*returnSize = *returnSize + 1;
	return *returnSize - 1;
}
void addpath(char ***arr, int pno, int val) {
	char temp[100];
	int len, len_s;
	memset(temp, '\0', 100);
	if(strlen((*arr)[pno]) == 0) {
		len = snprintf(temp, 100, "%d", val);
	}else {
		len = snprintf(temp, 100, "->%d", val);
	}
	temp[len] = '\0';
   len = strlen((*arr)[pno]) + len;
	(*arr)[pno] = realloc((*arr)[pno], len + 1);
	strcat((*arr)[pno], temp);
   (*arr)[pno][len] = '\0';
}
void binaryTreePaths_r(struct TreeNode* root, int* returnSize, int pno, char ***arr) {
	if(root->left == NULL && root->right == NULL) {
		addpath(arr, pno, root->val);
	} else if(root->left == NULL && root->right != NULL) {
		addpath(arr, pno, root->val);
		binaryTreePaths_r(root->right, returnSize, pno, arr);
	} else if(root->right == NULL && root->left != NULL) {
		addpath(arr, pno, root->val);
		binaryTreePaths_r(root->left, returnSize, pno, arr);
	} else if(root->left != NULL && root->right != NULL) {
		addpath(arr, pno, root->val);
		int right_no = createpath(arr, (*arr)[pno], returnSize);
		binaryTreePaths_r(root->left, returnSize, pno, arr);
		binaryTreePaths_r(root->right, returnSize, right_no, arr);
	}
	return;
}
char ** binaryTreePaths(struct TreeNode* root, int* returnSize){
	char **ret = malloc(sizeof(char*));
	*ret = malloc(sizeof(char));
	(*ret)[0] = '\0';
   *returnSize = 0;
	if(root == NULL) 
		return ret;

	*returnSize = 1;
	binaryTreePaths_r(root, returnSize , 0, &ret);
	return ret;
}

int main()
{
   struct TreeNode t1, t2, t3, t4, t5, t6, t7;
   t1.val = 1;
   t2.val = 2;
   t3.val = 3;
   t4.val = 4;
   t5.val = 5;
   t6.val = 6;
   t7.val = 7;

   t1.left = &t2;
   t1.right = &t3;   
   
   t2.left = &t4;
   t2.right = &t5;

   t3.left = &t6;
   t3.right = &t7;


   t4.left = t4.right = NULL;
   t5.left = t5.right = NULL;
   t6.left = t6.right = NULL;
   t7.left = t7.right = NULL;

   int retSize;
   char **p;
   p = binaryTreePaths(&t1, &retSize);

   printf("DONE\n");
   
   for(int i = 0; i < retSize; i++) {
      printf("%s\n", p[i]);
      free(p[i]);
   }
   free(p);

   return 0;

      
}



