#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
public:
   vector<int> twoSum(vector<int>& nums, int target) {
      unordered_map <int, int>m;
      vector <int> ret(0);
      for(int i = 0; i < nums.size(); i++) {
         int x = nums[i];
         int y = target - nums[i];
         if(m.find(y) != m.end()) {
            ret.push_back(m[y]);
            ret.push_back(i);
            return ret;
         }
         m[x] = i;
      }
      return ret;
   }
};

int main()
{
   Solution s;
   vector <int> x{2,7,11,15};
   int target = 9;
   vector <int>ret(0);

   ret = s.twoSum(x, target);

   if(ret.size() == 2) {
      cout << "Indices are " << ret[0] << " and " << ret[1] <<endl;
   }else{
      cout << "No indices found "<< endl;
   }
   return 0;
}
