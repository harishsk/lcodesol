#include <stdio.h>
#include <limits.h>

struct TreeNode {
   int val;
   struct TreeNode *left;
   struct TreeNode *right;
};

#define MIN(x,y) ((x) < (y) ? (x) : (y))
#if defined(ORG_HSK)
void minDepth_r(struct TreeNode*root, int height, int *min_depth) {

   if(root == NULL)
      return;
   if(height +1 > *min_depth)
      return;
   
   if(root->right == NULL && root->left == NULL) {
      *min_depth = MIN(height, *min_depth);
   }
   if(root->right)
      minDepth_r(root->right, height + 1, min_depth);
   
   if(root->left)
      minDepth_r(root->left, height + 1, min_depth);
   
	return;
}
int minDepth(struct TreeNode* root){
   if(root == NULL)
      return 0;
	int min_depth = INT_MAX;
	minDepth_r(root, 1, &min_depth);
	return min_depth;
}
#else // OPTIMISED
int minDepth(struct TreeNode* root){
    if(root == NULL)
        return 0;
    int st = minDepth(root->left);
    int dr = minDepth(root->right);
    if(st == 0)
        return dr + 1;
    else if(dr == 0)
        return st + 1;
    else if(st < dr)
        return st + 1;
    return dr + 1;
}
#endif
int main()
{
   struct TreeNode r1, a1, b1;
   
   //r1.left = &a1;
   r1.left = NULL;
   r1.right = &b1;
   a1.left = NULL;
   b1.left = NULL;
   a1.right = NULL;
   b1.right = NULL;   

   r1.val = 2;
   a1.val = 1;
   b1.val = 1;   

   printf("min_depth %d\n", minDepth(&r1));

   return 0;
}

