int uniquePathsWithObstacles(int** obstacleGrid, int obstacleGridSize, int* obstacleGridColSize) {
   int colMax = obstacleGridSize, rowMax = *obstacleGridColSize;
   if(rowMax == 0 || colMax == 0)
      return 0;

   int path_matrix[colMax][rowMax];
   bool obstacle = false;
   for(int row = 0; row < rowMax ; row++) {
      if(obstacleGrid[0][row] == 1 || obstacle){
         path_matrix[0][row] = 0;
         obstacle = true;
      }else {
         path_matrix[0][row] = 1;
      }
   }
   obstacle = false;
   for(int col = 0; col < colMax ; col++) {
      if(obstacleGrid[col][0] == 1 || obstacle){
         path_matrix[col][0] = 0;
         obstacle = true;
      }else {
         path_matrix[col][0] = 1;
      }
   }   
   for(int row = 1; row < rowMax; row ++) {
      for(int col = 1; col < colMax; col++) {
         if(obstacleGrid[col][row] == 1) {
            path_matrix[col][row] = 0;
         }else {
            path_matrix[col][row] = path_matrix[col-1][row] +
               path_matrix[col][row-1];
         }
      }
   }
   return path_matrix[colMax-1][rowMax-1];
}
