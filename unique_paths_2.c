/*
Link: 
https://leetcode.com/problems/unique-paths-ii/

Question:
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

Now consider if some obstacles are added to the grids. How many unique paths would there be?

An obstacle and empty space is marked as 1 and 0 respectively in the grid.

Note: m and n will be at most 100.

Example 1:

Input:
[
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
Output: 2
Explanation:
There is one obstacle in the middle of the 3x3 grid above.
There are two ways to reach the bottom-right corner:
1. Right -> Right -> Down -> Down
2. Down -> Down -> Right -> Right
 */

#include <stdio.h>
#include <stdbool.h>
/* Solution */


// DYNAMIC_PROGRAMMING
int uniquePathsWithObstacles(int** obstacleGrid, int obstacleGridSize, int* obstacleGridColSize) {
   int colMax = obstacleGridSize, rowMax = *obstacleGridColSize;
   if(rowMax == 0 || colMax == 0)
      return 0;

   int path_matrix[colMax][rowMax];
   bool obstacle = false;
   for(int row = 0; row < rowMax ; row++) {
      if(obstacleGrid[0][row] == 1 || obstacle){
         path_matrix[0][row] = 0;
         obstacle = true;
      }else {
         path_matrix[0][row] = 1;
      }
   }
   obstacle = false;
   for(int col = 0; col < colMax ; col++) {
      if(obstacleGrid[col][0] == 1 || obstacle){
         path_matrix[col][0] = 0;
         obstacle = true;
      }else {
         path_matrix[col][0] = 1;
      }
   }   
   for(int row = 1; row < rowMax; row ++) {
      for(int col = 1; col < colMax; col++) {
         if(obstacleGrid[col][row] == 1) {
            path_matrix[col][row] = 0;
         }else {
            path_matrix[col][row] = path_matrix[col-1][row] +
               path_matrix[col][row-1];
         }
      }
   }
   return path_matrix[colMax-1][rowMax-1];
}

/* Driver Code */
int main() {
   
   int ob0[] = {0,0,0};
   int ob1[] = {0,1,0};
   int ob2[] = {0,0,0};

   int *obstacle[] = {ob0, ob1, ob2};
   int colS = sizeof(ob0)/ sizeof(ob0[0]);

   printf("Number of unique ways with obstacles %d\n", uniquePathsWithObstacles(obstacle, sizeof(obstacle)/sizeof(int*), &colS));
   
   return 0;
   
}
