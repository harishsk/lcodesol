/* Link:
   https://leetcode.com/problems/find-numbers-with-even-number-of-digits/
*/

/* Problem:
Given an array nums of integers, return how many of them contain an even number of digits.

 

Example 1:

Input: nums = [12,345,2,6,7896]
Output: 2
Explanation: 
12 contains 2 digits (even number of digits). 
345 contains 3 digits (odd number of digits). 
2 contains 1 digit (odd number of digits). 
6 contains 1 digit (odd number of digits). 
7896 contains 4 digits (even number of digits). 
Therefore only 12 and 7896 contain an even number of digits.

Example 2:

Input: nums = [555,901,482,1771]
Output: 1 
Explanation: 
Only 1771 contains an even number of digits.

 

Constraints:

    1 <= nums.length <= 500
    1 <= nums[i] <= 10^5
  
*/

#define TEST
#if defined(TEST)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#endif

/* Solution: */


int findNumbers(int* nums, int numsSize){
  int even_cnt = 0;
  for(int i = 0; i < numsSize; i++) {
    if((nums[i] >= 10 && nums[i] < 100) ||
       (nums[i] >= 1000 && nums[i] < 10000) ||
       (nums[i] >= 100000 && nums[i] < 1000000)) {
      even_cnt++;
    }
  }
  return even_cnt;
}
/* Test Driver: */
#if defined(TEST)
int main() {
   int a[] = { 555,901,482,1771};
   printf("Count of number with even with even digits %d\n",
          findNumbers(a, sizeof(a) / sizeof(a[0])));
   
   return 0;
}
#endif
